# README #

2D Action game written for the Love2D framework (v0.9.2).

# The Team #

Caroline Cevallos- ccevall1@jhu.edu
Tucker Chapin- chapin@jhu.edu
Brandon Fiksel- bfiksel1@jhu.edu
Richard Gholston- rgholst1@jhu.edu
Margo Goodall- mgoodal3@jhu.edu