-- Main Game Screen
--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"
require "Camera"
require "Assets"

LevelTransition = class()

function LevelTransition:__init()
	self.image = love.graphics.newImage("assets/images/cutsceneframes.png")
	self.frames = 3
	self.frame_width = self.image:getWidth()/self.frames
	local Quad = love.graphics.newQuad
	self.quads = {}
	for j = 1, self.frames do
		self.quads[j] = Quad((j-1)*self.frame_width, 1, self.frame_width, 
					     self.image:getHeight(), self.image:getWidth(), self.image:getHeight())
	end
	self.backgroundImage = love.graphics.newImage("assets/images/cutsceneBG-01.jpg")
	self.camera = Camera(0,0)
    GLOBAL.WorldWidth = self.backgroundImage:getWidth()
	GLOBAL.WorldHeight = self.backgroundImage:getHeight()
	--self.backgroundImage:setWrap("repeat","repeat")
	--self.quad = love.graphics.newQuad(0,0,GLOBAL.WorldWidth*3,GLOBAL.WorldHeight*3,GLOBAL.WorldWidth,GLOBAL.WorldHeight)
	self.timer = 0.1
	self.imageTimer = 0.1
	self.iteration = 1
	self.ping = 1 
	self.pong = self.frames
	self.maxSpeed = 10
end

function LevelTransition:update(dt)
    self.timer = self.timer + dt
	self.imageTimer=self.imageTimer + dt

	if self.imageTimer > 0.3 then
		self.pong=self.pong+1
        self.iteration=self.pong
        if self.pong> self.frames then
	      self.iteration=self.ping
          self.ping=self.ping-1
	        if self.ping < 1 then
	           self.pong=1
			   self.ping= self.frames
			end
		end
		local x, y = self.camera:getPosition()
        --get distance to mouse
        -- velocity-based movement for inertia and acceleration
        local newX = x + self.maxSpeed*dt
        local newY = 0
        updateX = newX + x
        newPosX = newX + GLOBAL.CameraWidth/2
        self.camera:setPosition(newPosX, 0)
        self.camera:update(updateX, 0)
		if self.timer > 4 then
		GLOBAL.gameScreen.currLevel = Level(GLOBAL.gameScreen.currLevel.level["level"]["nextLevel"])
		GLOBAL.currentState = GLOBAL.gameScreen
		end
		self.imageTimer =0.1
	end
end

function LevelTransition:draw()
	self.camera:set()
	local x, y = self.camera:getPosition()
	love.graphics.draw(self.backgroundImage, GLOBAL.CameraWidth,  GLOBAL.CameraHeight)
	love.graphics.draw(self.image,self.quads[self.iteration], x + GLOBAL.CameraWidth/2, y + GLOBAL.CameraHeight/2)
	self.camera:unset()
end

function LevelTransition:keypressed(key)
end