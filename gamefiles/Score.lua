--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
--This class displays pieces collected and planet health meter (Handles UI elements)
require "class"
Score = class()

--Intializes with a new user
function Score:__init(username)
	self.newScore = 0
	self.points=0
	self.appiecesCollected = 0--GLOBAL.gameScreen.currLevel.level["level"]["startingPieces"]
    	self.appieceImage = ASSETS.appieceSprites[1]

	self.percentHealth = 1
	self.appiecesMeter = ASSETS.appiecesMeter
	self.planetHealthMeter = ASSETS.planetHealthMeter
	self.maxHealthWidth = self.planetHealthMeter:getWidth()*0.6
	self.planetHealth = 0

	self.planetAlarm = ASSETS.planetAlarm
	self.alarmPlaying = false

	self.username=username
	self.text_font = ASSETS.scoreFont
	self.path=self.username.."-HighScore.lua"
	self.g=io.open(self.path,"a+")
	self.intializer=""
	for i=1,6 do
		self.intializer = self.intializer..'0'.."\n"
	end
	self.g:write(self.intializer)
	self.g:close()

	--Animated Meters setup
    	local Quad= love.graphics.newQuad
	self.timer=0
	self.iteration=1
	self.state = "Classy"
	self.planetState = "Healthy"
	self.framerate = 0.5

	self.quads = {}
	self.quads[self.state] = {}
	self.quads[self.planetState] = {}
	--initialize quads
	local frame_height = self.appiecesMeter:getHeight()
	local sprite_width = self.appiecesMeter:getWidth()
	local frame_width = sprite_width / 2

	local frame_height2 = self.planetHealthMeter:getHeight()
	local sprite_width2 = self.planetHealthMeter:getWidth()
	local frame_width2 = sprite_width2 / 2
	for j = 1, 2 do
		self.quads[self.state][j] = Quad((j-1)*frame_width,1,
                                         frame_width,frame_height,
                                         sprite_width, frame_height)
		self.quads[self.planetState][j] = Quad((j-1)*frame_width2,1,
                                         frame_width2,frame_height2,
                                         sprite_width2, frame_height2)
	end

end

function Score:update(dt)
	self.timer = self.timer + dt
	if self.timer > self.framerate then
		self.iteration = (self.iteration % 2) + 1
		self.timer = 0
	end
	self.newScore = self.newScore + self.points
	self.points = 0
	--Update planetHealth values
        self.percentHealth = GLOBAL.gameScreen.currLevel:getPercentHealth()
        --width is percentage of max width
	self.healthwidth = self.maxHealthWidth*self.percentHealth
        if self.percentHealth <= 0.15 then
	    if not self.alarmPlaying then
	        self.planetAlarm:play()
	    end
	    if GLOBAL.gameScreen.currLevel.song:getPitch() < 2 then
	        GLOBAL.gameScreen.currLevel.song:setPitch(GLOBAL.gameScreen.currLevel.song:getPitch()+dt)
	    end
	    self.framerate = 0.1
	else
	    if self.alarmPlaying then
		self.planetAlarm:stop()
	    end
	end
end

function Score:pointer(p)
	self.points=self.points+p
end

function Score:draw()
	love.graphics.setFont(self.text_font)
	local cx,cy = GLOBAL.gameScreen.currLevel.camera:getX(),GLOBAL.gameScreen.currLevel.camera:getY()
	love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness,255)
	love.graphics.draw(self.appiecesMeter, self.quads[self.state][self.iteration],cx,cy,0,1.5,1.5)
	love.graphics.draw(self.planetHealthMeter, self.quads[self.planetState][self.iteration],
			   cx+GLOBAL.CameraWidth-self.planetHealthMeter:getWidth()*0.75,cy,0,1.5,1.5)
	--Draw appieces under label
	--local collectedAppieces = GLOBAL.gameScreen.currLevel.collectedAppieces
--	if self.appiecesCollected > 0 then
	for k,v in ipairs(collectedAppieces) do
            love.graphics.draw(v.image,v.quads[self.state][1],cx+k*5,cy+30)
	end

	--Planet health
	self.planetHealth = GLOBAL.gameScreen.currLevel:getPlanetHealth()
	if (self.percentHealth <=0.15) then
	    love.graphics.print("Hurry! The planet is dying! Find the source!",cx+30, cy+30)
	    love.graphics.setColor(255*GLOBAL.brightness,0.0,0.0,255*GLOBAL.brightness)
	else
	    love.graphics.setColor(0.0,255*GLOBAL.brightness,0.0,255*GLOBAL.brightness)
	end
	love.graphics.rectangle("fill",cx+GLOBAL.CameraWidth-self.healthwidth-10,cy+35,self.healthwidth,5)
	love.graphics.setColor(255*(GLOBAL.brightness),255*(GLOBAL.brightness),255*(GLOBAL.brightness),255)
	if self.planetHealth <= 5 and self.planetHealth > 4 then
		love.graphics.setFont(ASSETS.endOfWorldFont)
		love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness,255 - 255*(self.planetHealth-4))
		love.graphics.print("5", cx + GLOBAL.CameraWidth/2, cy + GLOBAL.CameraHeight/2)

	elseif self.planetHealth <= 4 and self.planetHealth > 3 then
		love.graphics.setFont(ASSETS.endOfWorldFont)
		love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness,255 - 255*(self.planetHealth-3))
		love.graphics.print("4", cx + GLOBAL.CameraWidth/2, cy + GLOBAL.CameraHeight/2)

	elseif self.planetHealth <= 3 and self.planetHealth > 2 then
		love.graphics.setFont(ASSETS.endOfWorldFont)
		love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness,255 - 255*(self.planetHealth-2))
		love.graphics.print("3", cx + GLOBAL.CameraWidth/2, cy + GLOBAL.CameraHeight/2)

	elseif self.planetHealth <= 2 and self.planetHealth > 1 then
		love.graphics.setFont(ASSETS.endOfWorldFont)
		love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness,255 - 255*(self.planetHealth-1))
		love.graphics.print("2", cx + GLOBAL.CameraWidth/2, cy + GLOBAL.CameraHeight/2)

	elseif self.planetHealth <= 1 and self.planetHealth > 0 then
		love.graphics.setFont(ASSETS.endOfWorldFont)
		love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness,255 - 255*self.planetHealth)
		love.graphics.print("1", cx + GLOBAL.CameraWidth/2, cy + GLOBAL.CameraHeight/2)
	end


end

function Score:record()
	self.word=""
	self.highscore = {}
	self.compscore=self.newScore
	c = io.open(self.path,"r")
	d=1
	for line in c:lines() do
		self.highscore[d]=tonumber(line)
		d=d+1
	end
	c:close()
	b = io.open(self.path,"w+")
	for i=1,6 do
		if self.highscore[i] < self.compscore then
			temp=self.highscore[i]
			self.highscore[i]=self.compscore
			self.compscore=temp
		end
		self.word=self.word..self.highscore[i].."\n"
	end
	b:write(self.word)
	b:close()
end
function Score:clear()
	z = io.open(self.path,"w+")
	self.intializer=""
	for i=1,6 do
		self.intializer = self.intializer..'0'.."\n"
	end
	z:write(self.intializer)
	z:close()
end
