--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

HighScoreScreen = class()
high_score_title_string = "High Scores"

function HighScoreScreen:__init()
    self.backgroundImage = ASSETS.highScoreScreenBG
    self.song = ASSETS.highScoreScreenSong
    self.song:setLooping(true)
    self.text_font = ASSETS.winScreenFont
    self.username="Margo"
    self.path=self.username.."-HighScore.lua"
    self.saveFileExists = love.filesystem.exists(self.path)
    self.scoresheet= ""
	self.highscore= {}

end

function HighScoreScreen:draw()
    love.graphics.setBackgroundColor(GLOBAL.color.black)
    love.graphics.draw(self.backgroundImage, GLOBAL.CameraWidth/2, GLOBAL.CameraHeight/2, 0,1,1,
		       self.backgroundImage:getWidth()/2, self.backgroundImage:getHeight()/2)
    --Dummy values for high scores
--[[	self.f=io.open(self.path,"r")
	self.scoresheet= ""
	local d=1
	if self.saveFileExists then
		for line in self.f:lines() do
			self.highscore[d]=line
			d=d+1
		end
		for i=1,6,1 do
			self.scoresheet = self.scoresheet..self.highscore[i].."\n"
		end

        end
	love.graphics.setFont(self.text_font)
	love.graphics.print(self.scoresheet,GLOBAL.CameraWidth/1.7, GLOBAL.CameraHeight/3)
	self.f:close()
]]
end

function HighScoreScreen:update()
    self.song:setVolume(GLOBAL.volume)
end

function HighScoreScreen:keypressed(key)
    self.song:stop()
    GLOBAL.mainScreen.song:play()
    GLOBAL.currentState = GLOBAL.mainScreen
end

function HighScoreScreen:mousepressed(x,y,button)
    self.song:stop()
    GLOBAL.mainScreen.song:play()
    GLOBAL.currentState = GLOBAL.mainScreen
end

function HighScoreScreen:mousemoved(x,y,dx,dy)
end
