-- Main Dialogue Screen
--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"
require "Camera"
require "Assets"
require "Conversation"

DialogueScreen = class()

function DialogueScreen:__init()
   	self.levelOneText= Conversation("level1dialogue1.txt")
	self.currChat = self.levelOneText
    self.timer=0.1
end

function DialogueScreen:update(dt)
    self.timer = self.timer + dt
	self.currChat:update(dt)
    if love.keyboard.isDown("escape") then
        love.event.quit()
    end
end


function DialogueScreen:draw()
    self.currChat:draw()
end



function DialogueScreen:keypressed(key)
    if key == "escape" then
        GLOBAL.currentState = GLOBAL.pauseScreen
    end
	self.currChat:keypressed(key)
end

function DialogueScreen:mousepressed(key)

end
