extern number radius;
extern number innerRadius;
extern number width;
extern number height;
extern number posx;
extern number posy;
extern number brightness;

vec4 effect(vec4 color, Image img, vec2 tex, vec2 scr)
{
	vec4 c = Texel(img,tex);
	vec2 pos = vec2(posx,posy);
	vec2 normscr = vec2(scr.x-width,scr.y-height);
	float dist = distance(pos,normscr);
	if (dist > radius) {
		return vec4(1.0,1.0,1.0,0.5)*brightness; //white
	} 
	if (dist > innerRadius) {
		c.r = 1.0;
		c.g = 1.0;
		c.b = 1.0;
		c.a = (0.5)*((dist - innerRadius)/(radius - innerRadius));
		return c*brightness;
	}
	else {
		return vec4(0.0,0.0,1.0,0.0)*brightness; //clear
	}
}
