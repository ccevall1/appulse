require "class"
require "Collidable"

bullet = class(Sprite)

function bullet:__init(x, y, dx, dy, image, typeBullet, frames, hasSmashImage, smashImage, smashFrames)
    self.image = image
	self.frames = frames
    self.smashImage = ASSETS.segmentSmash
    self.smashFrames = 1
    self.width = self.image:getWidth()
	self.framewidth = self.image:getWidth()/self.frames
    self.height = self.image:getHeight()
    local r = math.min(self.framewidth,self.height)/2
    self.bulletType = typeBullet
    self.positionx = x
    self.positiony = y

    --Add collidable
    self.collider = Collidable(self.positionx,self.positiony,r,GLOBAL.collIndex,self.bulletType,self)
    GLOBAL.collIndex = GLOBAL.collIndex + 1
    table.insert(WORLD.collidables, self.collider)

    self.bulletSpeed=speed
    self.dx = dx
    self.dy = dy
    self.index = index
    self.timer=0.05
    self.iteration=1
    self.smashIteration=1
    self.state = "Classy"
    self.smashState = "Smash"
    self.currState = self.state
    self.drawBullet = true
    self.ping=self.frames
    self.pong=1
    local Quad= love.graphics.newQuad
    self.quads = {}
    self.quads[self.state] = {}
    self.quads[self.smashState] = {}
    for j = 1, self.frames do
        if self.bulletType == "enemyBullet" then
          self.quads[self.state][j] = Quad((j-1)*self.framewidth, 1, self.framewidth, self.height, self.width, self.height)
        elseif self.bulletType == "playerBullet" then
          self.quads[self.state][j] = Quad((j-1)*self.framewidth,1,self.framewidth,self.height,player.sizes[player.health][3],self.height)
        end
    end
    if (hasSmashImage) then
    	self.hasSmashImage = hasSmashImage
    	self.smashImage = love.graphics.newImage(smashImage)
    	self.smashFrames = smashFrames
    end
    local framewidth = self.smashImage:getWidth()/self.smashFrames
    local frameheight = self.smashImage:getHeight()
    for j = 1, self.smashFrames do
        self.quads[self.smashState][j] = Quad((j-1)*framewidth,1,framewidth,frameheight,
					       self.smashImage:getWidth(),frameheight)
    end

    self.spawnBuffer = 0.1

end

function bullet:update(dt)
    --Update internal timer
    self.timer=self.timer+dt
    if self.spawnBuffer > 0 then
        self.spawnBuffer = self.spawnBuffer - dt
    end
    --Set frame of animation
    if self.timer>0.1 then
	if self.currState == self.state then
            self.pong=self.pong+1
            self.iteration=self.pong
            if self.pong>self.frames then
				self.iteration=self.ping
                self.ping=self.ping-1
				if self.ping< 1 then
					self.pong=1
					self.ping=self.frames
                end
            end
        elseif self.currState == "Smash" then
            self.smashIteration = self.smashIteration + 1
            if self.smashIteration>self.smashFrames then
                self.smashIteration=1
		self.collider.active = false
		--table.remove(WORLD.collidables,self.collider.index)
                self.drawBullet = false
            end
        end
        self.timer = 0.05
    end
	
    self.positionx = self.positionx + (self.dx * dt)
    self.positiony = self.positiony + (self.dy * dt)

    self.collider:update(self.positionx,self.positiony)

end

function bullet:draw()
    if self.currState == self.state then
        love.graphics.draw(self.image,self.quads[self.state][self.iteration], self.positionx, self.positiony, 0,1,1,self.framewidth/2,self.height/2)
    elseif self.currState == self.smashState then
        if self.drawBullet then
            love.graphics.draw(self.smashImage,self.quads[self.smashState][self.smashIteration],
                               self.positionx, self.positiony, 0)
        end
    end
end

function bullet:contact()
    self.currState = self.smashState
    self.collider.active = false
	--table.remove(WORLD.collidables,self.collider.index)
end

function bullet:checkCollision(c)
if (c.ctype == "border") then
    self:contact()
end
end
