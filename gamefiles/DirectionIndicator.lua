--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"

DirectionIndicator = class()

function DirectionIndicator:__init()
    -- Has small series of blinking dots in direction you're facing
    self.timer = 0
    self.positionx = 0
    self.positiony = 0
    self.angle = 0
    self.radius = 5
end

function DirectionIndicator:update(dt)
    self.timer = self.timer + dt
    self.positionx,self.positiony = player:getPosition()
    self.angle = player.angle - math.pi/2
end

function DirectionIndicator:draw()
    love.graphics.setColor(198,184,227,255*math.cos(self.timer))
    for i = 1,4 do
	local x = self.positionx + (i*100)*math.cos(self.angle)
	local y = self.positiony + (i*100)*math.sin(self.angle)
	love.graphics.circle("fill", x, y, self.radius)
    end
    love.graphics.setColor(GLOBAL.color.white)
end

