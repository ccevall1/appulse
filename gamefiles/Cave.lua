--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"
require "Collidable"

Cave = class()

function Cave:__init(image1,image2,location)
    self.image1 = image1
    self.image2 = image2
    self.timer = 0
    self.currImage = self.image1
    self.positionx = location[1]
    self.positiony = location[2]
    self.width = image1:getWidth()
    self.height = image2:getHeight()
    self.radius = math.min(self.width/2,self.height/2)
    --Add collidable
    self.collider = Collidable(self.positionx,self.positiony,self.radius,GLOBAL.collIndex,"cave",self)
    GLOBAL.collIndex = GLOBAL.collIndex + 1
    table.insert(WORLD.collidables, self.collider)
    self.collisionTimer = 0
end

function Cave:update(dt)
    self.timer = self.timer + dt
    if self.timer > 0.3 then
	self.timer = 0
	if self.currImage == self.image1 then
	    self.currImage = self.image2
	else
	    self.currImage = self.image1
	end
    end
    if self.collisionTimer > 0 then
	self.collisionTimer = self.collisionTimer - dt
    elseif self.collisionTimer <= 0 then
	self.collisionTimer = 0
    end
    if self.collisionTimer == 0 then
	self.collider.active = true
    end
end

function Cave:draw()
    love.graphics.draw(self.currImage, self.positionx, self.positiony, 0, 1, 1,self.width/2, self.height/2 )
end
