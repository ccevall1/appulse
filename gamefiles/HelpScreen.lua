--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

HelpScreen = class()

function HelpScreen:__init()
    --initialize menu options, background, click listeners?
    self.backgroundImage = ASSETS.helpScreenBG
end

function HelpScreen:draw()
    love.graphics.draw(self.backgroundImage,GLOBAL.CameraWidth/2, 0, 0, 1, 1,
                       self.backgroundImage:getWidth()/2, 0)
end

function HelpScreen:update(dt)

end

function HelpScreen:keypressed(key)
	GLOBAL.currentState = GLOBAL.mainScreen
end

function HelpScreen:mousepressed(key)
	GLOBAL.currentState = GLOBAL.mainScreen
end
