extern number time;
extern number brightness;

vec4 effect(vec4 color, Image img, vec2 tex, vec2 scr)
{
	vec4 c = Texel(img,tex);
	float wave1 = fract(sin(dot(scr,vec2(12.9898,78.233)))*43758.5453);
	float wave2 = (wave1+1.0)/2.0;
	vec4 ret = vec4(0.5,0.5,0.5,wave1*wave2*cos(time));
	return ret*brightness; //alpha blend
}
