require "Class"
GameFrameWork = class()

function GameFrameWork:__init()
	self.framework{}
	self.framework["1-1"] = {}
	self.framework["1-1"]["hasNext"] = true
	self.framework["1-1"]["nextLevel"] = "1-2"
	self.framework["1-1"]["backgroundframes"]=1
	self.framework["1-1"]["background"][1] = ASSETS.gameScreenBG
	self.framework["1-1"]["enemies"] = {} 
	self.framework["1-1"]["enemies"]["numEnemies"]=10
	self.framework["1-1"]["enemies"]["numtypes"] = 2
	self.framework["1-1"]["enemies"]["bug"] = {}
	self.framework["1-1"]["enemies"]["bug"]["health"] = 3
	self.framework["1-1"]["enemies"]["bug"]["pattern"] = "crazybug"
	self.framework["1-1"]["enemies"]["bug"]["imageTABLE"] = {}
	self.framework["1-1"]["enemies"]["bug"]["imageTABLE"]["image"]= {}
	self.framework["1-1"]["enemies"]["bug"]["imageTABLE"]["image"]["img"] = ASSETS.bugSprite
	self.framework["1-1"]["enemies"]["bug"]["imageTABLE"]["image"]["frame"] = 6
	self.framework["1-1"]["enemies"]["bug"]["imageTABLE"]["image"]= {}
	self.framework["1-1"]["enemies"]["bug"]["imageTABLE"]["deathimage"]["img"] = ASSETS.bugDeath
	self.framework["1-1"]["enemies"]["bug"]["imageTABLE"]["deathimage"]["frame"] = 3
	self.framework["1-1"]["enemies"]["bug"]["imageTABLE"]["shot"]= {}
	self.framework["1-1"]["enemies"]["bug"]["imageTABLE"]["shot"]["img"] = ASSETS.bugShot
	self.framework["1-1"]["enemies"]["bug"]["imageTABLE"]["shot"]["frame"] = 3
	self.framework["1-1"]["enemies"]["bug"]["hasDeath"] = true
	self.framework["1-1"]["enemies"]["bug"]["hasShield"] = false
	self.framework["1-1"]["enemies"]["bug"]["hasDamage"] = false
	self.framework["1-1"]["enemies"]["turtle"] = {}
	self.framework["1-1"]["enemies"]["turtle"]["health"] = 8
	self.framework["1-1"]["enemies"]["turtle"]["pattern"] = "turtleshuffle"
	self.framework["1-1"]["enemies"]["turtle"]["imageTABLE"] = {}
	self.framework["1-1"]["enemies"]["turtle"]["imageTABLE"]["image"]= {}
	self.framework["1-1"]["enemies"]["turtle"]["imageTABLE"]["image"]["img"] = ASSETS.bugSprite
	self.framework["1-1"]["enemies"]["turtle"]["imageTABLE"]["image"]["frame"] = 6
	self.framework["1-1"]["enemies"]["turtle"]["imageTABLE"]["image"]= {}
	self.framework["1-1"]["enemies"]["turtle"]["imageTABLE"]["deathimage"]["img"] = ASSETS.bugDeath
	self.framework["1-1"]["enemies"]["turtle"]["imageTABLE"]["deathimage"]["frame"] = 3
	self.framework["1-1"]["enemies"]["turtle"]["imageTABLE"]["shot"]= {}
	self.framework["1-1"]["enemies"]["turtle"]["imageTABLE"]["shot"]["img"] = ASSETS.bugShot
	self.framework["1-1"]["enemies"]["turtle"]["imageTABLE"]["shot"]["frame"] = 3
	self.framework["1-1"]["enemies"]["turtle"]["hasDeath"] = true
	self.framework["1-1"]["enemies"]["turtle"]["hasShield"] = true
	self.framework["1-1"]["enemies"]["turtle"]["hasDamage"] = false
	
end