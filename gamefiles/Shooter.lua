require "class"

Shooter = class()

function Shooter:__init()
    self.spacePressed = 10.5
	self.song = love.audio.newSource("assets/audio/babykill.ogg")
	self.song:setVolume(2)
	self.winsong = love.audio.newSource("assets/audio/youreawinner.mid")

	self.song:setVolume(GLOBAL.volume)
	self.song:play()
    self.shooter = {}
	self.score = 0
	self.gameover = false
	self.timeRemaining = 10
	self.shooter.x = 300
	self.shooter.y = 400
	self.shooter.speed = 200
	self.shooter.shots = {}
	self.babies = {}
	for i = 0,5 do
	   local baby = {}
	   baby.x = i * 60
	   baby.y = 100
	   table.insert(self.babies, baby)
	end
	self.shootersprite = love.graphics.newImage("assets/images/minigames/shooter.png")
	self.babysprite = love.graphics.newImage("assets/images/minigames/baby.png")
end

function Shooter:update(dt)
	self.timeRemaining = self.timeRemaining - dt
	if self.timeRemaining <= 0 then
		self.gameover = true
		self.gameWon = true
		self.timeRemaining = 0
		self.winsong:setLooping(false)
		self.winsong:play()
	end
	if self.gameover == false then
	self.mousex = love.mouse.getX()
	if self.mousex > self.shooter.x + 5 and self.shooter.x < 300 then
	    self.shooter.x = self.shooter.x + self.shooter.speed * dt
	elseif self.mousex < self.shooter.x - 5 then
	    self.shooter.x = self.shooter.x - self.shooter.speed * dt
	else
	    self.shooter.x = self.shooter.x
	end
	local remBaby = {}
	local remShot = {}
	for i,v in ipairs(self.shooter.shots) do
	    v.y = v.y - dt * 100
		if v.y < 0 then
			table.insert(remShot, i)
		end
		for ii,vv in ipairs(self.babies) do
			if self:collision(v.x, v.y, 5, 5, vv.x, vv.y, 32, 32) then
				table.insert(remBaby, ii)
				table.insert(remShot, i)
				self.score = self.score + 100
			end
		end
	end
	for i,v in ipairs(remBaby) do
		table.remove(self.babies, v)
	end
	for i,v in ipairs(remShot) do
		table.remove(self.shooter.shots, v)
	end
	end
end


function Shooter:keypressed(key)
if self.spacePressed - self.timeRemaining >= 0.5 then
	if (key == " ") then
		self:shoot()
		self.spacePressed = self.timeRemaining
	end
end
if self.gameWon then
	self.winsong:stop()
end
end


function Shooter:collision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end





function Shooter:shoot()
    local shot = {}
	shot.x = self.shooter.x + 8
	shot.y = self.shooter.y
	table.insert(self.shooter.shots, shot)
end

function Shooter:draw()
if self.gameover == false then
	love.graphics.draw(self.shootersprite, self.shooter.x, self.shooter.y)
	for i,v in ipairs(self.babies) do
		love.graphics.draw(self.babysprite, v.x, v.y)
	end
	love.graphics.setColor(255,255,255,255)
	for i,v in ipairs(self.shooter.shots) do
   		love.graphics.rectangle("fill", v.x, v.y, 5, 5)
	end
	love.graphics.print(self.timeRemaining, 5, 20)
else
	love.graphics.print("You win!",GLOBAL.CameraWidth/2-40, GLOBAL.CameraHeight/2)
end
 love.graphics.print(self.score, 5, 5)
end


