--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"

FreezeMask = class()

function FreezeMask:__init(shader)
    self.shader = shader
    self.timer = 0
    self.positionx = 0
    self.positiony = 0
    self.width = GLOBAL.WorldWidth
    self.height = GLOBAL.WorldHeight
    self.shader:send("brightness",GLOBAL.brightness)
end

function FreezeMask:update(dt)
end

function FreezeMask:draw()
	love.graphics.setShader(self.shader)
	love.graphics.rectangle("fill",self.positionx, self.positiony, self.width, self.height)
	love.graphics.setShader()
end

