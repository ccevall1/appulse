--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]

function LoadAssets()
ASSETS = {}
    --Title
    ASSETS.titleImage = love.graphics.newImage("assets/images/appulse_title_1.png")
    ASSETS.startButton = love.graphics.newImage("assets/images/start_button.png")

    --Backgrounds
    ASSETS.pauseScreenBG = love.graphics.newImage("assets/images/NewPauseScreen-01.png")
    ASSETS.mainScreenBG = {
                           love.graphics.newImage("assets/images/NewMainMenuStart-01.png"),
                           love.graphics.newImage("assets/images/NewMainMenuHighScore-01.png"),
                           love.graphics.newImage("assets/images/NewMainMenuOptions-01.png"),
                           love.graphics.newImage("assets/images/NewMainMenuQuit-01.png")
                          }

    ASSETS.winScreenBG = love.graphics.newImage("assets/images/win_screen.png")
    ASSETS.loseScreenBG = love.graphics.newImage("assets/images/GameOverScreen.png")
    ASSETS.highScoreScreenBG = love.graphics.newImage("assets/images/highScoreScreen.jpg")
    ASSETS.helpScreenBG = love.graphics.newImage("assets/images/HowToPlayScreenFin.jpg")
    ASSETS.optionsScreenBG = love.graphics.newImage("assets/images/OptionsScreen.jpg")
    ASSETS.continueScreenBG = love.graphics.newImage("assets/images/ContinueScreen.png")

    --Character Sprites
    ASSETS.playerSprite = love.graphics.newImage("assets/images/player_head_outline.png")
    ASSETS.playerOuch = love.graphics.newImage("assets/images/player_head_wince.png")
    ASSETS.playerDie = love.graphics.newImage("assets/images/die.png")
    ASSETS.playerProud = love.graphics.newImage("assets/images/WinSmile.png")
    --Segments
    ASSETS.segment1Sprite = love.graphics.newImage("assets/images/segment_1_outline.png")
    ASSETS.segment2Sprite = love.graphics.newImage("assets/images/segment_2_outline.png")
    ASSETS.segment3Sprite = love.graphics.newImage("assets/images/segment_3_outline.png")
    ASSETS.segment4Sprite = love.graphics.newImage("assets/images/segment_4_outline.png")
    ASSETS.segmentRegen = love.graphics.newImage("assets/images/segment_regenerating.png")
    ASSETS.segmentSmash = love.graphics.newImage("assets/images/segment_exploding.png")

    --Other Images
    ASSETS.menuSelectSprite = love.graphics.newImage("assets/images/player_head.png")
    ASSETS.cometSprite = love.graphics.newImage("assets/images/NewCometShadow.png")
    ASSETS.lightningSprite = love.graphics.newImage("assets/images/lightning.png")
    ASSETS.appieceSprites = {
				love.graphics.newImage("assets/images/PIECE.png"),
				love.graphics.newImage("assets/images/PIECE2.png"),
				love.graphics.newImage("assets/images/PIECE3.png")
			    }
    ASSETS.appiecesMeter = love.graphics.newImage("assets/images/AppiecesMeter.png")
    ASSETS.planetHealthMeter = love.graphics.newImage("assets/images/PlanetHealthMeter.png")
    ASSETS.talkBubbleImage = love.graphics.newImage("assets/images/TalkIcon.png")
    ASSETS.confirmationBox = love.graphics.newImage("assets/images/ConfirmationBox.png")
    ASSETS.yesImage = love.graphics.newImage("assets/images/YesAnimated.png")
    ASSETS.noImage = love.graphics.newImage("assets/images/NoAnimated.png")
	ASSETS.appiecePlus = love.graphics.newImage("assets/images/plusAppiece.png")

    --Music
    ASSETS.mainScreenSong = love.audio.newSource("assets/audio/MainMenuMusicOGG.ogg")
    ASSETS.highScoreScreenSong = love.audio.newSource("assets/audio/HighScoreScreenOGG.ogg")
    ASSETS.titleScreenSong = love.audio.newSource("assets/audio/TitleScreenSoundOGG.ogg")
--    ASSETS.bossDefeatedJingle = love.audio.newSource("assets/audio/WinJingle.ogg")
    ASSETS.bossDefeatedJingle = love.audio.newSource("assets/audio/BeatTheBossMusic.ogg")
    --SoundFX
    ASSETS.shootSound = love.audio.newSource("assets/audio/FireSoundOGG.ogg")
    ASSETS.painSound = love.audio.newSource("assets/audio/PainSoundOGG.ogg")
    ASSETS.hitSound = love.audio.newSource("assets/audio/HitSoundOGG.ogg")
    ASSETS.appieceCollectedSound = love.audio.newSource("assets/audio/PieceSound.ogg")
    ASSETS.newPieceCollectedSound = love.audio.newSource("assets/audio/5PieceCollectSound.ogg")
    ASSETS.menuSelectSound = love.audio.newSource("assets/audio/MenuSoundEffectOGG.ogg")
    ASSETS.deathSound = love.audio.newSource("assets/audio/Splosion.ogg")
    ASSETS.planetAlarm = love.audio.newSource("assets/audio/PlanetAlarm.ogg")

    --Fonts
    ASSETS.menuFont = love.graphics.newFont("assets/images/Electrolize-Regular.ttf", 15)
    ASSETS.winScreenFont = love.graphics.newFont("assets/images/Electrolize-Regular.ttf", 30)
    ASSETS.scoreFont = love.graphics.newFont("assets/images/Electrolize-Regular.ttf", 30)
    ASSETS.dialogueFont = love.graphics.newFont("assets/images/Electrolize-Regular.ttf", 26)
    ASSETS.endOfWorldFont = love.graphics.newFont("assets/images/Electrolize-Regular.ttf", 72)
    ASSETS.optionsFont = love.graphics.newFont("assets/images/Electrolize-Regular.ttf", 48)
    ASSETS.ConfirmationFont = love.graphics.newFont("assets/images/Electrolize-Regular.ttf", 20)
end
