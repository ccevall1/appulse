	{
		"level": {
			"player": {
				"x": 1.1,
				"y": 1.8
			},
			"hasNext": true,
			"hasSpecialEffect":false,
			"song": "assets/audio/Level3BossMusic.ogg",
			"nextLevel": "level3.txt",
			"backgroundframes": 1,
			"type": "betweenworld",
			"hasDialogue": true,
			"OpeningChat": "level3BOSSdialogue1.txt",
			"background": {
				"1": "assets/images/level2BOSS.jpg"
			},
			"exit": {
				"image1": "assets/images/portal1.png",
				"image2": "assets/images/portal2.png",
				"x": 200,
				"y": 200
			},
			"planetHealth": 200,
			"startingPieces": 0,
			"enemies": {
				"numEnemies": 0,
				"numtypes": 0
			},
			"numPieces": 0,
			"hasArcade": false,
			"Object": {
				"hasObjects": false
			},
			"NPC": {
				"numNPCs": 1,
				"1": {
					"imageTable": {
						"image": {
							"img": "assets/images/fishSprite.png",
							"frame": 4
						}
					},
					"dialogue": "postlevel2dialogue1.txt",
					"location": {
						"x": 2,
						"y": 2
					},
					"rotate":true,
					"towards":true,
					"speed": 300,
					"numPiecesGiven": 0
				}
			}
		}
	 }
