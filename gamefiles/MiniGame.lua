--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"
require "GameState"
require "Worm"
require "DressUp"
require "Shooter"
require "NoGame"
require "LevelFour"

MiniGame = class()

function MiniGame:__init(game)
	self.game = game
	self.currGame = nil
	if self.game == "Classy Worm" then
		self.currGame = Worm()
	elseif self.game == "Dress Up" then
		self.currGame = DressUp()
	elseif self.game == "Baby Killers 3" then
		self.currGame = Shooter()
	elseif self.game == "Insert Token" then
		self.currGame = NoGame()
	end
end

function MiniGame:update(dt)
	self.currGame:update(dt)
end

function MiniGame:draw()
	self.currGame:draw()
end

function MiniGame:keypressed(key)
self.currGame:keypressed(key)
if self.currGame.gameWon then
	self.currGame.song:stop()
	love.mouse.setVisible(true)
	
	GLOBAL.gameScreen.currLevel.song:setVolume(GLOBAL.volume)
	GLOBAL.gameScreen.currLevel.song:play()
	GLOBAL.currentState = GLOBAL.gameScreen
end
end

function MiniGame:mousepressed(x,y,button)
end
