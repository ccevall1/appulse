-- Opening Animation
--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"
require "Assets"
require "JSON"
require "Camera"
Conversation = class()

function Conversation:__init(file)
	local Quad= love.graphics.newQuad
	JSON = require("JSON")
	self.jsonrawtext= ""
	for line in love.filesystem.lines(file)do		
		self.jsonrawtext= self.jsonrawtext..line
	end
	self.dialogue = JSON:decode(self.jsonrawtext)
	self.backgroundImage={}
	for i = 1, self.dialogue["dialogue"]["backgroundframes"] do
		self.backgroundImage[i] =love.graphics.newImage(self.dialogue["dialogue"]["background"][""..i..""])
	end
	self.image = love.graphics.newImage(self.dialogue["dialogue"][ "spriteSheet"]["image"])
	self.maxFrame =  self.dialogue["dialogue"][ "spriteSheet"]["frames"]
	self.voice = love.audio.newSource(self.dialogue["dialogue"]["voice"])
	self.voice:setLooping(false)
	self.voice:setVolume(3*GLOBAL.volume)
	self.voice:play()

	self.winningDialogue = self.dialogue["dialogue"]["winningDialogue"]
	self.aggressive = self.dialogue["dialogue"]["aggressive"]
	self.won = false
	self.attacking = false

	self.frame_width = self.image:getWidth()/self.maxFrame
	self.sprite_sheet_width = self.image:getWidth()
	self.sprite_sheet_height = self.image:getHeight()
	self.quads = {}
	for j = 1, self.maxFrame do
		self.quads[j] = Quad((j-1)*self.frame_width, 1, self.frame_width, self.sprite_sheet_height, self.sprite_sheet_width, self.sprite_sheet_height)
	end
	self.pic = self.backgroundImage[1]

	self.text_font = ASSETS.dialogueFont

	self.timer = 0
	self.lineCount = 1
	self.frames = 1
	self.backgroundshift = 1
	self.readScript = self.dialogue["dialogue"]["text"][""..self.lineCount..""]
	self.camera = Camera(0,0)
	self.selector = ASSETS.menuSelectSprite
	self.currSelect = 1
	self.currY = GLOBAL.CameraHeight - GLOBAL.CameraHeight/4 + 40
	self.select1 = GLOBAL.CameraHeight - GLOBAL.CameraHeight/4 + 40
	self.select2 = GLOBAL.CameraHeight - GLOBAL.CameraHeight/4 + 70
	self.select3 = GLOBAL.CameraHeight - GLOBAL.CameraHeight/4 + 100

	self.drawAppieceTimer = 0
end

function Conversation:update(dt)
	self.timer = self.timer + dt
	if self.timer > 0.2 then
		self.frames = self.frames + 1
		if self.frames > self.maxFrame then
			self.frames = 1
		end
		self.backgroundshift = self.backgroundshift + 1
		if self.backgroundshift > self.dialogue["dialogue"]["backgroundframes"] then
			self.backgroundshift = 1
		end
		self.pic =  self.backgroundImage[self.backgroundshift]
		self.timer = 0
	end
	if self.lineCount == self.dialogue["dialogue"]["totalLines"] then
		self.dialogueEnd = true
	end

	if self.drawAppieceTimer > 0 then
		self.drawAppieceTimer = self.drawAppieceTimer - dt
	end
	if GLOBAL.inputMode == "mouse" then
		if self.lineCount == self.dialogue["dialogue"]["linesbeforeplayerresponse"]  then
		self.mousey = love.mouse.getY()
		if self.mousey < self.select1 + 15 and self.mousey > self.select1 - 15 then
            self.currSelect = 1
            self.currY = self.select1
		elseif self.mousey < self.select2 + 15 and self.mousey > self.select2 - 15 then
            self.currSelect = 2
            self.currY = self.select2
		elseif self.mousey < self.select3 + 15 and  self.mousey > self.select3 - 15 then
			self.currSelect = 3
            self.currY = self.select3
		end
	end	
	end
end

function Conversation:draw()
	love.graphics.setShader()
	local adjustedwhite = 255*GLOBAL.brightness
	love.graphics.setColor(adjustedwhite,adjustedwhite,adjustedwhite,255);
	love.graphics.setFont(self.text_font)
	love.graphics.draw(self.pic,0, 0, 0, 1, 1 )
	love.graphics.draw(self.image, self.quads[self.frames],self.camera:getX() + GLOBAL.CameraWidth/4, GLOBAL.CameraHeight/7 + self.camera:getY())
	love.graphics.setColor(122*GLOBAL.brightness,123*GLOBAL.brightness,154*GLOBAL.brightness)
	love.graphics.rectangle("fill", 0, GLOBAL.CameraHeight - GLOBAL.CameraHeight/4, GLOBAL.CameraWidth, GLOBAL.CameraHeight/4 )
	love.graphics.setColor(adjustedwhite,adjustedwhite,adjustedwhite,255)
	love.graphics.print(self.readScript, self.camera:getX()+10, GLOBAL.CameraHeight-GLOBAL.CameraHeight/4 + self.camera:getY())
	if self.lineCount == self.dialogue["dialogue"]["linesbeforeplayerresponse"]  then
		love.graphics.draw(self.selector, 20, self.currY)
	end

	if self.drawAppieceTimer > 0 then
		
	end

end

function Conversation:keypressed(key)
if key == " " or key == "return"  then
	if self.dialogueEnd then
		GLOBAL.currentState = GLOBAL.gameScreen
		self.readScript = self.dialogue["dialogue"]["text"][""..self.dialogue["dialogue"]["totalLines"]+1 ..""]
	elseif self.lineCount < self.dialogue["dialogue"]["linesbeforeplayerresponse"] then
		self.lineCount = self.lineCount + 1
		self.readScript = self.dialogue["dialogue"]["text"][""..self.lineCount..""]
	elseif self.lineCount == self.dialogue["dialogue"]["linesbeforeplayerresponse"] then
		self.lineCount = self.lineCount + 1
		self.readScript = self.dialogue["dialogue"]["text"][""..self.lineCount..""][""..self.currSelect..""]

		if (""..self.currSelect) == (""..self.winningDialogue) or self.winningDialogue == "all" then
			self.won = true
		elseif (""..self.currSelect) ~= (""..self.winningDialogue) and self.winningDialogue ~= "all" then
			if self.aggressive then
				self.won = false
				self.attacking = true
			end
		elseif self.winningDialogue == "none" and self.aggressive then
			self.won = false
			self.attacking = true
		end

	end
elseif (key == "down" or key == "s") and self.lineCount == self.dialogue["dialogue"]["linesbeforeplayerresponse"]  then
	if self.currSelect < 3 then
            self.currSelect = self.currSelect + 1
            self.currY = self.currY + 30
        end
elseif (key == "up" or key == "w") then
        if self.currSelect > 1 and self.lineCount == self.dialogue["dialogue"]["linesbeforeplayerresponse"]  then
			self.currSelect = self.currSelect - 1
			self.currY = self.currY - 30
        end
end		
end	

function Conversation:mousepressed(x,y,button)
if button == "l" then
	if self.dialogueEnd then
		GLOBAL.currentState = GLOBAL.gameScreen
		self.readScript = self.dialogue["dialogue"]["text"][""..self.dialogue["dialogue"]["totalLines"]+1 ..""]
	elseif self.lineCount < self.dialogue["dialogue"]["linesbeforeplayerresponse"] then
		self.lineCount = self.lineCount + 1
		self.readScript = self.dialogue["dialogue"]["text"][""..self.lineCount..""]
	elseif self.lineCount == self.dialogue["dialogue"]["linesbeforeplayerresponse"] then
		self.lineCount = self.lineCount + 1
		self.readScript = self.dialogue["dialogue"]["text"][""..self.lineCount..""][""..self.currSelect..""]

		if (""..self.currSelect) == (""..self.winningDialogue) or self.winningDialogue == "all" then
			self.won = true
		elseif (""..self.currSelect) ~= (""..self.winningDialogue) and self.winningDialogue ~= "all" then
			if self.aggressive then
				self.won = false
				self.attacking = true
			end
		elseif self.winningDialogue == "none" and self.aggressive then
			self.won = false
			self.attacking = true
		end
	end
end

end

function Conversation:mousemoved(x,y,dx,dy)
	
end
