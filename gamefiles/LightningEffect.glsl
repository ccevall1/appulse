extern number maxTime;
extern number timeLeft;
extern number brightness;

vec4 effect(vec4 color, Image img, vec2 tex, vec2 scr)
{
	vec4 c = Texel(img,tex);
	if (timeLeft > 0) {
		c.r = 1.0;
		c.g = 1.0;
		c.b = 1.0;
		c.a = (timeLeft)/(maxTime - timeLeft);
		return c*brightness; //alpha blend
	}
	else {
		return vec4(0.0,0.0,0.0,0.0)*brightness; //clear
	}
}
