--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"
require "Collidable"
Tail = class()

-- imagePath is a mandatory string, but shape and location are not
function Tail:__init(framerate,frame_width, frame_height, index, playerx, playery, image, sprite_width,sprite_height,y_start)
    self.width = frame_width
    self.height = frame_height

    --default position based on index and player position
    self.index = index
    self.positionx = playerx
    self.positiony = playery
    self.radius = 40
    local r = math.min(self.width,self.height)/2
    self.angle = self.index * math.pi/2

    --Add collidable
    self.collider = Collidable(self.positionx,self.positiony,r,GLOBAL.collIndex,"tail",tail)
    GLOBAL.collIndex = GLOBAL.collIndex + 1
    table.insert(WORLD.collidables, self.collider)

    --Sprite animation stuff
    local Quad= love.graphics.newQuad
    self.image = image
    self.regenImage = ASSETS.segmentRegen
    self.timer=0
    self.iteration=1
    self.regenIteration=1
    self.state = "Classy"
    self.regenState = "Regeneration"
    self.currState = self.state
    self.ping=1
    self.pong=6
    self.regen=3
    self.frames=frames
    self.quads = {}
    self.quads[self.state] = {}
    self.quads[self.regenState] = {}
    for j = 1, 6 do
        self.quads[self.state][j] = Quad((j-1)*frame_width, y_start, frame_width, frame_height, sprite_width, sprite_height)
    end
    for j = 1, 3 do
        self.quads[self.regenState][j] = Quad((j-1)*71, y_start, 71, 62, 212, 62)
    end

    self.regenTimer = 0

end

function Tail:update(dt, movement)

    --Update internal timer
    self.timer=self.timer+dt

    --Set frame of animation
    if self.timer>0.2 then

        if self.currState == self.state then
            self.pong=self.pong+1
            self.iteration=self.pong
            if self.pong>6 then
                self.iteration=self.ping
                self.ping=self.ping-1
                if self.ping<1 then
                    self.pong=1
                    self.ping=6
                end
            end
        elseif self.currState == self.regenState then
            self.regenIteration=self.regenIteration+1
            if self.regenIteration > 3 then
                self.regenIteration = 1
                self.currState = self.state
            end
        end

        self.timer = 0.1
    end

    --If player's health is less than this segment and less than the max regen amount
    if player.health == self.index - 1 and player.health < player.currMaxHealth then
        if self.regenTimer < (3) then --hardcoding in 3 second regeneration time
            self.regenTimer = self.regenTimer + dt
        else  --regenerate one health after x time
            self.regenTimer = 0
            self.currState = self.regenState
            player.health = player.health + 1
        end
    end
    self.angle = self.angle + dt
    self.positionx,self.positiony = player:getPosition()
    local newX = self.positionx + math.sin(self.angle) * self.radius*math.cos(self.timer)
    local newY = self.positiony + math.cos(self.angle) * self.radius*math.cos(self.timer)
    self.drawX = newX - player:getWidth()/2
    self.drawY = newY - player:getHeight()/2

end

function Tail:draw()
if player.health >= self.index then
    local x,y = self:getPosition()
    if (self.currState == self.state) then
        love.graphics.draw(self.image,self.quads[self.state][self.iteration],
                           self.drawX, self.drawY)
    elseif (self.currState == self.regenState) then
        love.graphics.draw(self.regenImage,self.quads[self.regenState][self.regenIteration],
                           self.drawX, self.drawY)
    end
    self.collider:draw()
end
end

function Tail:getPosition()
    return self.positionx,self.positiony
end

function Tail:checkCollision(c)
if (c.ctype == "bullet" or c.ctype == "enemy") then
    player:damaged()
end
end
