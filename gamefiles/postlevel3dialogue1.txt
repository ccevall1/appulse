{
    "dialogue": {
        "linesbeforeplayerresponse": 8,
	"voice": "assets/audio/Lvl1Voice.ogg",
        "playerchoices": 3,
		"totalLines":9,
		"font":{
			"fontSize": 26,
			"fontDisplay":"assets/images/CaesarDressing-Regular.ttf"
		} ,
        "text":{
			"1": "Lars: Hunky rock! I'm so glad to see you're ok!",
			"2": "Lars: Remember me? It's Lars from Cratertown!",
			"3": "Lars: I can't believe all the good you've done! \n You destroyed the King Hermy.",
			"4": "Lars: You squished the corrupt puppet-master Doc Molly.",
			"5": "Lars: And now you've destroyed Hugh Manatee, \n who was draining this planet's energy sources.",
			"6": "Lars: While you were doing that, I was busy with my own work.",
			"7": "Lars: I think I've found a way to recover your home planet that \n was destroyed!",
			"8":"Player:\n \t Negotiate \n \t Seduce \n \t Combat \n",
			"9": {
				"1": "Native: No time to explain, hop in that portal!",
				"2": "Native: No time for your flirtations. Hop into that portal!",
				"3": "Native: I swear I'm telling the truth! Hop in that portal and see for yourself!"
			},
			"10": "Native: I've told you all I can."
		},
		"backgroundframes": 1,
        "background": {
            "1": "assets/images/underground3.png"
        },
        "spriteSheet": {
            "image": "assets/images/bugTalk.png",
            "frames": 3
        },
	"winningDialogue": "all",
	"aggressive": false
    }
}
