-- Opening Animation
--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"
require "Assets"
require "class"

OpeningScreen = class()

function OpeningScreen:__init()
self.frame = 1
self.pic = {}
   for j = 1, 27 do
        self.pic[j] = love.graphics.newImage("assets/images/CutScene/Opening"..j..".png")
   end
self.clip = self.pic[1]
GLOBAL.WorldWidth = self.clip:getWidth()
GLOBAL.WorldHeight = self.clip:getHeight()
self.quad = love.graphics.newQuad(0,0,GLOBAL.WorldWidth*3,GLOBAL.WorldHeight*3,GLOBAL.WorldWidth,GLOBAL.WorldHeight)
self.timer = 0
end

function OpeningScreen:update(dt)
	self.timer = self.timer + dt
	if self.timer > 0.125 then
		self.frame=self.frame + 1
		if self.frame < 28 then
			self.clip = self.pic[self.frame]
			self.timer = 0
		else 
		GLOBAL.currentState = GLOBAL.titleScreen
		end
	end
end

function OpeningScreen:draw()
    love.graphics.draw(self.clip)
end

function OpeningScreen:keypressed(key)
   GLOBAL.currentState = GLOBAL.titleScreen
end
