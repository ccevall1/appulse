--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

ContinueScreen = class()

function ContinueScreen:__init()
    local Quad = love.graphics.newQuad
    --initialize menu options, background, click listeners?
    self.backgroundImage = ASSETS.continueScreenBG
    self.choice = ""
    self.currSelect = "yes"
    self.timer = 0
    self.yes = ASSETS.yesImage
    self.no = ASSETS.noImage

	self.state = "Selected"
	self.yesquads = {}
	self.yesIter = 1
	self.noIter = 1
	self.yesquads[self.state] = {}
	self.noquads = {}
	self.noquads[self.state] = {}
	self.yesframewidth = self.yes:getWidth()/2
	self.noframewidth = self.no:getWidth()/2
	self.yeswidth = self.yes:getWidth()
	self.yesheight = self.yes:getHeight()
	self.nowidth = self.no:getWidth()
	self.noheight = self.no:getHeight()
	--Set up animations for yes and no
	for j = 1, 3 do
	    self.yesquads[self.state][j] = Quad((j-1)*self.yesframewidth,1,self.yesframewidth,
						self.yesheight,self.yeswidth,self.yesheight)
	    self.noquads[self.state][j] = Quad((j-1)*self.noframewidth,1,self.noframewidth,
						self.noheight,self.nowidth,self.noheight)
	end

end

function ContinueScreen:draw()
--	local cx,cy = GLOBAL.gameScreen.currLevel.camera:getX(),GLOBAL.gameScreen.currLevel.camera:getY()
    love.graphics.draw(self.backgroundImage,0, 0, 0, 1, 1)
    love.graphics.draw(self.yes,self.yesquads[self.state][self.yesIter+1],
			GLOBAL.CameraWidth/2 - 100,GLOBAL.CameraHeight/2 + 50)
    love.graphics.draw(self.no,self.noquads[self.state][self.noIter+1],
			GLOBAL.CameraWidth/2 + 100,GLOBAL.CameraHeight/2 + 50)
--    love.graphics.draw(self.yes, GLOBAL.CameraWidth/2 - 100, GLOBAL.CameraHeight/2 + 100)
--    love.graphics.draw(self.no, GLOBAL.CameraWidth/2 + 100, GLOBAL.CameraHeight/2 + 100)
end

function ContinueScreen:update(dt)
    self.timer = self.timer + dt
    local mx,my = love.mouse:getPosition()

    if self.timer > 0.2 then

      if GLOBAL.inputMode == "mouse" then

        if mx < GLOBAL.CameraWidth/2 then
	    self.yesIter = (self.yesIter + 1) % 2
        else
	    self.noIter = (self.noIter + 1) % 2
        end

      elseif GLOBAL.inputMode == "keyboard" then

        if self.currSelect == "yes" then
	    self.yesIter = (self.yesIter + 1) % 2
        elseif self.currSelect == "no" then
	    self.noIter = (self.noIter + 1) % 2
        end

      end

	self.timer = 0
    end

if self.choice == "yes" then
	player.currMaxHealth = 0
	player.postDeathTimer = 0
	GLOBAL.gameScreen.currLevel = Level("level"..GLOBAL.currentLevel..".txt")
	self.choice = ""
	GLOBAL.currentState = GLOBAL.gameScreen
	GLOBAL.gameScreen.score:clear()
elseif self.choice == "no" then
	self.choice = ""
	GLOBAL.currentState = GLOBAL.loseScreen
end
end

function ContinueScreen:keypressed(key)
if key == "right" or key == "d" then
	self.currSelect = "no"
elseif key == "left" or key == "a" then
	self.currSelect = "yes"
end

if key == "y" then
	self.choice = "yes"
elseif key == "n" then
	self.choice = "no"
end

if key == " " or key == "return" then
	if self.currSelect == "yes" then
		self.choice = "yes"
	elseif self.currSelect == "no" then
		self.choice = "no"
	end
end
end

function ContinueScreen:mousepressed(x,y,button)
if button == "l" then
	if x < GLOBAL.CameraWidth/2 then
		self.choice = "yes"
	else
		self.choice = "no"
	end
end
end
