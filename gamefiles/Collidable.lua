--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"

Collidable = class()

function Collidable:__init(x,y,r,i,t,o)
    self.positionx = x
    self.positiony = y
    self.radius = r
    self.index = i
    self.ctype = t
    self.active = true
    self.object = o
end

function Collidable:update(x,y)
    self.positionx = x
    self.positiony = y
    --check if other collidables on screen are in range
    for k,v in ipairs(WORLD.collidables) do
        if self.active==true and v.active==true then
            self:checkCollision(v)
        end
    end
end

function Collidable:draw()
--For testing
if DEBUG.collisions and self.active then
    love.graphics.circle("fill",self.positionx,self.positiony,self.radius)
end
end

function Collidable:checkCollision(c)
    --first check that you're not colliding with yourself
    if self.index == c.index then
        --ignore
    else
    if self.active==true and c.active==true then
            --check if there is actually a collision (circular bounding boxes)
            local distance = math.sqrt((self.positionx - c.positionx)^2 + (self.positiony - c.positiony)^2)
            if distance < (self.radius + c.radius) then
                --Call specific checkCollision for each class to handle
                self.object:checkCollision(c)
            end
    end
    end
end
