--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

OptionsScreen = class()

function OptionsScreen:__init()
    --initialize menu options, background, click listeners?
    self.backgroundImage = ASSETS.optionsScreenBG

    self.currentlySelected = 1 --which menu option is the player pointed at?
   --load graphic
    self.optionsSelector = ASSETS.menuSelectSprite
end

function OptionsScreen:draw()
    love.graphics.setBackgroundColor(GLOBAL.color.black)
    love.graphics.draw(self.backgroundImage,GLOBAL.CameraWidth/2, 0, 0, 1, 1,
                       self.backgroundImage:getWidth()/2, 0)
    --print menu options
    local currY = GLOBAL.CameraHeight/2 + 70
    local currX = GLOBAL.CameraWidth/3 - 35
--    local option_height_index
    for k,v in ipairs(pause_menu_options) do
        love.graphics.setColor(GLOBAL.color.white)
        currY = currY + 50
	currX = currX + 30
        if k == pause_currently_selected then
            --draw head
            love.graphics.draw(pause_selector, currX, currY-50)
        end
    end
end

function OptionsScreen:update(dt)

end

function OptionsScreen:keypressed(key)
    if key == "down" then
        pause_enable_selection = true
        if pause_currently_selected < #pause_menu_options then
            pause_currently_selected = pause_currently_selected + 1
        else pause_currently_selected = 1
        end
    elseif key == "up" then
        pause_enable_selection = true
        if pause_currently_selected > 1 then
            pause_currently_selected = pause_currently_selected - 1
        else pause_currently_selected = #pause_menu_options
        end
    elseif key == "escape" then
        GLOBAL.currentState = GLOBAL.gameScreen
    end

    if pause_enable_selection then
        if key == " " then
            --parse which menu option you are at
            local opt = pause_currently_selected
            if pause_menu_options[opt] == "Resume" then
		GLOBAL.gameScreen.currLevel.song:play()
                GLOBAL.currentState = GLOBAL.gameScreen
            elseif pause_menu_options[opt] == "Quit" then
                GLOBAL.gameScreen.currLevel.song:stop()
                GLOBAL.mainScreen.song:play()
                GLOBAL.currentState = GLOBAL.mainScreen
            end
        end
    end
end

function OptionsScreen:mousepressed(x,y,button)

end

function OptionsScreen:mousemoved(x,y,dx,dy)
end
