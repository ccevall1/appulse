--[[
- Caroline Cevallos- ccevall1@jhu.edu
- Tucker Chapin- chapin@jhu.edu
- Brandon Fiksel- bfiksel1@jhu.edu
- Richard Gholston- rgholst1@jhu.edu
- Margo Goodall- mgoodall3@jhu.edu
]]
require "class"
Animation = class()
-- img-the png file of the picture
-- frames-How many frames in this animation
-- states- How many states in the animation
-- frame_width, frame_height
function Animation:_init(framerate,frame_width,frame_height,sprite_width,sprite_height,y_start)
  self.framerate = framerate
  self.sprite_width = sprite_width
  self.sprite_height = sprite_height
  self.frame_width = frame_width
  self.frame_height = frame_height
  self.y_start = y_start
end

function Animation:update(dt)
	timer = timer + dt
	if timer > framerate
		then timer = 0.1
	end
end
