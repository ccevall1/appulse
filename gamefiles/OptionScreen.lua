--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

OptionScreen = class()

function OptionScreen:__init()
    self.backgroundImage = ASSETS.optionsScreenBG
    self.currently_selected = ""
    self.width = self.backgroundImage:getWidth()*0.25
    self.height = self.backgroundImage:getHeight()*0.25
end

function OptionScreen:draw()
    love.graphics.setColor(GLOBAL.color.white)
    love.graphics.draw(self.backgroundImage,0,0,0,0.25,0.25)
    love.graphics.setFont(ASSETS.optionsFont)
    love.graphics.print(string.format("%.1f",GLOBAL.brightness),self.width/4.4,self.height/2.2)
    love.graphics.print(string.format("%.1f",GLOBAL.volume),3*self.width/4.35,self.height/2.2)
--[[    if GLOBAL.inputMode == "keyboard" then
    	love.graphics.setFont(ASSETS.ConfirmationFont)
	love.graphics.print("a",self.width/5,self.height/2.1)
	love.graphics.print("d",self.width/3,self.height/2.2)
    end
]]

end

function OptionScreen:update(dt)
GLOBAL.mainScreen.song:setVolume(GLOBAL.volume)
if GLOBAL.inputMode == "mouse" then
local x,y = love.mouse.getPosition()

if y > (self.height/3) and y < (self.height/1.7) then
    if x < (self.width/4) then
	self.currently_selected = "brightnessDown"
    elseif (x < self.width/2) then
	self.currently_selected = "brightnessUp"
    elseif (x < 3*self.width/4) then
	self.currently_selected = "volumeDown"
    else
	self.currently_selected = "volumeUp"
    end
elseif y > (3*self.height/4) then
    if x < (self.width/3) then
	self.currently_selected = "back"
    elseif x > (self.width/2) then
	self.currently_selected = "help"
    end
end
end
end

function OptionScreen:keypressed(key) --global function, check state
if key == "a" then
    if GLOBAL.brightness > 0 then
	GLOBAL.brightness = GLOBAL.brightness - 0.1
    end
elseif key == "d" then
    if GLOBAL.brightness < 1 then
	GLOBAL.brightness = GLOBAL.brightness + 0.1
    end
elseif key == "left" then
    if GLOBAL.volume > 0 then
	GLOBAL.volume = GLOBAL.volume - 0.1
    end
elseif key == "right" then
    if GLOBAL.volume < 1 then
	GLOBAL.volume = GLOBAL.volume + 0.1
    end
elseif key == "h" then
    GLOBAL.currentState = GLOBAL.helpScreen
else
    GLOBAL.currentState = GLOBAL.mainScreen
end
end

function OptionScreen:mousepressed(x,y,button)
if button == "l" then
    --parse which menu option you are at
    local opt = self.currently_selected
    if opt == "brightnessDown" then
        if GLOBAL.brightness > 0 then
	    GLOBAL.brightness = GLOBAL.brightness - 0.1
        end
    elseif opt == "brightnessUp" then
        if GLOBAL.brightness < 1 then
	    GLOBAL.brightness = GLOBAL.brightness + 0.1
        end
    elseif opt == "volumeDown" then
        if GLOBAL.volume > 0 then
	    GLOBAL.volume = GLOBAL.volume - 0.1
        end
    elseif opt == "volumeUp" then
        if GLOBAL.volume < 1 then
	    GLOBAL.volume = GLOBAL.volume + 0.1
        end
    elseif opt == "back" then
	GLOBAL.currentState = GLOBAL.mainScreen
    elseif opt == "help" then
	GLOBAL.currentState = GLOBAL.helpScreen
    end
end
end

