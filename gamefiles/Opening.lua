-- Opening Animation
--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"
require "Assets"

OpeningScreen = class()

function OpeningScreen:__init(pictures, nextClip)
self.frame = 1
self.clip={}
self.pictures = pictures
for i=1, self.pictures do
self.clip[i] = love.graphics.newImage("assets/images/CutScene/Opening"..i..".png")
end
self.pic=self.clip[1]
GLOBAL.WorldWidth = self.pic:getWidth()
GLOBAL.WorldHeight = self.pic:getHeight()
self.timer = 0
self.nextClip = nextClip
end

function OpeningScreen:update(dt)
	self.timer = self.timer + dt
	if self.timer > 0.2 then
		self.frame=self.frame + 1
		if self.frame > pictures then
			GLOBAL.currentState = nextClip
		end
		self.pic = self.clip[self.frame]
		self.timer = 0
	end
end

function OpeningScreen:draw()
    love.graphics.setColor(GLOBAL.color.white);
    love.graphics.draw(self.clip, 0,0, 1, 1)
end

function OpeningScreen:keypressed(key)
   GLOBAL.currentState = GLOBAL.titleScreen
end
