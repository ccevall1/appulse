-- Handles Level Four end of game events
--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"
require "Enemy"
require "Assets"
require "Conversation"
require "Appiece"
require "Cave"
require "NPC"
require "Mask"
require "Fog"

LevelFour = class()

function LevelFour:__init()
    self.state = "Title"
    --Internal timer for level
    self.timer = 0

    --initialize text and opening animation
    self.image1 = ASSETS.titleImage
    self.start = ASSETS.startButton
    self.background = love.graphics.newImage("assets/images/checkerboard.jpg")

    self.song = ASSETS.titleScreenSong
    self.song:setLooping(false)
    self.song:play()
    self.titleshader = love.graphics.newShader("NewTitleEffect.glsl")
    self.glitchshader = love.graphics.newShader("FogEffect.glsl")
    self.flashshader = love.graphics.newShader("FlashEffect.glsl")
    self.glitchEnemy = love.graphics.newImage("assets/images/GridLarva.png")
    self.mask = Fog(self.titleshader)
    self.camera = Camera(0,0)

    player.vulnerable = false
    self.enemies = {}
    self.Objects = {}

    self.maxPlanetHealth = 100 --to be read from file
    self.planetHealth = 0.15*self.maxPlanetHealth
    self.endOfWorldTimer = 8
    self.numEnemies = 1000000000000

end

function LevelFour:update(dt)
self.timer = self.timer + dt
if self.state == "Title" then		
    self.titleshader:send("time",self.timer)
    if self.timer > 4 then
	self.mask.shader = self.glitchshader
	self.state = "Game"
	self.camera:setShake(20,10000)
    end
elseif self.state == "Game" then
	player:update(dt)
	self.endOfWorldTimer = self.endOfWorldTimer - dt
	--Generate random enemies and appieces, comets, lightning etc.
	if math.floor(self.timer) % math.random(1,5) == 0 then
		local imgTable = {}
		imgTable["image"] = {}
		imgTable["shot"] = {}
		imgTable["image"]["img"] = "assets/images/GridLarva.png"
		imgTable["image"]["frame"] = 6
		imgTable["shot"]["img"] = "assets/images/enemy_shot.png"
		imgTable["shot"]["frame"] = 6
		table.insert(self.enemies, Enemy(imgTable,"glitch",250,25,false,
				false,false,false, {math.random()*2,
				math.random()*2},100000,500,true,0))
	end
    for k,v in ipairs(self.enemies) do
	v:update(dt)
    end

    if self.endOfWorldTimer <= 1 then
	--set shader to flash of white
	self.mask.shader = self.flashShader
	self.mask.timer = 0
    end
    if self.endOfWorldTimer <= 0 then
	--move to boss level
	self.song:stop()
	GLOBAL.gameScreen.currLevel.planetHealth = GLOBAL.gameScreen.currLevel.maxPlanetHealth
	self:clearEnemies()
	player.vulnerable = true
	GLOBAL.gameScreen.currLevel = Level("level4BOSS1.txt")
    end

end
end

function LevelFour:draw()
if self.state == "Title" then
--    love.graphics.setShader()
--    love.graphics.setShader(self.titleshader)
    love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness,255)
    love.graphics.draw(self.image1, GLOBAL.CameraWidth/2, GLOBAL.CameraHeight/2, 0, 1, 1,
                       self.image1:getWidth()/2, self.image1:getHeight()/2)
    self.mask:draw()
    love.graphics.setShader(GLOBAL.brightnessShader)
elseif self.state == "Game" then
	self.camera:set()
	love.graphics.setColor(GLOBAL.color.white)
	--draw background
	love.graphics.draw(self.background, 0, 0, 0, 1, 1)

	-- player
	local x,y = player:getPosition()
	player:draw()		

	for k,v in ipairs(self.enemies) do
		v:draw()
	end

	self.mask:draw()
	self.camera:unset()
end
end

function LevelFour:keypressed(key)
	player:keypressed(key)
end


function LevelFour:getPercentHealth()
	return 0.15
end

function LevelFour:clearEnemies()
  for k,v in ipairs(self.enemies) do
	v.collider.active = false
		for i,j in ipairs(v.shooter) do
			j.collider.active = false
			table.remove(WORLD.collidables, j.collider.index)
			table.remove(v.shooter, i)
		end
	table.remove(WORLD.collidables, v.collider.index)
	table.remove(self.enemies, k)
    end
	self.enemies = {}
end

function LevelFour:dropPiece(x,y)
	table.insert(appieces, Appiece(3,0.3,78,22,{x,y},self.appieceTotal))
	--self.appieceCounter = self.appieceCounter + 1
end

function LevelFour:mousepressed(x,y,button)
	player:mousepressed(x,y,button)
--[[	for index, npc in ipairs(self.NPCs) do
		npc:mousepressed(x,y,button)
	end
	for index, arcade in ipairs(self.arcades) do
		arcade:mousepressed(x,y,button)
	end
]]
end


