-- Main Game Screen
--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"
require "Player"
require "Enemy"
require "Camera"
require "Assets"
require "Comet"
require "Conversation"
require "Appiece"
require "Cave"

Level = class()

function Level:__init(file)
	--Initialize level from file
	JSON = assert(loadfile "JSON.lua")()
	self.jsonrawtext= ""
	for line in love.filesystem.lines(file)do		
		self.jsonrawtext= self.jsonrawtext..line
	end
	self.level = JSON:decode(self.jsonrawtext)
	self.levelNum = self.level["level"]["levelNum"]
	self.bossDefeated = false
	self.postBossTimer = 0
	self.bossDefeatedJingle = ASSETS.bossDefeatedJingle
	self.levelState = self.level["level"]["type"]

	--Initialize Camera Object
	self.camera = Camera(0,0)
 
	--Set Background
	self.backgroundImage={}
	for i = 1, self.level["level"]["backgroundframes"] do
		self.backgroundImage[i] =love.graphics.newImage(self.level["level"]["background"][""..i..""])
	end
	GLOBAL.WorldWidth = self.backgroundImage[1]:getWidth()
	GLOBAL.WorldHeight = self.backgroundImage[1]:getHeight()
	self.backgroundshift = 1

	--Start Music
	self.song = love.audio.newSource(self.level["level"]["song"])
	self.song = ASSETS.gameScreenSong
	self.song:setLooping(true)

	--Initialize mouse object
	mouse = {}
		mouse.x, mouse.y = love.mouse.getPosition()

	--Enemy Objects for testing
	self.enemies = {}
	self.numEnemies = self.level["level"]["enemies"]["numEnemies"]
	self.wave = 3

	--Internal timer for level
	self.timer = 0

	self.pic = self.backgroundImage[1]
	self.clip={}
	    self.clip[1]= love.graphics.newImage(self.level["level"]["exit"]["image1"])
	    self.clip[2]= love.graphics.newImage(self.level["level"]["exit"]["image2"])
	    self.caveimg=self.clip[1]
	self.outro = {}
	self.outro[1]= love.graphics.newImage("assets/images/Start.png")
	self.outro[2]= love.graphics.newImage("assets/images/Middle.png")
	self.outro[3]= love.graphics.newImage("assets/images/Middle2.png")
	self.outro[4]= love.graphics.newImage("assets/images/End.png")
	self.intro= self.outro[1]
	self.enter=false
	self.count = 1
	self.start=false
	self.round= false
	self.EndofDialogue = false
	self.imgur = 1

	if self.level["level"]["type"] == "overworld" then
    	    player = Player(playerImage, 6 , 0.1 , 40 , 253 , {GLOBAL.WorldWidth/2, GLOBAL.WorldHeight/2})
	elseif self.level["level"]["type"] == "underworld" then
	    player.positionx = GLOBAL.WorldWidth/2
	    player.positiony = GLOBAL.WorldHeight/2
	end
	--Comet Object
	if self.levelNum == 1 then
	    self.cometTimer = 0
	    self.comet = Comet(0.3, 9, 904, 201)
	end

	--Planet Health
        self.maxPlanetHealth = self.level["level"]["planetHealth"] --to be read from file
        self.planetHealth = self.maxPlanetHealth

	--If overworld, initialize appieces and planet health
	self.cave = Cave(self.clip[1],self.clip[2],{self.level["level"]["exit"]["x"],self.level["level"]["exit"]["y"]})
	--Appiece setup
        appieces = {}
	if self.level["level"]["type"] == "overworld" then
	    self.appieceTotal = 1
	    --create cave collidable
	    for i = 1, 4 do
       	        table.insert(appieces,Appiece(3,0.3,78,22,{0,0},i))
	        appieces[i].doDraw = false
	        appieces[i].collider.active = false
		print("appieces added: "..i)
		self.appieceTotal = self.appieceTotal + 1
	    end
	    --From file
	    for i = 1, 6 do
	        local posx = self.level["level"]["locations"][""..i..""]["x"]*(GLOBAL.WorldWidth/2)
	        local posy = self.level["level"]["locations"][""..i..""]["y"]*(GLOBAL.WorldHeight/2)
                table.insert(appieces,Appiece(3,0.3,78,22,{posx,posy},i))
		print("appieces added: "..i)
	    end
	    self.appieceModCounter = self.level["level"]["startingPieces"]
	    self.appieceCollectedSound = ASSETS.appieceCollectedSound
	    self.appiecesCollected = self.level["level"]["startingPieces"]
	
	    self.gameOver = false
	    self.endOfWorldTimer = 3
	end
end

function Level:update(dt)
if self.EndofDialogue then
    self.timer = self.timer + dt
    if self.levelNum == 1 then
        self.cometTimer = self.cometTimer + dt
    end
    if self.start == false then
        if self.timer > 0.3 then
            if self.enter then --outro
                self.count = self.count -1
                if self.count ==0 then
                    if self.level["level"]["hasNext"] then
			GLOBAL.gameScreen.currLevel.song:stop()
			self.bossDefeatedJingle:stop()
                        GLOBAL.gameScreen.currLevel= Level(self.level["level"]["nextLevel"])
			GLOBAL.gameScreen.currLevel.song:play()
			self:clearAppieces()
			self:clearEnemies()
		    else
			GLOBAL.gameScreen.win = true	
		    end	
		else
		    self.intro=self.outro[self.count]
		end
	    else --intro
		self.intro = self.outro[self.count]
		self.count = self.count + 1
	        if self.count == 5 then
		    self.start= true
		    self.count = 1
	        end
	    end
	    self.timer =0.1
        end
    elseif self.start then
        --check boss defeated
        if self.bossDefeated then
	    if self.postBossTimer > 0 then
		self.postBossTimer = self.postBossTimer - dt
	    else
	        if self.level["level"]["hasNext"] then
	            GLOBAL.gameScreen.currLevel.song:stop()
		    self.bossDefeatedJingle:stop()
                    GLOBAL.gameScreen.currLevel= Level(self.level["level"]["nextLevel"])
		    GLOBAL.gameScreen.currLevel.song:play()
		    self:clearAppieces()
		    self:clearEnemies()
		else
		    GLOBAL.gameScreen.win = true	
		end	
	    end
	end
	--Animated background
        self.pic =  self.backgroundImage[self.backgroundshift]

	--Just to show scoring works
	for index, enemy in ipairs(self.enemies) do
	    enemy:update(dt)
	end

	if self.level["level"]["type"] == "overworld" then
	    if self.levelNum == 1 then
            --Comet update
    	    self.cometTimer = self.cometTimer + dt
            if self.cometTimer > 5 then
                local cx,cy = player:getPosition()
                self.comet:setNewComet(cx,cy)
                self.cometTimer = 0
            end
	    end

	    --Planet health update
	    if self.planetHealth > 0 then
    	        self.planetHealth = self.planetHealth - dt
	    end
	    if self.planetHealth <= 0 then
	        self:onPlanetDeath()
	        if self.gameOver then
		    if self.endOfWorldTimer > 0 then
	                self.endOfWorldTimer = self.endOfWorldTimer - dt
		    end
	            if self.endOfWorldTimer <= 0 then
		        player:onDeath()
	            end
	        end
	    end
        end
	--enemy waves
	--if self.numEnemies == 5 * self.wave  then
	    if self.wave >= 1  then
	        for i=1,self.numEnemies do
			if self.level["level"]["enemies"]["1"]["type"] == "boss" then
			self.location = {GLOBAL.WorldWidth/2, GLOBAL.WorldHeight/2}
			else 
			self.location = {math.random(GLOBAL.WorldWidth),math.random(GLOBAL.WorldHeight)}
			end
		    local e = Enemy(self.level["level"]["enemies"]["1"]["enemyimageTABLE"], 
                                    self.level["level"]["enemies"]["1"]["type"], 
                                    self.level["level"]["enemies"]["1"]["attackspeed"],
				    self.level["level"]["enemies"]["1"]["movementspeed"],
                                    self.level["level"]["enemies"]["1"]["hasDeath"],
                                    self.level["level"]["enemies"]["1"]["hasShield"],
                                    self.level["level"]["enemies"]["1"]["hasDamage"], self.location, 
                                    self.level["level"]["enemies"]["1"]["health"],500,
									self.level["level"]["enemies"]["1"]["rotate"])
		    table.insert(self.enemies, e)
		end

	    	self.wave = -1
	    end
	    if self.numEnemies==0 then
		self:levelComplete()
		self.numEnemies= -1
	    end
	--end

	--Update background
	if self.timer > 0.5 then
	    self.backgroundshift = self.backgroundshift + 1
	    if self.backgroundshift > self.level["level"]["backgroundframes"] then
	        self.backgroundshift = 1
	    end
	    if self.enter then
	        self.count = self.count + 1
		if self.count < 3 then
		    self.caveimg=self.clip[self.count]
		else 
		    self.count = 1
		    self.caveimg = self.clip[self.count]
		end
	    end
	    self.timer=0.1
	end

	if self.levelNum == 1 then
	    self.comet:update(dt)
	end
	self.cave:update(dt)
    end --end "if start"

    player:update(dt)
    for k,v in ipairs(appieces) do
        v:update(dt)
    end
else
    GLOBAL.currentState = Conversation(self.level["level"]["OpeningChat"])
    self.EndofDialogue = true
end
end

function Level:draw()
    self.camera:set()
    love.graphics.setColor(GLOBAL.color.white)
    love.graphics.draw(self.pic, 0, 0, 0, 1, 1)
    local x,y = player:getPosition()
		-- player
		player:draw()		
		if self.start == false  then
			love.graphics.draw(self.intro, self.camera:getX(), self.camera:getY(), 0, 1, 1 )
		end
		if self.start then
		-- enemies
		for index, enemy in ipairs(self.enemies) do
			enemy:draw()
		end
			GLOBAL.gameScreen.score:draw()
			if self.levelNum == 1 then
			    self.comet:draw()
			end
		end

	for k,v in ipairs(appieces) do
	    v:draw(dt)
	end
	self.cave:draw()
    self.camera:unset()
end

function Level:keypressed(key)
	if key == "z" then
		GLOBAL.currentState = GLOBAL.dialogueScreen
	elseif key == "q" then
		self.bossDefeatedJingle:stop()
        	GLOBAL.gameScreen.currLevel= Level(self.level["level"]["nextLevel"])
		GLOBAL.gameScreen.currLevel.song:play()
		self:clearAppieces()
		self:clearEnemies()
	end
	player:keypressed(key)
end

function Level:levelComplete()
	self.enter = true
end

function Level:onBossDefeated()
	self.bossDefeated = true
	player.currState = player.bossDefeatedState
	player.canMove = false
	self.postBossTimer = 3
	self.bossDefeatedJingle:play()
end

--Increment number of pieces in Score (to display and save)
--If number of pieces is a mult of 5, gain a new health
function Level:pieceCollected()
	if self.level["level"]["type"] == "overworld" then
		self.appieceModCounter = self.appieceModCounter + 1
		GLOBAL.gameScreen.score:pointer(10000)
		self.appiecesCollected = self.appiecesCollected + 1
    --GLOBAL.gameScreen.score.appiecesCollected = GLOBAL.gameScreen.score.appiecesCollected + 1
		print("Number of pieces found: "..self.appiecesCollected)
		if self.appieceModCounter == 5 then
			print("New Piece!")
			self.appieceModCounter = 0
			if player.currMaxHealth < player.maxHealth then
				print("More health!")
				player.currMaxHealth = player.currMaxHealth + 1
				player.health = player.currMaxHealth
			end
		end
    self.appieceCollectedSound:play()
	end
end

function Level:dropPiece(x,y)
	table.insert(appieces, Appiece(3,0.3,78,22,{x,y},self.appieceTotal))
	--self.appieceCounter = self.appieceCounter + 1
end

function Level:getPercentHealth()
	return self.planetHealth/self.maxPlanetHealth
end

function Level:getPlanetHealth()
	return self.planetHealth
end

function Level:onPlanetDeath()
    self.camera:setShake(40,5)
    self.gameOver = true
end

function Level:getAppiecesCollected()
    return self.appiecesCollected
end

function Level:clearAppieces()
    for k,v in ipairs(appieces) do
	table.remove(appieces, k)
    end
    self.appieceTotal = 0
end

function Level:clearEnemies()
    for k,v in ipairs(self.enemies) do
	table.remove(self.enemies, k)
    end
end

