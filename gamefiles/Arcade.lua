--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"
require "MiniGame"

Arcade = class()

function Arcade:__init(imageTABLE, location, game)
	local Quad = love.graphics.newQuad
--	self.rotate = rotate
	self.positionx = (GLOBAL.WorldWidth/2) * location[1]
	self.positiony = (GLOBAL.WorldHeight/2) * location[2]

	self.game = game

	--Image stuff
	self.imageTABLE = imageTABLE
	self.idleImage = love.graphics.newImage(self.imageTABLE["image"]["img"])
	self.idleFrame = self.imageTABLE["image"]["frame"]

	self.frame_width = self.idleImage:getWidth()/self.idleFrame
	self.sprite_sheet_width = self.idleImage:getWidth()
	self.sprite_sheet_height = self.idleImage:getHeight()

	self.width = self.frame_width
	self.height = self.sprite_sheet_height

	--Initialize animation
	self.quads = {}
	self.quads["Idle"] = {}
	for j = 1, self.idleFrame do
		self.quads["Idle"][j] = Quad((j-1)*self.frame_width, 1, self.frame_width, 
					     self.sprite_sheet_height, self.sprite_sheet_width, 
					     self.sprite_sheet_height)
	end

	self.state = "Idle"
	self.image = self.idleImage
--	self.currMaxFrame = self.idleFrame
	self.iteration = 1
	self.drawImage = true
	self.talkRadius = self.idleImage:getHeight() + 20
	self.timer = 0
	self.angle = 0
	self.gamePlayed = false
end

function Arcade:update(dt)
	self.timer=self.timer+dt
	--Step animation
	if self.timer > 0.2 then
		self.iteration = self.iteration + 1
		if self.iteration > self.idleFrame then
			self.iteration = 1
		end
		self.timer = 0.1
	end
end

function Arcade:draw()
	local distance = math.sqrt((player.positionx-self.positionx)^2 +
                                   (player.positiony-self.positiony)^2)
	if distance < self.talkRadius then
		love.graphics.setFont(ASSETS.menuFont)
		love.graphics.setColor(GLOBAL.color.white)
		if GLOBAL.inputMode == "mouse" then
			love.graphics.print("Right Click to play\n"..self.game, player.positionx, player.positiony+20)
		elseif GLOBAL.inputMode == "keyboard" then
			love.graphics.print("Press Z to play\n"..self.game, player.positionx, player.positiony+20)
		end
	end
	love.graphics.draw(self.image,self.quads[self.state][self.iteration],
		           self.positionx,self.positiony,math.pi/2,1,
                           1,self.frame_width/2,self.sprite_sheet_height/2)
end

function Arcade:keypressed(key)
    if key == "z" then
	local distance = math.sqrt((player.positionx-self.positionx)^2 +
                                   (player.positiony-self.positiony)^2)
	if distance < self.talkRadius then
		if not self.gamePlayed then
			if self.game == "End Game" then
				GLOBAL.gameScreen.currLevel.song:stop()
				GLOBAL.gameScreen.currLevel = LevelFour()
			else
				self.miniGame  = MiniGame(self.game)
				self.gamePlayed = true
				GLOBAL.gameScreen.currLevel.arcadeCount = GLOBAL.gameScreen.currLevel.arcadeCount + 1
				for i = 1,5 do
				    GLOBAL.gameScreen.currLevel:pieceCollected()
				end
				GLOBAL.gameScreen.currLevel.song:stop()
				GLOBAL.currentState = self.miniGame
			end
		else
			self.miniGame = MiniGame("Insert Token")
			GLOBAL.currentState = self.miniGame
		end
	end
     end
end

function Arcade:mousepressed(x,y,button)
    if button == "r" then
	local distance = math.sqrt((player.positionx-self.positionx)^2 +
                                   (player.positiony-self.positiony)^2)
	if distance < self.talkRadius then
		if not self.gamePlayed then
			if self.game == "End Game" then
				GLOBAL.gameScreen.currLevel.song:stop()
				GLOBAL.gameScreen.currLevel = LevelFour()
			else
				self.miniGame  = MiniGame(self.game)
				self.gamePlayed = true
				GLOBAL.gameScreen.currLevel.arcadeCount = GLOBAL.gameScreen.currLevel.arcadeCount + 1
				for i = 1,5 do
				    GLOBAL.gameScreen.currLevel:pieceCollected()
				end
				GLOBAL.gameScreen.currLevel.song:stop()
				GLOBAL.currentState = self.miniGame
			end
		else
			self.miniGame = MiniGame("Insert Token")
			GLOBAL.currentState = self.miniGame
		end
	end
     end
end

