--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"
require "Tail"
require "Bullet"
require "Bullets"
require "DirectionIndicator"
require "Collidable"

Player = class(Sprite)

function Player:__init(imagePath,frames,framerate,frame_height,
                       sprite_width,location)
    --Init Data
    self.positionx = location[1]
    self.positiony = location[2]
    self.direction = {0,0}
    self.maxSpeed = 170 -- max speed of sprite
    self.speed = 0 -- current speed of sprite
    self.reduceSpeed = true
    self.maxAccel = 25
    self.velocity = {x=0, y=0}
    self.angle = 0

    self.frame_width = sprite_width / frames
    self.width = self.frame_width
    self.height = frame_height

    self.radius = math.min(self.width,self.height)/2
    --Add collidable
    self.collider = Collidable(self.positionx,self.positiony,self.radius,GLOBAL.collIndex,"player",self)
    GLOBAL.collIndex = GLOBAL.collIndex + 1
    table.insert(WORLD.collidables, self.collider)

    --Sprite animation stuff
    local Quad= love.graphics.newQuad
    self.image = ASSETS.playerSprite
    self.damageImage = ASSETS.playerOuch
    self.fallImage = ASSETS.fallImage
    self.dieImage = ASSETS.playerDie
    self.bossDefeatedImage = ASSETS.playerProud

    --Sizing info for quads {frames, height, width}
    self.animSizes = { {6, 40, 253}, --idle
                       {4, 40, 172}, --ouch
		       {8, 43, 329}, --falling
		       {7, 40, 301}, --dead
		       {3, 43, 124}  --proud
		     }

    self.timer=0
    self.iteration=1
    self.painIteration = 1
    self.fallIteration = 1
    self.deathIteration = 1
    self.postDeathTimer = 0
    self.bossDefeatedIteration = 1
    self.dead = false
    self.state = "Classy"
    self.ouch = "Ouch"
    self.dyingState = "Dead"
    self.fallState = "Falling"
    self.bossDefeatedState = "Proud"
    self.currState = self.state
    self.canMove = true
    self.doDraw = true

    self.vulnerable = true
    self.offset=1.5
    self.ping=1
    self.pong=6
    self.pain=1
    self.fall=1

    self.frames=frames
    self.quads = {}
    self.quads[self.state] = {}
    self.quads[self.ouch] = {}
    self.quads[self.fallState] = {}
    self.quads[self.dyingState] = {}
    self.quads[self.bossDefeatedState] = {}
    --initialize quads
    for j = 1, self.animSizes[1][1] do
	local framewidth = self.animSizes[1][3]/self.animSizes[1][1]
        self.quads[self.state][j] = Quad((j-1)*framewidth,1,
                                         framewidth,self.animSizes[1][2],
                                         self.animSizes[1][3], self.animSizes[1][2])
    end
    for j = 1, self.animSizes[2][1] do
	local framewidth = self.animSizes[2][3]/self.animSizes[2][1]
        self.quads[self.ouch][j] = Quad((j-1)*framewidth,1,
                                         framewidth,self.animSizes[2][2],
                                         self.animSizes[2][3], self.animSizes[2][2])
    end
    for j = 1, self.animSizes[3][1] do
	local framewidth = self.animSizes[3][3]/self.animSizes[3][1]
        self.quads[self.fallState][j] = Quad((j-1)*framewidth,1,
                                         framewidth,self.animSizes[3][2],
                                         self.animSizes[3][3], self.animSizes[3][2])
    end
    for j = 1, self.animSizes[4][1] do
	local framewidth = self.animSizes[4][3]/self.animSizes[4][1]
        self.quads[self.dyingState][j] = Quad((j-1)*framewidth,1,
                                         framewidth,self.animSizes[4][2],
                                         self.animSizes[4][3], self.animSizes[4][2])
    end
    for j = 1, self.animSizes[5][1] do
	local framewidth = self.animSizes[5][3]/self.animSizes[5][1]
        self.quads[self.bossDefeatedState][j] = Quad((j-1)*framewidth,1,
                                         framewidth,self.animSizes[5][2],
                                         self.animSizes[5][3], self.animSizes[5][2])
    end
    --Player data
    self.maxHealth = 4
    self.currMaxHealth = 0
    self.health = 0

    self.segments = {ASSETS.segment1Sprite,ASSETS.segment2Sprite,ASSETS.segment3Sprite,ASSETS.segment4Sprite}
    self.sizes = {{31,31,187},{29,29,175},{23,23,139},{14,14,85}}

    --Player tail
    self.tail = {}
    local x,y = self:getPosition()
    for i = 1, self.maxHealth do
        self.tail[i] = Tail(0.1,self.sizes[i][1],self.sizes[i][2],i,x+i*20,y+i*20,
                            self.segments[i],self.sizes[i][3],self.sizes[i][1],1)
    end

    --Direction indicator
    self.directionIndicator = DirectionIndicator()

    self.shader = love.graphics.newShader("PlayerShader.glsl")
    self.shader:send("brightness",GLOBAL.brightness)

    self.b = bullets(250)
end

function Player:update(dt)
    --Update internal timer
    self.timer=self.timer+dt

    --check if dead
    if self.dead then
        self.postDeathTimer = self.postDeathTimer + dt
        if self.postDeathTimer >= 2 then
	    self.dead = false
	    self.positionx = GLOBAL.WorldWidth/2
	    self.positiony = GLOBAL.WorldHeight/2
	    self.canMove = true
	    self.doDraw = true
	    self.health = 0
	    self.currMaxHealth = 0
            GLOBAL.currentState = GLOBAL.continueScreen
        end
    end

    if self.canMove then
        local x, y = self:getPosition()
	local newX, newY --new position after input
	if GLOBAL.inputMode == "mouse" then
            --calculate mouse position
            mouse.x, mouse.y = love.mouse.getPosition()
            mouse.x = mouse.x + GLOBAL.gameScreen.currLevel.camera:getX()
            mouse.y = mouse.y + GLOBAL.gameScreen.currLevel.camera:getY()

            --get distance to mouse
            self.direction = {(mouse.y - y), (mouse.x - x)}
            local distance = math.sqrt(self.direction[1]^2 + self.direction[2]^2)

            -- make unit vector
            self.direction[1] = self.direction[1] / distance
            self.direction[2] = self.direction[2] / distance
            self.angle = math.atan2(self.direction[1], self.direction[2]) + math.pi/2

            -- velocity-based movement for inertia and acceleration
            local distX = mouse.x - x
            local distY = mouse.y - y
            local accelX = distX/(GLOBAL.CameraWidth/2) * self.maxAccel
            local accelY = distY/(GLOBAL.CameraHeight/2) * self.maxAccel

            self.velocity.x = self.velocity.x + accelX
            self.velocity.y = self.velocity.y + accelY

            if self.velocity.x > self.maxSpeed then
              self.velocity.x = self.maxSpeed
            elseif self.velocity.x < -1*(self.maxSpeed) then
              self.velocity.x = self.maxSpeed * -1
            end

            if self.velocity.y > self.maxSpeed then
              self.velocity.y = self.maxSpeed
            elseif self.velocity.y < -1*(self.maxSpeed) then
              self.velocity.y = self.maxSpeed * -1
            end

            newX = x + self.velocity.x*dt
            newY = y + self.velocity.y*dt

	elseif GLOBAL.inputMode == "keyboard" then
            local accelX = 0
            local accelY = 0

	    --if forward mouse down, accelerate in current direction
	    if love.keyboard.isDown("w") or love.keyboard.isDown("up") then
		self.reduceSpeed = false
                accelX = 5*math.cos(self.angle-math.pi/2)
                accelY = 5*math.sin(self.angle-math.pi/2)
	    elseif love.keyboard.isDown("s") or love.keyboard.isDown("down") then
		self.reduceSpeed = false
                accelX = -5*math.cos(self.angle-math.pi/2)
                accelY = -5*math.sin(self.angle-math.pi/2)
	    else
		self.reduceSpeed = true
		accelX = 0.1
		accelY = 0.1
	    end

	    if love.keyboard.isDown("a") or love.keyboard.isDown("left") then
		--rotate angle counterclockwise
                self.angle = self.angle - 5*dt
	    end
	    if love.keyboard.isDown("d") or love.keyboard.isDown("right") then
		self.angle = self.angle + 5*dt
	    end

	    if self.reduceSpeed then
		if (math.abs(self.velocity.x) - accelX) > 0 then
                    self.velocity.x = self.velocity.x - self.velocity.x*accelX
		else
		    self.velocity.x = 0
		end
		if (math.abs(self.velocity.y) - accelY) > 0 then
                    self.velocity.y = self.velocity.y - self.velocity.y*accelY
		else
		    self.velocity.y = 0
		end
	    else
                self.velocity.x = self.velocity.x + accelX
                self.velocity.y = self.velocity.y + accelY
	    end

            if self.velocity.x > self.maxSpeed + 50 then
              self.velocity.x = self.maxSpeed + 50
            elseif self.velocity.x < -1*(self.maxSpeed + 50) then
              self.velocity.x = -1 * (self.maxSpeed + 50)
            end

            if self.velocity.y > self.maxSpeed + 50 then
              self.velocity.y = self.maxSpeed + 50
            elseif self.velocity.y < -1*(self.maxSpeed + 50) then
              self.velocity.y = -1 * (self.maxSpeed + 50)
            end

            newX = x + self.velocity.x*dt
            newY = y + self.velocity.y*dt
    	end

        self.positionx,self.positiony = self:checkBorder(newX, newY)
		local camerax,cameray = GLOBAL.gameScreen.currLevel.camera:getPosition()
        --Update camera
        -- check if X would be outside of world
        if (newX+ GLOBAL.CameraWidth/2) > GLOBAL.WorldWidth then
            updateX = 0
            newPosX = GLOBAL.WorldWidth - GLOBAL.CameraWidth
        elseif (newX - GLOBAL.CameraWidth/2) < 0 then
            updateX = 0
            newPosX = 0
        else
            updateX = newX - x
            newPosX = newX - GLOBAL.CameraWidth/2
	    end
        
        -- check if Y would be outside of world
        if (newY + GLOBAL.CameraHeight/2) > GLOBAL.WorldHeight then
            updateY = 0
            newPosY = GLOBAL.WorldHeight - GLOBAL.CameraHeight
        elseif (newY - GLOBAL.CameraHeight/2) < 0 then
            updateY = 0
            newPosY = 0
        else
            updateY = newY - y
            newPosY = newY - GLOBAL.CameraHeight/2
        end

        GLOBAL.gameScreen.currLevel.camera:setPosition(newPosX, newPosY)
        GLOBAL.gameScreen.currLevel.camera:update(updateX, updateY)
    end
    --Set frame of animation
    if self.timer > 0.2 then
      if (self.currState == self.state) then
        self.pong=self.pong+1
        self.iteration=self.pong
        if self.pong>6 then
	        self.iteration=self.ping
          self.ping=self.ping-1
	        if self.ping<1 then
	           self.pong=1
             self.ping=6
          end
	      end
    elseif (self.currState == self.ouch) then
      self.painIteration=self.painIteration + 1
      if self.painIteration>4 then
        --set back to "Classy"
        self.painIteration=1
        self.currState = self.state
      end
    elseif (self.currState == self.fallingState) then
      self.fallIteration = self.fallIteration + 1
      if self.fallIteration>8 then
        --set back to "Classy"
        self.fallIteration=1
        self.currState = self.state
      end
    elseif (self.currState == self.dyingState) then
        self.deathIteration = self.deathIteration + 1
        if self.deathIteration>7 then
	    self.deathIteration = 1
            --set back to "Classy"
	    self.doDraw = false
	    self.dead = true
            self.currState = self.state
	    self.health = 0
        end
    elseif (self.currState == self.bossDefeatedState) then
        self.bossDefeatedIteration = self.bossDefeatedIteration + 1
        if self.bossDefeatedIteration>=3 then
	    self.bossDefeatedIteration = 3
            --set back to "Classy"
        end
    end
      self.timer = 0.1
    end

    --Update associated objects

    for i = 1,self.maxHealth do
        self.tail[i]:update(dt)
    end

    self.directionIndicator:update(dt)
    self.b:update(dt)
    self.collider:update(self.positionx,self.positiony)
end

function Player:draw()
--love.graphics.setShader(self.shader)
love.graphics.setShader()
love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness,255.0)
if self.doDraw then
    if not self.vulnerable then
--	love.graphics.circle("fill",self.positionx,self.positiony,self:getWidth()/2)
    end
    if (self.currState == self.state) then
	if self.doDraw then
            love.graphics.draw(self.image,self.quads[self.state][self.iteration],
                               self.positionx,self.positiony,self.angle,1,1,self:getWidth()/2,self:getHeight()/2)
	end
    elseif (self.currState == self.ouch) then
        love.graphics.draw(self.damageImage,self.quads[self.ouch][self.painIteration],
                           self.positionx,self.positiony,self.angle)
    elseif (self.currState == self.fallState) then
        love.graphics.draw(self.fallImage,self.quads[self.fallState][self.fallIteration],
                           self.positionx,self.positiony,self.angle)
    elseif (self.currState == self.dyingState) then
        love.graphics.draw(self.dieImage,self.quads[self.dyingState][self.deathIteration],
                           self.positionx,self.positiony,self.angle)
    elseif (self.currState == self.bossDefeatedState) then
        love.graphics.draw(self.bossDefeatedImage,self.quads[self.bossDefeatedState][self.bossDefeatedIteration],
                           self.positionx,self.positiony,0)
    end
    for i = 1, self.currMaxHealth do
        self.tail[i]:draw()
    end

--    if GLOBAL.inputMode == "keyboard" then
        self.directionIndicator:draw()
--    end
    self.b:draw()
end
--Draw post death fade out
love.graphics.setColor(0.0,0.0,0.0,(255/2)*self.postDeathTimer)
local x,y = GLOBAL.gameScreen.currLevel.camera:getPosition()
love.graphics.rectangle("fill",x,y,GLOBAL.CameraWidth,GLOBAL.CameraHeight)
love.graphics.setColor(GLOBAL.color.white)

end

--Switch to high score screen, reset things, save user score
function Player:onDeath()
    print("Dead")
    GLOBAL.gameScreen.score:record()
    GLOBAL.gameScreen.score.planetAlarm:stop()
    GLOBAL.gameScreen.currLevel.song:stop()
    local song = ASSETS.deathSound
    song:setVolume(GLOBAL.volume)
    song:play()
    self.currState = self.dyingState
    self.canMove = false
    self.health = 0
    self.currMaxHealth = 0
end

function Player:keypressed(key)
    if key == "]" or key == "i" then
        if player.vulnerable then
            player.vulnerable = false
        else
            player.vulnerable = true
        end
    end

    if key == " " or key == "return" then
        if self.health > 0 and self.health <= self.currMaxHealth then
            local z, w = self:getPosition()
            self.b:shoot(z,w)
            self.health = self.health - 1
        end
    end
end

function Player:mousepressed(x,y,button)
    if button == "l" then
        if self.health > 0 and self.health <= self.currMaxHealth then
            local z, w = self:getPosition()
            self.b:shoot(z,w)
            self.health = self.health - 1
        end
    end
end

function Player:getPosition()
    return self.positionx,self.positiony
end

function Player:getWidth()
    return self.width
end

function Player:getHeight()
    return self.height
end

function Player:getAngle()
    return self.angle
end

function Player:getDirection()
    return self.direction[1], self.direction[2]
end

function Player:damaged()
    local song = ASSETS.painSound
    song:setVolume(GLOBAL.volume)
    song:play()
    if self.currState ~= self.ouch then
		if player.vulnerable then
			self.health = self.health - 1
			self.currState = self.ouch
		end
    end
    --Check player alive
    if (self.health < 0) then
        self:onDeath()
    end
end

function round(x)
    if x%2 ~= 0.5 then
        return math.floor(x+0.5)
    end
    return x-0.5
end

function Player:checkCollision(c)
if (c.ctype == "comet") then
    --Only damage if on the last few frames
    local obj = c.object
    if obj.currState == obj.state and (obj.iteration <= obj.frames and obj.iteration >= obj.frames-1) then
	print("comet hit")
	c.active = false
        self:damaged()
    end
elseif (c.ctype == "enemyBullet") then
    --damage causing things
    if c.object.spawnBuffer <= 0 then
        self:damaged()
    end
    c.active = false
    c.object:contact()
elseif (c.ctype == "enemy") then
	print("enemy hit")
    if c.object.enemytype == "bomb" then
	self:damaged()
	c.object:damaged()
    elseif c.object.spawnBuffer <= 0 then
        self:damaged()
    end
--	c.active = false
elseif (c.ctype == "appiece") then
    --Collect appiece
    c.object.doDraw = false
    c.active = false
--    table.insert(collectedAppieces,c.object)
    print("Piece grabbed")
    --c.object:removeSelf()
    GLOBAL.gameScreen.currLevel:pieceCollected()
elseif (c.ctype == "cave") then
    if GLOBAL.gameScreen.currLevel.canLeave then
 	c.active = false
	c.object.collisionTimer = 3
	--draw confirmation screen
	GLOBAL.gameScreen.currLevel.confirmationScreen.active = true
	GLOBAL.gameScreen.currLevel.freezeWorld = true
    end
elseif (c.ctype == "npc") then
    local dirx = (self.positionx - c.object.positionx)/(math.abs(self.positionx-c.object.positionx))
    local diry = (self.positiony - c.object.positiony)/(math.abs(self.positiony-c.object.positiony))
    
    self.positionx = self.positionx + 10*dirx
    self.positiony = self.positiony + 10*diry
    self.velocity.x = -1*self.velocity.x
    self.velocity.y = -1*self.velocity.y
elseif (c.ctype == "border") then
    local dirx = (self.positionx - c.object.positionx)/(math.abs(self.positionx-c.object.positionx))
    local diry = (self.positiony - c.object.positiony)/(math.abs(self.positiony-c.object.positiony))
    
    self.positionx = self.positionx + 10*dirx
    self.positiony = self.positiony + 10*diry
    self.velocity.x = -1*self.velocity.x
    self.velocity.y = -1*self.velocity.y
end
end

function Player:checkBorder(x,y)
    --Left and right borders
    local retx,rety
    if (x - self.radius) <= 0 then
	retx = x + self.radius
	self.velocity.x = -self.velocity.x
    elseif (x + self.radius) >= GLOBAL.WorldWidth then
	retx = x - self.radius
	self.velocity.x = -self.velocity.x
    else
	retx = x
    end
    --Top and bottom borders
    if (y - self.radius) <= 0 then
	rety = y + self.radius
	self.velocity.y = -self.velocity.y
    elseif (y + self.radius) >= GLOBAL.WorldHeight then
	rety = y - self.radius
	self.velocity.y = -self.velocity.y
    else
	rety = y
    end

    return retx,rety

end
