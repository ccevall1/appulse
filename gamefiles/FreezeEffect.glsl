extern number brightness;

vec4 effect(vec4 color, Image img, vec2 tex, vec2 scr)
{
	vec4 c = Texel(img,tex);
	return c*vec4(1.0,1.0,1.0,0.5)*brightness;
}
