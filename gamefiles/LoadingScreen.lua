--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

LoadingScreen = class()

function LoadingScreen:__init()
	self.loadingText = "Loading"
	self.loadingTimer = 0
end

function LoadingScreen:draw()
    love.graphics.setBackgroundColor(GLOBAL.color.black)
    love.graphics.print(self.loadingText)
end

function LoadingScreen:update(dt)
	self.loadingTimer = self.loadingTimer + 1
	if self.loadingTimer > 0.5 then
		self.loadingText = self.loadingText .. "."
		self.loadingTimer = 0
	end
end

