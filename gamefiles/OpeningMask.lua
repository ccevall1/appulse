--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"

OpeningMask = class()

function OpeningMask:__init(shader)
    self.shader = shader
    self.timer = 0
    self.positionx = 0
    self.positiony = 0
    self.width = GLOBAL.WorldWidth
    self.height = GLOBAL.WorldHeight
    self.forward = true
    self.shader:send("brightness",GLOBAL.brightness)
end

function OpeningMask:update(dt)
    if self.forward then
	self.timer = self.timer + dt
    else
	self.timer = self.timer - dt
    end
    if self.timer > 1 then
	GLOBAL.gameScreen.currLevel.start = true
    end
    local cx,cy = GLOBAL.gameScreen.currLevel.camera:getPosition()
    self.shader:send("time",self.timer)
    if (player.positionx < self.width-GLOBAL.CameraWidth) then
        self.shader:send("width",math.min(GLOBAL.CameraWidth/2,math.min(player.positionx,self.width-player.positionx)))
    else
        self.shader:send("width",player.positionx - cx)
    end
    if (player.positiony > GLOBAL.CameraHeight/2) then
        self.shader:send("height",math.min(GLOBAL.CameraHeight/2,math.min(player.positiony,self.height-player.positiony)))
    else
        self.shader:send("height",GLOBAL.CameraHeight-player.positiony)
    end
    self.shader:send("posx",player.positionx/self.width)
    self.shader:send("posy",player.positiony/self.height)

end

function OpeningMask:draw()
	love.graphics.setShader(self.shader)
	love.graphics.rectangle("fill",self.positionx, self.positiony, self.width, self.height)
	love.graphics.setShader()
end

