--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "Collidable"

Comet = class()

-- imagePath is a mandatory string, but shape and location are not
function Comet:__init(image, framerate,frames)
    self.image = image
    self.frames = frames
    self.width = self.image:getWidth() / self.frames
    self.height = self.image:getHeight()
    local radius = math.min(self.width,self.height)/2

    self.cometx = 0 + (math.random(GLOBAL.CameraWidth) - GLOBAL.CameraWidth/2)
    self.comety = 0 + (math.random(GLOBAL.CameraHeight) - GLOBAL.CameraHeight/2)
   
    --Add collidable
    self.collider = Collidable(self.cometx,self.comety,radius,GLOBAL.collIndex,"comet",self)
    GLOBAL.collIndex = GLOBAL.collIndex + 1
    table.insert(WORLD.collidables, self.collider)

    --Sprite animation stuff
   local Quad= love.graphics.newQuad
   self.timer=0
   self.iteration=1
   self.state = "Classy"
   self.crash = "Crashed"
   self.currState = self.crash

   self.quads = {}
   self.quads[self.state] = {}
   for j = 1, self.frames do
        self.quads[self.state][j] = Quad((j-1)*self.width, 1, self.width, self.height, self.image:getWidth(), self.height)
   end
end

function Comet:update(dt)
if self.currState == self.state then
    --Update internal timer
    self.timer=self.timer+dt

    --Set frame of animation
    if self.timer>0.3 then
        self.iteration=self.iteration + 1
            if self.iteration>self.frames then
                self.currState = self.crash
                self.iteration = 1
            end
        if self.iteration == self.frames - 1 then
	    if GLOBAL.gameScreen.currLevel.levelNum == 1 then
                GLOBAL.gameScreen.currLevel.camera:setShake(10,3)
	    elseif GLOBAL.gameScreen.currLevel.levelNum == 3 then
                GLOBAL.gameScreen.currLevel.camera:setShake(5,3)
		GLOBAL.gameScreen.currLevel:lightWorld()
	    end
        end
        self.timer = 0.1
    end
    self.collider:update(self.cometx,self.comety)
end
end

function Comet:draw()
if self.currState == self.state then
    love.graphics.draw(self.image,self.quads[self.state][self.iteration],self.cometx,self.comety,0,1,1,self.width/2,self.height-20)
end
end

function Comet:setNewComet(playerx,playery)
    self.cometx = playerx + (math.random(GLOBAL.CameraWidth) - GLOBAL.CameraWidth/2)
    self.comety = playery + (math.random(GLOBAL.CameraHeight) - GLOBAL.CameraHeight/2)
    self.currState = self.state
    self.iteration = 1
end

function Comet:checkCollision(c)
end
