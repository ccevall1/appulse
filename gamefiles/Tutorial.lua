--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

Tutorial = class()

function Tutorial:__init()
 	self.time = 0
	--Tutorial Booleans
	self.howToMove = true
	self.firstTailSegment = true
	self.firstTimeDamaged = true
	self.firstAppiece = true
	self.showTutorial = true

	self.event = ""
end

function Tutorial:draw()
	local cx,cy = GLOBAL.gameScreen.currLevel.camera:getX(),GLOBAL.gameScreen.currLevel.camera:getY()
	love.graphics.setColor(GLOBAL.color.black)
	love.graphics.setFont(ASSETS.scoreFont)
    if self.event == "howToMove" then
	if GLOBAL.inputMode == "mouse" then
	    love.graphics.print("Appulse will follow your mouse (click to resume)",
				cx+100,cy+GLOBAL.CameraHeight/2)
	elseif GLOBAL.inputMode == "keyboard" then
	    love.graphics.print("Use wasd or arrow keys to move (space to resume)",
				cx+100,cy+GLOBAL.CameraHeight/2)
	end
    elseif self.event == "firstTailSegment" then
	if GLOBAL.inputMode == "mouse" then
	    love.graphics.print("Click to fire your segment at enemies (will regenerate)",
				cx+100,cy+GLOBAL.CameraHeight/2)
	elseif GLOBAL.inputMode == "keyboard" then
	    love.graphics.print("Press space to fire your segment (will regenerate)",
				cx+100,cy+GLOBAL.CameraHeight/2)
	end
    elseif self.event == "firstTimeDamaged" then
	    love.graphics.print("Firing or taking damage will make you lose a segment.\nBe careful not to get hit without it.",
				cx+100,cy+GLOBAL.CameraHeight/2)
    elseif self.event == "firstAppiece" then
	    love.graphics.print("You found a piece of your body!\nCollect 5 to get a new segment.\n Once you feel strong enough, go fight the boss!",
				cx+100,cy+GLOBAL.CameraHeight/2)
    end
end

function Tutorial:update(dt)
if self.showTutorial then
    --check event conditions
    if self.howToMove and GLOBAL.gameScreen.currLevel.start then
	self.event = "howToMove"
	player.canMove = true
	player:update(dt)
	GLOBAL.gameScreen.currLevel.freezeWorld = true
    end
    if self.firstTailSegment and player.health == 1 then
	--self.howToMove = false
	self.event = "firstTailSegment"
	GLOBAL.gameScreen.currLevel.freezeWorld = true
    elseif not self.firstTailSegment and self.firstTimeDamaged and player.health == 0 then
	self.event = "firstTimeDamaged"
	GLOBAL.gameScreen.currLevel.freezeWorld = true
    elseif self.firstAppiece then
	if GLOBAL.gameScreen.currLevel:getAppiecesCollected() > 0 then
	    self.howToMove = false
	    self.event = "firstAppiece"
	    GLOBAL.gameScreen.currLevel.freezeWorld = true
	end
    end
end
end

function Tutorial:keypressed(key)
if key == " " or key == "return" then
    if self.event == "howToMove" then
	self.howToMove = false
    elseif self.event == "firstTailSegment" then
	self.firstTailSegment = false
    elseif self.event == "firstTimeDamaged" then
	self.firstTimeDamaged = false
    elseif self.event == "firstAppiece" then
	self.firstAppiece = false
    end
    GLOBAL.gameScreen.currLevel.freezeWorld = false
end
self.event = ""
end

function Tutorial:mousepressed(x,y,button)
if button == "l" then
    if self.event == "howToMove" then
	self.howToMove = false
    elseif self.event == "firstTailSegment" then
	self.firstTailSegment = false
    elseif self.event == "firstTimeDamaged" then
	self.firstTimeDamaged = false
    elseif self.event == "firstAppiece" then
	self.firstAppiece = false
    end

    GLOBAL.gameScreen.currLevel.freezeWorld = false
end
self.event = ""
end
