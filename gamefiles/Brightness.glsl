extern number brightness;

vec4 effect(vec4 color, Image img, vec2 tex, vec2 scr)
{
	vec4 c = Texel(img,tex);
	return c*vec4(brightness,brightness,brightness,1.0);
}
