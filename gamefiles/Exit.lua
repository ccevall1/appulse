--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"
require "Sprite"
Exit = class(Sprite)
function Exit:__init(frame_width,frame_height,location)
    self.sprite = Sprite(frame_width,frame_height,location)
	self.clip = love.graphics.newImage("assets/images/LevelExit1.png")
	self.enter = false
    self.sprite.fixture:setUserData({type = "exit", instance = self})
	self.image = self.clip
	self.timer= 0.1
	self.flicker = 2
	self.x,self.y = GLOBAL.gameScreen.currLevel.player.sprite.body:getPosition()
end

function Exit:update(dt)
	self.timer=self.timer +dt
	if self.enter then
		if self.timer>0.2 then
			self.image = self.clip[self.flicker]
			self.flicker= self.flicker + 1
			if self.flicker == 3 then 
				self.flicker=1
			end
		
		end
	end
end

function Exit:draw()
love.graphics.draw(self.image, self.x ,self.y )
end
