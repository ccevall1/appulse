function love.conf(t)
	t.identity = nil
	t.version = "0.9.2"
	t.window.title = "Appulse"
	t.window.width = 864
	t.window.height = 646
	t.window.fsaa = 0
end
