--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

MainScreen = class()

function MainScreen:__init()
    self.title_string = "Menu"
    self.menu_options = {"New Game", "High Scores", "Options", "Quit"}
    self.currently_selected = 1 --which menu option is the player pointed at?
   --load graphic
    self.selector = ASSETS.menuSelectSprite

    self.backgroundImages = {ASSETS.mainScreenBG[1],ASSETS.mainScreenBG[2],
			     ASSETS.mainScreenBG[3],ASSETS.mainScreenBG[4]}

    self.song = ASSETS.mainScreenSong
    self.song:setLooping(true)

    self.selectSound = ASSETS.menuSelectSound
    self.selectSound:setLooping(false)
end

function MainScreen:draw()
    love.graphics.setBackgroundColor(GLOBAL.color.black)
    love.graphics.setColor(GLOBAL.color.white)
    love.graphics.setFont(ASSETS.menuFont)
    local width = self.backgroundImages[self.currently_selected]:getWidth()
    local height = self.backgroundImages[self.currently_selected]:getHeight()
    love.graphics.draw(self.backgroundImages[self.currently_selected],GLOBAL.CameraWidth/2,GLOBAL.CameraHeight/2,0,
		       1,1, width/2, height/2)
end

function MainScreen:update(dt)
self.song:setVolume(GLOBAL.volume)
self.selectSound:setVolume(GLOBAL.volume)
if GLOBAL.inputMode == "mouse" then
local x,y = love.mouse.getPosition()
local w,h = self.backgroundImages[self.currently_selected]:getDimensions()
local d = 0
local x1,x2,y1,y2 = 5*w/16,15*w/16,h,0

d = (x-x1)*(y2-y1) - (y-y1)*(x2-x1)
if d > 0 then --left of High Score top border
    self.currently_selected = 1
else
    x1,x2,y1,y2 = (w/4),w,h/2,h
    d = (x-x1)*(y2-y1) - (y-y1)*(x2-x1)
    local x_1,x_2,y_1,y_2 = (4*w/8),(15*w/16),h,0
    local d_1 = (x-x_1)*(y_2-y_1) - (y-y_1)*(x_2-x_1)
    if d_1 < 0 and d < 0 then
	self.currently_selected = 3
    else
	self.currently_selected = 2
    	x1,x2,y1,y2 = 3*w/4,w,h,h/4
	d = (x-x1)*(y2-y1) - (y-y1)*(x2-x1)
	if d < 0 then --right of Quit top border
	--    self.currently_selected = 3
        --else --else right of Quit top border
	    self.currently_selected = 4
        end
    end
end
end
end

function MainScreen:keypressed(key) --global function, check state
    if key == "down" or key == "s" then
        self.selectSound:play()
        if self.currently_selected < #ASSETS.mainScreenBG then
            self.currently_selected = self.currently_selected + 1
        else self.currently_selected = 1
        end
    elseif key == "up" or key == "w" then
        self.selectSound:play()
        if self.currently_selected > 1 then
            self.currently_selected = self.currently_selected - 1
        else self.currently_selected = #ASSETS.mainScreenBG
        end
    end

        if key == " " or key == "return" then
            --parse which menu option you are at
            local opt = self.currently_selected
            if opt == 1 then
                self.song:stop()
				WORLD.collidables = {}
                GLOBAL.gameScreen = GameScreen()
                GLOBAL.gameScreen.currLevel.song:play()
                GLOBAL.currentState = GLOBAL.gameScreen
            elseif opt == 2 then
                self.song:stop()
                GLOBAL.highScoreScreen.song:play()
                GLOBAL.currentState = GLOBAL.highScoreScreen
            elseif opt == 3 then
		GLOBAL.currentState = GLOBAL.optionsScreen
                --Options menu
            elseif opt == 4 then
                love.event.quit()
            end
            --self.currently_selected = 1
        end
end

function MainScreen:mousepressed(x,y,button)
if button == "l" then
	self:update(dt)
	self:draw()
    --parse which menu option you are at
    local opt = self.currently_selected
    if opt == 1 then
        self.song:stop()
	WORLD.collidables = {}
        GLOBAL.gameScreen = GameScreen()
        GLOBAL.gameScreen.currLevel.song:play()
        GLOBAL.currentState = GLOBAL.gameScreen
    elseif opt == 2 then
        self.song:stop()
        GLOBAL.highScoreScreen.song:play()
        GLOBAL.currentState = GLOBAL.highScoreScreen
    elseif opt == 3 then
	GLOBAL.currentState = GLOBAL.optionsScreen
        --Options menu
    elseif opt == 4 then
        love.event.quit()
    end
end
end

