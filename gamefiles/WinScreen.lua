--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

WinScreen = class()

function WinScreen:__init()
    --initialize menu options, background, click listeners?
    self.backgroundImage = ASSETS.winScreenBG
end

function WinScreen:draw()
    love.graphics.setBackgroundColor(GLOBAL.color.black)
    love.graphics.draw(self.backgroundImage,GLOBAL.CameraWidth/2, 0, 0, .7, .7,
                       self.backgroundImage:getWidth()/2, 0)
    
    love.graphics.setFont(ASSETS.winScreenFont)
    love.graphics.print("You found: "..GLOBAL.gameScreen.score.appiecesCollected.." Appieces",GLOBAL.CameraWidth/3,50)
    love.graphics.print("Your Score: "..GLOBAL.gameScreen.score.newScore,GLOBAL.CameraWidth/2.6, GLOBAL.CameraHeight/1.1)
end

function WinScreen:update(dt)

end

function WinScreen:keypressed(key)
    GLOBAL.currentState = GLOBAL.mainScreen
end

function WinScreen:mousepressed(x,y,button)
    GLOBAL.currentState = GLOBAL.mainScreen
end
