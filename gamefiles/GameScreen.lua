-- Main Game Screen
--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"
require "Score"
require "Camera"
require "Assets"
require "Level"

GameScreen = class()

function GameScreen:__init()
    --intializes the score
    self.score = Score("Margo")
    self.tutorial = Tutorial()
--    GLOBAL.highScoreScreen = HighScoreScreen()
    self.timer=0.1
    self.win=false;
    playerImage = ASSETS.playerSprite
	player = Player(playerImage, 6 , 0.1 , 40 , 253 , {GLOBAL.WorldWidth/2, GLOBAL.WorldHeight/2})
    --start level 1
    self.levelOne=Level("level1.txt")
    self.currLevel = self.levelOne
end

function GameScreen:update(dt)
    self.score:update(dt)
    if self.win then
		self.score:record()
        GLOBAL.currentState = GLOBAL.winScreen
    end
    self.timer = self.timer + dt
	self.currLevel:update(dt)
end

function GameScreen:draw()
    self.currLevel:draw()
end

function GameScreen:keypressed(key)
    if key == "escape" then
	--self.currLevel.song:stop()
        GLOBAL.currentState = GLOBAL.pauseScreen
    end
	self.currLevel:keypressed(key)
end

function GameScreen:mousepressed(x,y,button)
	self.currLevel:mousepressed(x,y,button)
end
function GameScreen:mousemoved(x,y,dx,dy)
end
