--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]

require "class"
require "Collidable"

Appiece = class()

function Appiece:__init(frames,framerate,sprite_width,sprite_height,location,index)
    --Sprite animation stuff
    local Quad= love.graphics.newQuad
    local pieceImage = math.random(#ASSETS.appieceSprites) --choose a sprite randomly
    self.image = ASSETS.appieceSprites[pieceImage]
    local frame_width = sprite_width / frames
    self.width = frame_width
    self.height = sprite_height

    self.timer=0
    self.iteration=1
    self.state = "Classy"
    self.positionx,self.positiony = location[1],location[2]
    self.index = index
    self.doDraw = true
    self.drawOnMiniMap = false
    local radius = 1.1*(math.min(self.width,self.height)/2)

    --Add collidable
    self.collider = Collidable(self.positionx,self.positiony,radius,GLOBAL.collIndex,"appiece",self)
    GLOBAL.collIndex = GLOBAL.collIndex + 1
    table.insert(WORLD.collidables, self.collider)

    self.quads = {}
    self.quads[self.state] = {}
     --initialize quads
    for j = 1, 3 do
        self.quads[self.state][j] = Quad((j-1)*frame_width,1,
                                         frame_width,sprite_height,
                                         sprite_width, sprite_height)
    end
end

function Appiece:update(dt)
    --Update internal timer
    self.timer=self.timer+dt
    --Set frame of animation
    if self.timer > 0.15 then
        self.iteration=self.iteration+1
        if self.iteration>3 then
            self.iteration=1
        end
	self.timer = 0
    end
--    self.collider:update(self.positionx,self.positiony)
end

function Appiece:draw()
if (self.doDraw) then
    love.graphics.draw(self.image,self.quads[self.state][self.iteration],
                       self.positionx,self.positiony,0,1.2,1.2,self.width/2,self.height/2)
    self.collider:draw()
end
end

--[[function Appiece:removeSelf()
    table.remove(GLOBAL.gameScreen.currLevel.appieces,self.index)
end
]]
function Appiece:checkCollision(c)
--[[if (c.ctype == "player") then
    self.active = false
    self.object.doDraw = false
end]]
end
