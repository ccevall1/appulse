--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "math"
require "class"

Worm = class()

function Worm:__init()
	love.mouse.setVisible(false)
	--Player table has i,j coordinate, direction, and past 4 moves
	self.player = {}
	self.player.x=1
	self.player.y=4
	self.player.direction="right"
	self.player.pastMoves = {{1,4},{1,4},{1,4},{1,4}}
	--Matrix of grid positions set to 0 or 1
	self.maxX = 10
	self.maxY = 10
	self.grid = {}
	for i = 1,self.maxX do
		self.grid[i] = {}
		for j = 3,self.maxY do
			self.grid[i][j] = 1
		end
	end
	for i = 1,self.maxX do
		for j = 1,3 do
			self.grid[i][j] = 0
		end
	end
	self.grid[3][1] = 0
	self.worldwidth,self.worldheight = love.window.getDimensions()
	self.width = self.worldwidth / self.maxX
	self.height = self.worldheight / self.maxY
--	self.height = self.width
	self.timer = 0
	self.score = 0
	self.numPieces = (self.maxX * self.maxY) - 30
	self.gameWon = false

	self.song = love.audio.newSource("assets/audio/hermitcrabtheme2.mid")
	self.song:setVolume(2*GLOBAL.volume)
	self.song:play()
end

function Worm:update(dt)
self.timer = self.timer + dt
if self.numPieces <= 0 then
	self.gameWon = true
end
if not self.gameWon then
    if self.timer > 0.3 then
	if self.player.direction == "up" then
		if self.player.y > 1 then
			self.player.y = self.player.y - 1
		end
	elseif self.player.direction == "down" then
		if self.player.y < self.maxY then
			self.player.y = self.player.y + 1
		end
	elseif self.player.direction == "left" then
		if self.player.x > 1 then
			self.player.x = self.player.x - 1
		end
	elseif self.player.direction == "right" then
		if self.player.x < self.maxX then
			self.player.x = self.player.x + 1
		end
	end
	if self.grid[self.player.x][self.player.y] == 1 then
		self.grid[self.player.x][self.player.y] = 0
		self.score = self.score + 100
		self.numPieces = self.numPieces - 1
	else
		self.score = self.score - 25
	end
	--update tail
	self:pushMove(self.player.x,self.player.y)

	self.timer = 0
    end
end
end

function Worm:draw()
	love.graphics.setShader()
	--Draw grid
	love.graphics.setColor(0.7*255*GLOBAL.brightness,0.5*255*GLOBAL.brightness,0.04*255*GLOBAL.brightness)
	for i = 1,self.maxX do
		for j = 1,self.maxY do
			if self.grid[i][j] == 1 then
				love.graphics.rectangle("fill",self.width*(i-1)+20,self.height*(j-1),self.width*0.7,self.height*0.7)
			end
		end
	end
	--Draw player
	love.graphics.setColor(255*GLOBAL.brightness,128*GLOBAL.brightness,0.0)
	love.graphics.rectangle("fill",self.width*(self.player.x-1)+20,self.height*(self.player.y-1),self.width*0.7,self.height*0.7)
	--Draw tail
	for i = 1,4 do
		love.graphics.rectangle("fill",self.width*(self.player.pastMoves[i][1] - 1) + 20,
						self.height*(self.player.pastMoves[i][2] - 1),
						self.width*0.7,
						self.height*0.7)
	end

	--Print text
	love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness)
	love.graphics.print("Classy Worm",self.worldwidth/2,10)
	love.graphics.print("Score: "..self.score,self.worldwidth/2,40)
	if self.gameWon then
		love.graphics.setColor(255,255,255)
		love.graphics.print("A winner is you!",self.worldwidth/2,self.worldheight/2)
	end
end

function Worm:keypressed(key)
	if key == "up" or key == "w" then
		self.player.direction = "up"
	elseif key == "down" or key == "s" then
		self.player.direction = "down"
	elseif key == "left" or key == "a" then
		self.player.direction = "left"
	elseif key == "right" or key == "d" then
		self.player.direction = "right"
	elseif key == "escape" then
		self.gameWon = true
	end
end

function Worm:pushMove(x,y)
	self.player.pastMoves[4] = self.player.pastMoves[3]
	self.player.pastMoves[3] = self.player.pastMoves[2]
	self.player.pastMoves[2] = self.player.pastMoves[1]
	self.player.pastMoves[1] = {x,y}
end

