--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"
require "Assets"

GameState = class()

function GameState:__init()
end

function GameState:draw()
    love.graphics.setColor(self.color)
    positionx, positiony = self.body:getWorldPoints(self.shape:getPoints())
    -- love.graphics.polygon("fill", self.body:getWorldPoints(self.shape:getPoints())) -- for debugging and seeing actual collision box
    love.graphics.draw(self.image, positionx, positiony, self.body:getAngle() + 1.57079633)
end
