	{
		"level": {
			"player": {
				"x": 1.1,
				"y": 1.8
			},
			"hasNext": true,
			"hasSpecialEffect":false,
			"song": "assets/audio/FinalBossMusic.ogg",
			"nextLevel": "level4.txt",
			"levelNum": 4,
			"backgroundframes": 1,
			"type": "underworld",
			"hasDialogue": true,
			"OpeningChat": "level4BOSSdialogue1.txt",
			"background": {
				"1": "assets/images/underground4.jpg"
			},
			"borders": {
				"hasBorders": false
			},
			"exit": {
				"image1": "assets/images/LevelExit1.png",
				"image2": "assets/images/LevelExit2.png",
				"x": 100000000,
				"y": 100000000
			},
			"planetHealth": 200,
			"startingPieces": 0,
			"enemies": {
				"numEnemies": 1,
				"numtypes": 1,
				"1": {
					"type": "boss",
					"health": 8,
					"number": 1,
					"movementspeed": 50,
					"rotate":true,
					"attackspeed": 400,
					"attackFrequency": 1.5,
					"attackradar": 1000,
					"hasDeath": true,
					"hasShield": false,
					"hasDamage": true,
					"hasAttack": false,
					"movement": "towards",
					"spawnsEnemies": false,
					"hasMultipleShots": true,
					"enemyimageTABLE": {
						"image": {
							"img": "assets/images/GiantLars.png",
							"frame": 8
						},
						"deathimage": {
							"img": "assets/images/DeathSmoke.png",
							"frame": 7
						},
						"damageimage": {
							"img": "assets/images/GiantLarsWince.png",
							"frame": 2
						},
						"shot": {
							"img": "assets/images/enemy_shot.png",
							"frame": 6,
							"shotTable": {
								"1": {
									"img": "assets/images/ChunkLevel1.png",
									"frame": 1
								},
								"2": {
									"img": "assets/images/ChunkLevel2.png",
									"frame": 1
								},
								"3": {
									"img": "assets/images/ChunkLevel3.png",
									"frame": 1
								},
								"4": {
									"img": "assets/images/ChunkLevel4.png",
									"frame": 1
								}
							}
						}
					},
					"spawnEnemyTABLE": {
						"type": "bug",
						"attackSpeed": 1000000,
						"movementSpeed": 100,
						"hasDeath": true,
						"hasShield": false,
						"hasDamage": false,
						"hasAttack": false,
						"attackRadar": 10000,
						"movement": "towards",
						"rotate": false,
						"image": {
							"img": "assets/images/LarvaOutline.png",
							"frame": 6
						},
						"deathimage": {
							"img": "assets/images/bugdeathSprite.png",
							"frame": 3
						},
						"shot": {
							"img": "assets/images/enemy_shot.png",
							"hasSmashImage": false,
							"frame": 6
						}
					}
				},
				"locations": {
					"1": {
						"x": 1,
						"y": 1
					}
				}
			},
			"numPieces": 0,
			"hasArcade": false,
			"NPC": {
				"numNPCs": 1,
				"1": {
					"imageTable": {
						"image": {
							"img": "assets/images/LarvaOutline.png",
							"frame": 6
						}
					},
					"dialogue": "level4BOSSdialogue1.txt",
					"location": {
						"x": 1,
						"y": 1
					},
					"rotate":true,
					"numPiecesGiven": 0,
					"typeNum": "1"
				}
			},
			"Object": {
				"hasObjects": false
			}
		}
	 }
