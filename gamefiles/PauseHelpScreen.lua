--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

PauseHelpScreen = class()

function PauseHelpScreen:__init()
    --initialize menu options, background, click listeners?
    self.backgroundImage = ASSETS.helpScreenBG
end

function PauseHelpScreen:draw()
    love.graphics.draw(self.backgroundImage,GLOBAL.CameraWidth/2, 0, 0, 1, 1,
                       self.backgroundImage:getWidth()/2, 0)
end

function PauseHelpScreen:update(dt)

end

function PauseHelpScreen:keypressed(key)
	GLOBAL.currentState = GLOBAL.pauseScreen
end

function PauseHelpScreen:mousepressed(x,y,button)
	GLOBAL.currentState = GLOBAL.pauseScreen
end

function PauseHelpScreen:mousemoved(x,y,dx,dy)
end
