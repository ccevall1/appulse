--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

ConfirmationScreen = class()

function ConfirmationScreen:__init()
	local Quad = love.graphics.newQuad
 	self.time = 0
	--ConfirmationScreen Boolean
	self.active = false
	self.choice = ""
	self.currSelect = "yes"
	self.timer = 0
	self.image = ASSETS.confirmationBox
	self.yes = ASSETS.yesImage
	self.no = ASSETS.noImage

	self.state = "Selected"

	self.yesquads = {}
	self.yesIter = 1
	self.noIter = 1
	self.yesquads[self.state] = {}
	self.noquads = {}
	self.noquads[self.state] = {}
	self.yesframewidth = self.yes:getWidth()/2
	self.noframewidth = self.no:getWidth()/2
	self.yeswidth = self.yes:getWidth()
	self.yesheight = self.yes:getHeight()
	self.nowidth = self.no:getWidth()
	self.noheight = self.no:getHeight()
	--Set up animations for yes and no
	for j = 1, 3 do
	    self.yesquads[self.state][j] = Quad((j-1)*self.yesframewidth,1,self.yesframewidth,
						self.yesheight,self.yeswidth,self.yesheight)
	    self.noquads[self.state][j] = Quad((j-1)*self.noframewidth,1,self.noframewidth,
						self.noheight,self.nowidth,self.noheight)
	end

end

function ConfirmationScreen:draw()
	local cx,cy = GLOBAL.gameScreen.currLevel.camera:getX(),GLOBAL.gameScreen.currLevel.camera:getY()
	love.graphics.setColor(GLOBAL.color.white)
	love.graphics.setFont(ASSETS.ConfirmationFont)
	if self.active then
	    love.graphics.draw(self.image,cx,cy)
	    if GLOBAL.inputMode == "keyboard" then
		love.graphics.setColor(GLOBAL.color.black)
		if GLOBAL.gameScreen.currLevel.levelType == "betweenworld" then
	        	love.graphics.print("Are you sure you want to go to the next planet? (y/n)",
				 	    cx+200,cy+GLOBAL.CameraHeight/2 - 25)
		else
	        	love.graphics.print("Are you sure you want to enter the boss's lair? (y/n)",
				 	    cx+200,cy+GLOBAL.CameraHeight/2 - 25)
		end
		love.graphics.setColor(GLOBAL.color.white)
	    else
		love.graphics.setColor(GLOBAL.color.black)
		if GLOBAL.gameScreen.currLevel.levelType == "betweenworld" then
	        	love.graphics.print("Are you sure you want to go to the next planet?",
					    cx+200,cy+GLOBAL.CameraHeight/2 - 25)
		else
	        	love.graphics.print("Are you sure you want to enter the boss's lair?",
					    cx+200,cy+GLOBAL.CameraHeight/2 - 25)
		end
		love.graphics.setColor(GLOBAL.color.white)
		love.graphics.draw(self.yes,self.yesquads[self.state][self.yesIter+1],
				   cx+GLOBAL.CameraWidth/2 - 100,cy + GLOBAL.CameraHeight/2)
		love.graphics.draw(self.no,self.noquads[self.state][self.noIter+1],
				   cx+GLOBAL.CameraWidth/2 + 100,cy + GLOBAL.CameraHeight/2)
		--love.graphics.draw(self.yes,cx + GLOBAL.CameraWidth/2 - 100,cy + GLOBAL.CameraHeight/2)
		--love.graphics.draw(self.no,cx + GLOBAL.CameraWidth/2 + 100,cy + GLOBAL.CameraHeight/2)
	    end
	end
end

function ConfirmationScreen:update(dt)
    self.timer = self.timer + dt
    local mx,my = love.mouse:getPosition()
    --mx = mx + GLOBAL.gameScreen.currLevel.camera:getX()
    if self.timer > 0.2 then
	if GLOBAL.inputMode == "mouse" then
            if mx < GLOBAL.CameraWidth/2 then
	        self.yesIter = (self.yesIter + 1) % 2
            else
	        self.noIter = (self.noIter + 1) % 2
            end
	elseif GLOBAL.inputMode == "keyboard" then
            if self.currSelect == "yes" then
	        self.yesIter = (self.yesIter + 1) % 2
            elseif self.currSelect == "no" then
	        self.noIter = (self.noIter + 1) % 2
            end
	end
	self.timer = 0
    end

    if self.active then
	if self.choice == "yes" then
	    GLOBAL.gameScreen.currLevel.freezeWorld = false
	    GLOBAL.gameScreen.currLevel.start = false
	    GLOBAL.gameScreen.currLevel.enter = true
	    self.active = false
	    self.choice = ""
	elseif self.choice == "no" then
	    GLOBAL.gameScreen.currLevel.freezeWorld = false
	    self.active = false
	    self.choice = ""
	    --move player back
	    player.velocity.x = -1.5*player.velocity.x
	    player.velocity.y = -1.5*player.velocity.y
	end
    else
	self.choice = ""
    end
end

function ConfirmationScreen:keypressed(key)
if self.active then
if key == "y" then
	self.choice = "yes"
	    GLOBAL.gameScreen.currLevel.freezeWorld = false
	    GLOBAL.gameScreen.currLevel.start = false
	    GLOBAL.gameScreen.currLevel.enter = true
elseif key == "n" then
	self.choice = "no"
end
if key == " " or key == "return" then
	if self.currSelect == "yes" then
	    self.choice = "yes"
	    GLOBAL.gameScreen.currLevel.freezeWorld = false
	    GLOBAL.gameScreen.currLevel.start = false
	    GLOBAL.gameScreen.currLevel.enter = true
	elseif self.currSelect == "no" then
	    self.choice = "no"
	end
end
end
end

function ConfirmationScreen:mousepressed(x,y,button)
if self.active then
if button == "l" then
	if x < GLOBAL.CameraWidth/2 then
		self.choice = "yes"
	    GLOBAL.gameScreen.currLevel.freezeWorld = false
	    GLOBAL.gameScreen.currLevel.start = false
	    GLOBAL.gameScreen.currLevel.enter = true
	else
		self.choice = "no"
	end
end
end
end
