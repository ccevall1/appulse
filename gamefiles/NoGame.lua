--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "math"
require "class"

NoGame = class()

function NoGame:__init()
	love.mouse.setVisible(false)
	self.timer = 0
	self.gameWon = true
	self.song = love.audio.newSource("assets/audio/babykill.ogg")
end

function NoGame:update(dt)
	self.timer = self.timer + dt
end

function NoGame:draw()
	if math.floor(self.timer) % 2 == 0 then
		love.graphics.setColor(GLOBAL.color.white)
		love.graphics.print("INSERT TOKEN TO PLAY",GLOBAL.CameraWidth/2,GLOBAL.CameraHeight/2)
		self.timer = 0
	end
end

function NoGame:keypressed(key)

end

