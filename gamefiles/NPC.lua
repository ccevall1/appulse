--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"

NPC = class()

function NPC:__init(imageTABLE, dialogue, location, numPiecesGiven,rotate, towards, speed, typeNum, enemyTable)
	local Quad = love.graphics.newQuad
	self.rotate = rotate
	self.towards = towards
	if(self.towards) then
		self.speed = speed
	end
	self.positionx = (GLOBAL.WorldWidth/2) * location[1]
	self.positiony = (GLOBAL.WorldHeight/2) * location[2]
	self.dialogue = dialogue
	self.conversation = Conversation(self.dialogue)
	self.numPiecesGiven = numPiecesGiven
	self.talkedTo = false
	self.active = true
	--Image stuff
	self.imageTABLE = imageTABLE
	self.idleImage = love.graphics.newImage(self.imageTABLE["image"]["img"])
	self.idleFrame = self.imageTABLE["image"]["frame"]
	self.talkBubbleImage = ASSETS.talkBubbleImage
	self.talkBubbleSizes = {3,48,163}
	self.currMaxFrame = self.idleFrame
	self.frame_width = self.idleImage:getWidth()/self.idleFrame
	self.sprite_sheet_width = self.idleImage:getWidth()
	self.sprite_sheet_height = self.idleImage:getHeight()
	self.width = self.frame_width
	self.height = self.sprite_sheet_height
	local r = math.max(self.frame_width,self.sprite_sheet_height)/2
	--Add collidable
	self.collider = Collidable(self.positionx,self.positiony,r,GLOBAL.collIndex,"npc",self)
	GLOBAL.collIndex = GLOBAL.collIndex + 1
	table.insert(WORLD.collidables, self.collider)
	--Initialize animation
	self.quads = {}
	self.quads["Idle"] = {}
	self.quads["Bubble"] = {}
	for j = 1, self.idleFrame do
		self.quads["Idle"][j] = Quad((j-1)*self.frame_width, 1, self.frame_width, 
					     self.sprite_sheet_height, self.sprite_sheet_width, 
					     self.sprite_sheet_height)
	end

	for j = 1, 3 do
		local framewidth = self.talkBubbleSizes[3] / self.talkBubbleSizes[1]
		self.quads["Bubble"][j] = Quad((j-1)*framewidth, 1, framewidth, 
					     self.talkBubbleSizes[2], self.talkBubbleSizes[3], 
					     self.talkBubbleSizes[2])
	end

	self.state = "Idle"
	self.image = self.idleImage
	self.currMaxFrame = self.idleFrame
	self.iteration = 1
	self.talkBubbleIteration = 1
	self.npcTimer = 0
	self.drawImage = true
	self.angle = 0
	self.direction = {0,0}
	self.talkRadius = self.idleImage:getWidth()/2

	self.typeNum = typeNum
	self.enemyTable = enemyTable
	self.enemyData = self.enemyTable[self.typeNum]
end

function NPC:update(dt)
	self.npcTimer=self.npcTimer+dt

	if self.talkedTo then
		GLOBAL.gameScreen.currLevel.cave.collider.active = true
	end

	--Step animation
	if self.npcTimer > 0.2 then
		self.iteration = self.iteration + 1
		self.talkBubbleIteration = self.talkBubbleIteration + 1
		if self.iteration > self.idleFrame then
			self.iteration = 1
		end
		if self.talkBubbleIteration > 3 then
			self.talkBubbleIteration = 1
		end
		self.npcTimer = 0.1
	end

	--if player within distance, aim at them and fire
	local distance = math.sqrt((player.positionx-self.positionx)^2 +
                                   (player.positiony-self.positiony)^2)
	if distance < self.talkRadius then
		self.direction = {(player.positionx-self.positionx),(player.positiony-self.positiony)}
		self.angle = math.atan2(self.direction[2],self.direction[1])
		self.towards = false
	end
	if distance > self.talkRadius and self.towards then
			self.direction = {(player.positionx-self.positionx),(player.positiony-self.positiony)}
			self.angle = math.atan2(self.direction[2],self.direction[1])
			self.positionx = (self.positionx + math.cos(self.angle)*self.speed*dt)
			self.positiony = (self.positiony + math.sin(self.angle)*self.speed*dt)
	end
	self:checkDialogue()
	self.collider:update(self.positionx,self.positiony)
end

function NPC:draw()
if (self.drawImage) then
	local distance = math.sqrt((player.positionx-self.positionx)^2 +
                                   (player.positiony-self.positiony)^2)
	if distance < self.talkRadius then
		love.graphics.setFont(ASSETS.menuFont)
		love.graphics.setColor(GLOBAL.color.white)
		if GLOBAL.inputMode == "keyboard" then
		    love.graphics.print("Press Z to talk", player.positionx, player.positiony+20)
		elseif GLOBAL.inputMode == "mouse" then
		    love.graphics.print("Right Click to Talk", player.positionx, player.positiony+20)
		end
	end

	if self.rotate then
		love.graphics.draw(self.image,self.quads[self.state][self.iteration],
			           self.positionx,self.positiony,self.angle+math.pi/2,1,
                                   1,self.frame_width/2,self.sprite_sheet_height/2)
		love.graphics.draw(self.talkBubbleImage,self.quads["Bubble"][self.talkBubbleIteration],
			           self.positionx+(self.width/3),self.positiony-self.height,0)
	else
		love.graphics.draw(self.image,self.quads[self.state][self.iteration],
			           self.positionx,self.positiony,self.angle-math.pi/2,1,
                                   1,self.frame_width/2,self.sprite_sheet_height/2)
	love.graphics.draw(self.talkBubbleImage,self.quads["Bubble"][self.talkBubbleIteration],
			           self.positionx+(self.width/3),self.positiony-self.height,0)

    end

end
end

function NPC:keypressed(key)
if key == "z" then
	local distance = math.sqrt((player.positionx-self.positionx)^2 +
                                   (player.positiony-self.positiony)^2)
	if distance < self.talkRadius and self.active then
		self.conversation.voice:play()
		GLOBAL.currentState = self.conversation
		self.talkedTo = true
	end
end
end

function NPC:mousepressed(x,y,button)
if button == "r" then
	local distance = math.sqrt((player.positionx-self.positionx)^2 +
                                   (player.positiony-self.positiony)^2)
	if distance < self.talkRadius and self.active then
		self.conversation.voice:play()
		GLOBAL.currentState = self.conversation
		self.talkedTo = true
	end
end
end

function NPC:checkDialogue()
if self.conversation.won then
	for i = 1,self.numPiecesGiven do
	    GLOBAL.gameScreen.currLevel:pieceCollected()
	end
	GLOBAL.gameScreen.currLevel.npcCollect = true
	self.conversation.won = false
elseif self.conversation.attacking then
	self:createEnemy()
end
end

function NPC:createEnemy()
if self.active then
	self.drawImage = false
	self.active = false
	--If talked to boss then build boss enemy
	local e = Enemy(self.enemyData["enemyimageTABLE"],
                        self.enemyData["type"],
                        self.enemyData["attackspeed"],
			self.enemyData["movementspeed"],
                        self.enemyData["hasDeath"],
                        self.enemyData["hasShield"],
                        self.enemyData["hasDamage"],
			self.enemyData["hasAttack"],
			{(2*self.positionx/GLOBAL.WorldWidth),(2*self.positiony/GLOBAL.WorldHeight)},
                        self.enemyData["health"],self.enemyData["attackradar"],
			self.enemyData["rotate"],
			1,
			false,
			self.enemyData["movement"],
			self.enemyData["spawnsEnemies"],
			self.enemyData["spawnEnemyTABLE"],
			self.enemyData["hasMultipleShots"],
			self.enemyData["attackFrequency"])
	table.insert(GLOBAL.gameScreen.currLevel.enemies,e)
	self.collider.active = false
end
end

function NPC:checkCollision(c)

end
