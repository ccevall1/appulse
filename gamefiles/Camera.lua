--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
--Camera class determines what pixels of the overall image to draw

Camera = class()

function Camera:__init(x,y)
    self.x, self.y = x,y
    self.shake = 0
    self.shakeTimer = 0
    self.bounds = {0,0,GLOBAL.CameraWidth, GLOBAL.CameraHeight}
end

function Camera:setPosition(x,y)
    self.x = x + (math.random(self.shake) - self.shake/2)
    self.y = y + (math.random(self.shake) - self.shake/2)
end

function Camera:getPosition()
    return self.x, self.y
end

function Camera:getX()
    return self.x
end

function Camera:getY()
    return self.y
end

function Camera:set()
    love.graphics.push()
    love.graphics.translate(-self.x,-self.y)
end

function Camera:unset()
    love.graphics.pop()
end

function Camera:update(dx,dy)
    self.x = self.x + dx
    self.y = self.y + dy
    if self.shakeTimer > 0 then
        self.shakeTimer = self.shakeTimer - 1
    elseif self.shakeTimer == 0 then
	self.shake = 0
    end
end

--Set amount to shake world for "length" frames
function Camera:setShake(shake,length)
    self.shake = shake
    self.shakeTimer = length
end
