--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "Collidable"

Border = class()

-- imagePath is a mandatory string, but shape and location are not
function Border:__init(image,x,y)
    self.image = love.graphics.newImage(image)
    self.width = self.image:getWidth()
    self.height = self.image:getHeight()
    self.radius = (math.min(self.width,self.height) / 2) - 10
    self.positionx = x*GLOBAL.WorldWidth/2
    self.positiony = y*GLOBAL.WorldHeight/2
   
    --Add collidable
    self.collider = Collidable(self.positionx,self.positiony,self.radius,GLOBAL.collIndex,"border",self)
    GLOBAL.collIndex = GLOBAL.collIndex + 1
    table.insert(WORLD.collidables, self.collider)
end

function Border:update(dt)
    --self.collider:update(self.x,self.y)
end

function Border:draw()
    love.graphics.draw(self.image,self.positionx,self.positiony,0,1,1,self.width/2,self.height/2)
    self.collider:draw()
end

function Border:checkCollision(c)
end
