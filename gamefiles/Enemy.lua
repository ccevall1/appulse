--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"
require "Bullet"

Enemy = class()

function Enemy:__init(imageTABLE, enemytype, attackSpeed, movementSpeed, hasDeath, 
		      hasShield, hasDamage,hasAttack, location, health,attackradar,
		      rotate,id,floater,movement,spawnsEnemies,spawnEnemyTABLE,hasMultipleShots,
		      attackFrequency,noPiece)
    local Quad = love.graphics.newQuad
	self.id= id
	self.positionx = location[1]*GLOBAL.WorldWidth/2
    self.positiony = location[2]*GLOBAL.WorldHeight/2
    self.startx = self.positionx
    self.starty = self.positiony
	self.distance = 100000000
	self.movement = movement
	self.spawnsEnemies = spawnsEnemies
	self.spawnEnemyTable = spawnEnemyTABLE
	self.amplitude = 100
	--Image stuff 
	self.frameMap = {} 
	self.imageTABLE = imageTABLE
	self.enemytype = enemytype
	self.hasDeath = hasDeath
	self.hasDamage = hasDamage
	self.hasShot = hasShot
	self.hasShield = hasShield
	self.hasAttack = hasAttack
	self.canSeePlayer = false
	if floater then
		self.floater = true
	else
		self.floater = false
	end
	self.idleImage = love.graphics.newImage(self.imageTABLE["image"]["img"])
	self.idleFrame = self.imageTABLE["image"]["frame"]
	self.currMaxFrame = self.idleFrame
	self.frame_width = self.idleImage:getWidth()/self.idleFrame
	self.sprite_sheet_width = self.idleImage:getWidth()
	self.sprite_sheet_height = self.idleImage:getHeight()
	local r = math.max(self.frame_width/2,self.sprite_sheet_height/2)
	    --Add collidable
    self.collider = Collidable(self.positionx,self.positiony,r,GLOBAL.collIndex,"enemy",self)
    GLOBAL.collIndex = GLOBAL.collIndex + 1
    table.insert(WORLD.collidables, self.collider)
	self.quads = {}
	self.frameMap["Idle"]= {}
	self.frameMap["Idle"]["height"]= self.sprite_sheet_height
	self.frameMap["Idle"]["width"]= self.sprite_sheet_height

	self.quads["Idle"] = {}
	for j = 1, self.idleFrame do
		self.quads["Idle"][j] = Quad((j-1)*self.frame_width, 1, self.frame_width, 
					     self.sprite_sheet_height, self.sprite_sheet_width, self.sprite_sheet_height)
	end
	
	if self.hasDeath then
		self.deathImage = love.graphics.newImage(self.imageTABLE["deathimage"]["img"])
		self.deathFrame= self.imageTABLE["deathimage"]["frame"]
		self.quads["Death"]={}
		self.frame_width = self.deathImage:getWidth()/self.deathFrame
		self.sprite_sheet_width = self.deathImage:getWidth()
		self.sprite_sheet_height = self.deathImage:getHeight()
		for j = 1, self.deathFrame do
		self.quads["Death"][j] = Quad((j-1)*self.frame_width, 1, self.frame_width, 
                                              self.sprite_sheet_height, self.sprite_sheet_width, self.sprite_sheet_height)
		end
		self.frameMap["Death"]= {}
		self.frameMap["Death"]["height"]= self.sprite_sheet_height
		self.frameMap["Death"]["width"]= self.sprite_sheet_height
	end
	
	if self.hasDamage then
		self.damageImage = love.graphics.newImage(self.imageTABLE["damageimage"]["img"])
		self.damageFrame= self.imageTABLE["damageimage"]["frame"]
		self.quads["Damage"]={}
				self.frame_width = self.damageImage:getWidth()/self.damageFrame
		self.sprite_sheet_width = self.damageImage:getWidth()
		self.sprite_sheet_height = self.damageImage:getHeight()
		for j = 1, self.deathFrame do
		self.quads["Damage"][j] = Quad((j-1)*self.frame_width, 1, self.frame_width, 
                                                self.sprite_sheet_height, self.sprite_sheet_width, self.sprite_sheet_height)
		end
		self.frameMap["Damage"]= {}
		self.frameMap["Damage"]["height"]= self.sprite_sheet_height
		self.frameMap["Damage"]["width"]= self.sprite_sheet_height
	end
	
	if self.hasAttack then
		self.attackImage = love.graphics.newImage(self.imageTABLE["attackimage"]["img"])
		self.attackFrame= self.imageTABLE["attackimage"]["frame"]
		self.quads["Attack"]={}
				self.frame_width = self.attackImage:getWidth()/self.attackFrame
		self.sprite_sheet_width = self.attackImage:getWidth()
		self.sprite_sheet_height = self.attackImage:getHeight()
		for j = 1, self.attackFrame do
		self.quads["Attack"][j] = Quad((j-1)*self.frame_width, 1, self.frame_width, 
                                               self.sprite_sheet_height, self.sprite_sheet_width, self.sprite_sheet_height)
		end
		self.frameMap["Attack"]= {}
		self.frameMap["Attack"]["height"]= self.sprite_sheet_height
		self.frameMap["Attack"]["width"]= self.sprite_sheet_height
	end
	
	
	if self.hasShield then
		self.shieldImage = love.graphics.newImage(self.imageTABLE["shieldimage"]["img"])
		self.shieldFrame= self.imageTABLE["shieldimage"]["frame"]
		self.recoverShieldImage = love.graphics.newImage(self.imageTABLE["recovershieldimage"]["img"])
		self.recoverShieldFrame = self.imageTABLE["recovershieldimage"]["frame"] 		
		self.quads["Shield"]={}
		self.frame_width = self.shieldImage:getWidth()/self.shieldFrame
		self.sprite_sheet_width = self.shieldImage:getWidth()
		self.sprite_sheet_height = self.shieldImage:getHeight()
		for j = 1, self.shieldFrame do
			self.quads["Shield"][j] = Quad((j-1)*self.frame_width,1,self.frame_width, 
                                                  self.sprite_sheet_height,self.sprite_sheet_width,self.sprite_sheet_height)
		end
		self.frameMap["Shield"]= {}
		self.frameMap["Shield"]["height"]= self.sprite_sheet_height
		self.frameMap["Shield"]["width"]= self.sprite_sheet_height
		self.quads["RecoverShield"]={}
		self.frame_width = self.recoverShieldImage:getWidth()/self.recoverShieldFrame
		self.sprite_sheet_width = self.recoverShieldImage:getWidth()
		self.sprite_sheet_height = self.recoverShieldImage:getHeight()
		for j = 1, self.recoverShieldFrame do
			self.quads["RecoverShield"][j] = Quad((j-1)*self.frame_width,1,self.frame_width,
                                                              self.sprite_sheet_height,self.sprite_sheet_width,
                                                              self.sprite_sheet_height)
		end
		self.frameMap["RecoverShield"]= {}
		self.frameMap["RecoverShield"]["height"]= self.sprite_sheet_height
		self.frameMap["RecoverShield"]["width"]= self.sprite_sheet_height
		--self.recoverTime = 0.5
		--self.recoverTimer = self.recoverTime
		
	end
	
	self.shot = love.graphics.newImage(self.imageTABLE["shot"]["img"])
	self.hasSmashImage = false
	self.hasMultipleShots = hasMultipleShots
	self.smashImage = ASSETS.segmentSmash
	self.smashFrames = 1

	if (self.imageTABLE["shot"]["hasSmashImage"] == true) then
		self.hasSmashImage = true
		self.smashImage = self.imageTABLE["shot"]["smashImage"]
		self.smashFrames = self.imageTABLE["shot"]["smashFrames"]
	end
	if (self.hasMultipleShots) then
		self.shotTable = {}
		for i = 1,4 do
			local img = love.graphics.newImage(self.imageTABLE["shot"]["shotTable"][""..i..""]["img"])
			table.insert(self.shotTable,img)
		end
	end

	self.shotFrame= self.imageTABLE["shot"]["frame"]
    self.bulletSpeed=attackSpeed;
    self.b={}
    --Sprite data
    self.maxHealth = health
    self.health = self.maxHealth
    self.speed = movementSpeed
    self.angle = 0
--    self.sprite.fixture:setUserData({type = "enemy", instance = self})
    self.drawImage = true
--	local x,y = self.sprite.body:getPosition()
    self.shooter= {}
    self.attackTimer= math.random(10)
    if not attackFrequency then
	self.attackFrequency = 4
    else
    	self.attackFrequency = attackFrequency
    end
    self.attackRadius = attackradar
    self.noPiece = noPiece
    if self.noPiece then
	self.droppedPiece = true
    else
    	self.droppedPiece = false
    end
	self.state = "Idle"
	self.image = self.idleImage
	self.currMaxFrame = self.idleFrame
	self.iteration = 1
	self.enemyTimer = 0
	self.moveTimer = math.random()
	self.rotate = rotate
	self.spawnBuffer = 3

	--If boss, knock player back
	if self.enemytype == "boss" then
		--based on opposite angle
		local dirx = (player.positionx - self.positionx)
		local diry = (player.positiony - self.positiony)
		player.velocity.x = 1000000000*player.maxSpeed*dirx
		player.velocity.y = 1000000000*player.maxSpeed*diry
	end

end

function Enemy:update(dt)
--Update timer
self.enemyTimer=self.enemyTimer+dt
self.moveTimer=self.moveTimer+dt
if self.spawnBuffer > 0 then
    self.spawnBuffer = self.spawnBuffer - dt
end
	--If dead, make sure you're dead
if self.health <= 0 then
	if not self.droppedPiece then
		self:onDeath() --drops a piece, deletes bullets, sets active to false
	end
	--Play death animation
	if self.hasDeath then
		self.state = "Death"
		self.image = self.deathImage
	else
		self.deathFrame = self.idleFrame
	end
	if self.enemyTimer > 0.2 then
		--Step death animation
		self.iteration = self.iteration + 1
		if self.iteration>self.deathFrame then
			self.iteration=1
			--Stop drawing enemy
			self.drawImage = false
			for i=1, self.maxHealth do
				GLOBAL.gameScreen.score:pointer(1000)
			end
			love.graphics.setColor( 255,255,255)
			self.health = 0
			self.state = "Idle"
		end
		self.enemyTimer = 0.1
	end
--Otherwise alive and trying to kill you
elseif self.health > 0 then
    if self.enemytype == "bomb" then
	if self.moveTimer >= 2.5 then
		self:damaged()
	end
    end

    if self.enemyTimer > 0.2 then
	self.iteration = self.iteration + 1
	if self.iteration > self.currMaxFrame then
		self.iteration = 1
		if self.state== "Damage" then
			self.state = "Idle"
			self.image = self.idleImage
			self.currMaxFrame = self.idleFrame
		elseif self.state == "RecoverShield" then
			self.state = "Shield"
			self.image = self.shieldImage
			self.currMaxFrame = self.shieldFrame
		elseif self.state =="Idle" and self.hasShield then
		    --if self.recoverTimer > 0 then
			--	self.recoverTimer = self.recoverTimer - dt
				  --  else
			self.state = "RecoverShield"
			self.image = self.recoverShieldImage 
			self.currMaxFrame = self.recoverShieldFrame
				    --end
		elseif self.state == "Attack" then
			self:attack()
			self.state = "Idle"
			--self.recoverTimer = self.recoverTime
			self.image = self.idleImage
			self.currMaxFrame = self.idleFrame
		end
			end
			self.enemyTimer = 0.1
		end
	end
	--if player within distance, aim at them and fire
	self.distance = math.sqrt((player.positionx-self.positionx)^2 +
                                   (player.positiony-self.positiony)^2)
	if self.distance < self.attackRadius then
		self:checkCanSeePlayer()
		local direction = {(player.positionx-self.positionx),(player.positiony-self.positiony)}
		self.angle = math.atan2(direction[2],direction[1])
		self.attackTimer=self.attackTimer+dt
		if self.attackTimer > self.attackFrequency then
		    if self.hasAttack then
			self.state = "Attack"
			self.iteration = 1
			self.image = self.attackImage
			self.currMaxFrame = self.attackFrame
		    else
			self:attack()
		    end
		--end
		self.attackTimer = 0
		end
		if self.movement == "lemniscate" then
			if self.collider.active then
				--else lemniscate
    				local newX, newY = self:lemniscate()
				newX = newX + self.startx
				newY = newY + self.starty
   	 			self.positionx,self.positiony = newX, newY
			end
		elseif self.movement == "towards" then
			if self.collider.active then
			    self.positionx = (self.positionx + math.cos(self.angle)*self.speed*dt)
			    self.positiony = (self.positiony + math.sin(self.angle)*self.speed*dt)
			end
		elseif self.movement == "bomb" then
			if self.collider.active then
    				local newX, newY = self:bomb()
				newX = newX + self.startx
				newY = newY + self.starty
    				self.positionx,self.positiony = newX, newY
			end
		else
			self.positionx = self.positionx
			self.positiony = self.positiony
	    	end
	else
		self.canSeePlayer = false
	end

	for i,v in ipairs(self.shooter) do
		v:update(dt)
	end
	self.collider:update(self.positionx,self.positiony)
end

function Enemy:draw()
	if (self.drawImage) then
		if GLOBAL.gameScreen.currLevel.levelNum == 4 or GLOBAL.gameScreen.currLevel.levelNum == 5 or GLOBAL.gameScreen.currLevel.levelNum == 6 then
			love.graphics.draw(self.image,self.quads[self.state][self.iteration],
			           self.positionx,self.positiony,0,1,
                                   1,self.frameMap[self.state]["width"]/2,self.frameMap[self.state]["height"]/2)
		else
		if self.rotate then
		    if self.floater and (self.distance < 300) then
			-- do nothing
		    else
			love.graphics.draw(self.image,self.quads[self.state][self.iteration],
			           self.positionx,self.positiony,self.angle+math.pi/2,1,
                                   1,self.frameMap[self.state]["width"]/2,self.frameMap[self.state]["height"]/2)
		    end
		else
		    if self.floater and (self.distance < 300) then
			-- do nothing
		    else
			love.graphics.draw(self.image,self.quads[self.state][self.iteration],
			           self.positionx,self.positiony,self.angle-math.pi/2,1,
                                   1,self.frameMap[self.state]["width"]/2,self.frameMap[self.state]["height"]/2)
		    end
		end
		end
		for i,v in ipairs(self.shooter) do
			v:draw()
		end
	end
end

function Enemy:damaged()
    print("Health left: "..self.health)
    if self.state ~= "Shield" then
		self.health = self.health - 1
	end
	if self.hasDamage then
		self.state = "Damage"
		self.currMaxFrame = self.damageFrame
		self.image= self.damageImage
	end
    if (self.health <= 0) then
        self.collider.active = false
    end

	self.iteration = 1
end

function Enemy:attack()
if self.collider.active and self.canSeePlayer then
	local bulletDx = self.bulletSpeed * math.cos(self.angle)
	local bulletDy = self.bulletSpeed * math.sin(self.angle)
	if self.hasMultipleShots then
	table.insert(self.shooter, bullet(self.positionx, self.positiony, bulletDx, 
					  bulletDy, self.shotTable[math.random(4)],"enemyBullet",1,
					  self.hasSmashImage,self.smashImage,self.smashFrames))
	else
	table.insert(self.shooter, bullet(self.positionx, self.positiony, bulletDx, 
					  bulletDy, self.shot,"enemyBullet",self.shotFrame,
					  self.hasSmashImage,self.smashImage,self.smashFrames))
	end
	if self.spawnsEnemies and self.attackTimer > 3 then
	    if GLOBAL.gameScreen.currLevel.numEnemies <  6 then
		if self.spawnEnemyTable["type"] == "fish" then
			table.insert(GLOBAL.gameScreen.currLevel.enemies, Enemy(self.spawnEnemyTable, 
						 self.spawnEnemyTable["type"], 
						 self.spawnEnemyTable["attackSpeed"], 
						 self.spawnEnemyTable["movementSpeed"], 
						 self.spawnEnemyTable["hasDeath"], 
						 self.spawnEnemyTable["hasShield"], 
						 self.spawnEnemyTable["hasDamage"],
						 self.spawnEnemyTable["hasAttack"], 
						 {0,0}, 
						 1,self.spawnEnemyTable["attackRadar"],
		     	 			 true,0,false,
						 self.spawnEnemyTable["movement"],
						 false,nil,false,10000000,true))
			GLOBAL.gameScreen.currLevel.numEnemies = GLOBAL.gameScreen.currLevel.numEnemies + 1
		else
			table.insert(GLOBAL.gameScreen.currLevel.enemies, Enemy(self.spawnEnemyTable, 
						 self.spawnEnemyTable["type"], 
						 self.spawnEnemyTable["attackSpeed"], 
						 self.spawnEnemyTable["movementSpeed"], 
						 self.spawnEnemyTable["hasDeath"], 
						 self.spawnEnemyTable["hasShield"], 
						 self.spawnEnemyTable["hasDamage"],
						 self.spawnEnemyTable["hasAttack"], 
						 {2*self.positionx/GLOBAL.WorldWidth,2*self.positiony/GLOBAL.WorldHeight}, 
						 1,self.spawnEnemyTable["attackRadar"],
		     	 			 false,0,false,
						 self.spawnEnemyTable["movement"],
						 false,nil,false,1000000,true))
			GLOBAL.gameScreen.currLevel.numEnemies = GLOBAL.gameScreen.currLevel.numEnemies + 1
		end
	    end
	end
end
end

function Enemy:onDeath()
	--Delete bullets
	for i,j in ipairs(self.shooter) do
		j:contact()
		j.collider.active = false
		self.shooter = {}
	end
	--Drop a piece
	self.droppedPiece = true
	--Set inactive
	self.collider.active = false
	--Decrement world count of enemies
	GLOBAL.gameScreen.currLevel.numEnemies = GLOBAL.gameScreen.currLevel.numEnemies - 1
	print("numEnemies: "..GLOBAL.gameScreen.currLevel.numEnemies)
--        self.drawImage = false
	if self.enemytype ~= "boss" then
		GLOBAL.gameScreen.currLevel:dropPiece(self.positionx,self.positiony)
	end
	if self.enemytype == "boss" then
	    GLOBAL.gameScreen.currLevel:onBossDefeated()
	end
end

function Enemy:lemniscate()
if self.collider.active then
	self.amplitude = 2 / (3 - math.cos(2*self.moveTimer))
	local newX = 100*self.amplitude*math.cos(self.moveTimer)
	local newY = 100*self.amplitude*math.sin(2*self.moveTimer) / 2
	return newX, newY
end
end

function Enemy:bomb()
if self.collider.active then
	self.amplitude = 2 / (3 - math.cos(2*self.moveTimer))
	local newX = 100*self.amplitude*math.cos(self.moveTimer)
	local newY = 100*self.amplitude*math.sin(2*self.moveTimer) / 2
	return newX, newY
end
end

function Enemy:checkCanSeePlayer()
--check midway point between player and enemy and see if collides with border
local midpoint = {(player.positionx - self.positionx)/2, (player.positiony - self.positiony)/2}
self.canSeePlayer = true
for i,object in ipairs(GLOBAL.gameScreen.currLevel.Objects) do
	local distance = math.sqrt((midpoint[1] - object.positionx)^2 + (midpoint[2] - object.positiony)^2)
	if distance < (object.collider.radius) then
		self.canSeePlayer = false
	end
end
end

function Enemy:checkCollision(c)
if (c.ctype == "comet") then
    local obj = c.object
    if obj.currState == obj.state and (obj.iteration <= obj.frames and obj.iteration >= obj.frames-1) then
	c.active = false
        self:damaged()
    end
elseif (c.ctype == "playerBullet") then
    self:damaged()
    c.active = false
    c.object.currState = "Smash"
elseif (c.ctype == "border") then
--    local dirx = math.cos(self.angle)
--    local diry = math.sin(self.angle)
--    self.positionx = self.positionx - 5*((dirx<0 and -1) or 1)
--    self.positiony = self.positiony - 5*((diry<0 and -1) or 1)
end
end
