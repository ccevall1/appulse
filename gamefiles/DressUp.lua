require "class"

DressUp = class()

function DressUp:__init()
	self.character = {}
	self.character.x = 0
	self.character.y = 100
	self.state = "skirts"
	   --sets the default choices to the skirts
	self.thumb = {}
	self.thumb.x = 600
	self.thumb.y = 100
	self.thumb.xo = 100
	   -- x offset
	self.thumb.yo = 100
	self.banner = {}
	self.banner.x = 5
	self.banner.y = 50
	self.banner.xo = 140
    self.banner.yo = 80

	-- default values for each dressable area
	self.background = love.graphics.newImage("assets/images/minigames/background1.png")
    self.base = love.graphics.newImage("assets/images/minigames/base.png")
	self.ears1 = love.graphics.newImage("assets/images/minigames/ears1.png")
	self.ears2 =love.graphics.newImage("assets/images/minigames/ears2.png")		  
	self.ears1Tab = love.graphics.newImage("assets/images/minigames/ears1_t.png")
	self.ears2Tab = love.graphics.newImage("assets/images/minigames/ears2_t.png")
	self.tail1 = love.graphics.newImage("assets/images/minigames/tail1.png")
    self.eyes1 = love.graphics.newImage("assets/images/minigames/eyes1.png")
	self.eyes2 = love.graphics.newImage("assets/images/minigames/eyes2.png")
	self.eyesTab1 =love.graphics.newImage("assets/images/minigames/eyes1_t.png")
	self.eyesTab2 = love.graphics.newImage("assets/images/minigames/eyes2_t.png")
	self.skirt1 = love.graphics.newImage("assets/images/minigames/skirt1.png")
	self.skirt1Tab = love.graphics.newImage("assets/images/minigames/skirt1_t.png")
	self.skirt2 = love.graphics.newImage("assets/images/minigames/skirt2.png")
	self.skirt2Tab = love.graphics.newImage("assets/images/minigames/skirt2_t.png")
	self.unknown = love.graphics.newImage("assets/images/minigames/untitled2.png")
	self.sleeves1 = love.graphics.newImage("assets/images/minigames/sleeves1.png")
	self.sleevesTab = love.graphics.newImage("assets/images/minigames/sleeves1_t.png")
	self.thumbone = self.skirt1Tab
	self.thumbtwo = self.skirt2Tab
	self.gameWon = true

	self.song = love.audio.newSource("assets/audio/babykill.ogg")
	self.skirts_b = love.graphics.newImage("assets/images/minigames/skirt_b.png")
	self.sleeves_b = love.graphics.newImage("assets/images/minigames/sleeves_b.png")
	self.ears_b = love.graphics.newImage("assets/images/minigames/ears_b.png")
	self.eyes_b = love.graphics.newImage("assets/images/minigames/eyes_b.png")
	self.eyes = self.unknown
	self.sleeves = self.unknown
	self.ears = self.unknown
	self.skirt = self.unknown
		
end

function DressUp:update(dt)
    self.mousex = love.mouse.getX()
	self.mousey = love.mouse.getY()
	if love.mouse.isDown("l") == true then
		if self.mousex > self.banner.x and self.mousex < self.banner.x + self.banner.xo and self.mousey > self.banner.y and self.mousey < self.banner.yo then	   
	      self.state = "skirts"
        end
		if self.mousex > self.banner.x + self.banner.xo and self.mousex < self.banner.xo + 2* self.banner.xo and self.mousey > self.banner.y and self.mousey < self.banner.yo then	   
	      self.state = "sleeves"
        end
		if self.mousex > self.banner.x + 2*self.banner.xo and self.mousex < self.banner.xo + 3*self.banner.xo and self.mousey > self.banner.y and self.mousey < self.banner.yo then	   
	      self.state = "ears"
        end
		if self.mousex > self.banner.x + 3*self.banner.xo and self.mousex < self.banner.xo + 4*self.banner.xo and self.mousey > self.banner.y and self.mousey < self.banner.yo then	   
	      self.state = "eyes"
        end
		if self.state == "skirts" then
			self.thumbone = self.skirt1Tab
			self.thumbtwo = self.skirt2Tab
			if self.mousex > self.thumb.x and self.mousex < self.thumb.x + self.thumb.xo and self.mousey > self.thumb.y and self.mousey < self.thumb.y + self.thumb.yo then	   
				self.skirt = self.skirt1		  
			end
			if self.mousex > self.thumb.x + self.thumb.xo and self.mousex < self.thumb.x + 2*self.thumb.xo and self.mousey > self.thumb.y and self.mousey < self.thumb.y + self.thumb.yo then	   
				self.skirt = self.skirt2
			end
		end
		if self.state == "sleeves" then
			self.thumbone = self.sleevesTab
			self.thumbtwo = self.unknown
	    if self.mousex > self.thumb.x and self.mousex < self.thumb.x + self.thumb.xo and self.mousey > self.thumb.y and self.mousey < self.thumb.y + self.thumb.yo then	   
	      self.sleeves = self.sleeves1		  
        end
		end
		if self.state == "ears" then
			self.thumbone = self.ears1Tab
			self.thumbtwo = self.ears2Tab
			if self.mousex > self.thumb.x and self.mousex < self.thumb.x + self.thumb.xo and self.mousey > self.thumb.y and self.mousey < self.thumb.y + self.thumb.yo then	   
				self.ears = self.ears1		  
			end
			if self.mousex > self.thumb.x + self.thumb.xo and self.mousex < self.thumb.x + 2*self.thumb.xo and self.mousey > self.thumb.y and self.mousey < self.thumb.y + self.thumb.yo then	   
				self.ears = self.ears2
			end
		end
		if self.state == "eyes" then
			self.thumbone = self.eyesTab1
			self.thumbtwo = self.eyesTab2
	    if self.mousex > self.thumb.x and self.mousex < self.thumb.x + self.thumb.xo and self.mousey > self.thumb.y and self.mousey < self.thumb.y + self.thumb.yo then	   
	      self.eyes = self.eyes1		  
        end
		if self.mousex > self.thumb.x + self.thumb.xo and self.mousex < self.thumb.x + 2*self.thumb.xo and self.mousey > self.thumb.y and self.mousey < self.thumb.y + self.thumb.yo then	   
	      self.eyes = self.eyes2 		  
        end
		end

	end
end

function DressUp:draw()
	love.graphics.draw(self.skirts_b, self.banner.x, self.banner.y)
	love.graphics.draw(self.sleeves_b, self.banner.x + self.banner.xo, self.banner.y)
	love.graphics.draw(self.ears_b, self.banner.x + 2*self.banner.xo, self.banner.y)
	love.graphics.draw(self.eyes_b, self.banner.x + 3*self.banner.xo, self.banner.y)
	love.graphics.draw(self.background,self.character.x, self.character.y)
	love.graphics.draw(self.base, self.character.x, self.character.y)
	love.graphics.draw(self.tail1,self.character.x, self.character.y)
	love.graphics.draw(self.eyes,self.character.x, self.character.y)

	love.graphics.draw(self.skirt,self.character.x, self.character.y)
	love.graphics.draw(self.sleeves,self.character.x, self.character.y)
	love.graphics.draw(self.ears,self.character.x, self.character.y)
	love.graphics.draw(self.thumbone, self.thumb.x, self.thumb.y, 0, 0.75, 0.75)
	love.graphics.draw(self.thumbtwo, self.thumb.x + self.thumb.xo, self.thumb.y)
	love.graphics.print("Click the tabs and pictures to dress your character!",50,10)
	love.graphics.print("Press any key to quit",50,GLOBAL.CameraHeight-50)

end
	

function DressUp:keypressed(key)
end


