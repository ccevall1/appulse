--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

LoseScreen = class()

function LoseScreen:__init()
    --initialize menu options, background, click listeners?
    self.backgroundImage = ASSETS.loseScreenBG
end

function LoseScreen:draw()
    love.graphics.setBackgroundColor(GLOBAL.color.black)
    love.graphics.draw(self.backgroundImage,GLOBAL.CameraWidth/2, 100, 0, 0.7, 0.7,
                       self.backgroundImage:getWidth()/2, 0)
    love.graphics.setFont(ASSETS.winScreenFont)
    love.graphics.setColor(GLOBAL.color.blue)
    love.graphics.print("You found: "..GLOBAL.gameScreen.score.appiecesCollected.." Appieces",GLOBAL.CameraWidth/3,50)
    love.graphics.print("Your Score: "..GLOBAL.gameScreen.score.newScore,GLOBAL.CameraWidth/2.5, GLOBAL.CameraHeight - 50)
end

function LoseScreen:update(dt)

end

function LoseScreen:keypressed(key)
	GLOBAL.highScoreScreen.song:play()
	GLOBAL.currentState = GLOBAL.highScoreScreen
end

function LoseScreen:mousepressed(key)
	GLOBAL.highScoreScreen.song:play()
	GLOBAL.currentState = GLOBAL.highScoreScreen
end

