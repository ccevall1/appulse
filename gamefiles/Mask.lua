--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"

Mask = class()

function Mask:__init(shader,lightShader)
    self.width = GLOBAL.WorldWidth
    self.height = GLOBAL.WorldHeight
    self.radius = 300
    self.innerRadius = 200
    self.shader = shader
    self.lightShader = lightShader
    self.lightTimer = 0
    self.maxLight = 0.5
    self.shader:send("radius",self.radius)
    self.shader:send("brightness",GLOBAL.brightness)
    if self.lightShader then
        self.lightShader:send("maxTime",self.maxLight)
        self.lightShader:send("brightness",GLOBAL.brightness)
    end
    self.positionx = 0
    self.positiony = 0
end

function Mask:update(dt)
--    self.positionx = GLOBAL.gameScreen.currLevel.camera:getX()
--    self.positiony = GLOBAL.gameScreen.currLevel.camera:getY()
    self.positionx = 0
    self.positiony = 0
    local cx,cy = GLOBAL.gameScreen.currLevel.camera:getPosition()
    if (player.positionx < self.width-GLOBAL.CameraWidth) then
        self.shader:send("width",math.min(GLOBAL.CameraWidth/2,math.min(player.positionx,self.width-player.positionx)))
    else
        self.shader:send("width",player.positionx - cx)
    end
    if (player.positiony > GLOBAL.CameraHeight/2) then
        self.shader:send("height",math.min(GLOBAL.CameraHeight/2,math.min(player.positiony,self.height-player.positiony)))
    else
        self.shader:send("height",GLOBAL.CameraHeight-player.positiony)
    end
    self.shader:send("posx",player.positionx/self.width)
    self.shader:send("posy",player.positiony/self.height)

    if self.lightTimer > 0 then
	self.lightTimer = self.lightTimer - dt
        self.lightShader:send("timeLeft",self.lightTimer)
    end
end

function Mask:draw()
    love.graphics.setColor(GLOBAL.color.black)
    if self.lightTimer > 0 then
        love.graphics.setShader(self.lightShader)
        love.graphics.rectangle("fill",self.positionx, self.positiony, self.width, self.height)
    else
        love.graphics.setShader(self.shader)
        --love.graphics.setColor(GLOBAL.color.black)
        love.graphics.rectangle("fill",self.positionx, self.positiony, self.width, self.height)
    end
--    love.graphics.setShader()
end

function Mask:lightWorld()
    self.lightTimer = self.maxLight
end
