-- Main Game Screen
--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"
require "Player"
require "Enemy"
require "Camera"
require "Assets"
require "Comet"
require "Conversation"
require "Appiece"
require "Cave"
require "Border"
require "NPC"
require "Mask"
require "Fog"
require "FreezeMask"
require "OpeningMask"
require "Arcade"
require "Leveltransition"
require "Tutorial"
require "ConfirmationScreen"

Level = class()

function Level:__init(file)
	--Clear every collider
	for k,v in ipairs(WORLD.collidables) do
		v.active = false
	end
	--Add back players
	player.collider.active = true
	player.vulnerable = true
	--Initialize level from file
	JSON = require("JSON")
	self.jsonrawtext= ""
	for line in love.filesystem.lines(file)do
		self.jsonrawtext= self.jsonrawtext..line
	end
	self.level = JSON:decode(self.jsonrawtext)
	self.levelNum = self.level["level"]["levelNum"]
	self.levelType = self.level["level"]["type"]
	self.bossDefeated = false
	self.bossCreated = false
	player.currState = player.state
	self.postBossTimer = 0
	self.bossDefeatedJingle = ASSETS.bossDefeatedJingle
	GLOBAL.currentLevel = self.levelNum

	--Initialize Camera Object
	self.camera = Camera(0,0)
    	self.shader = love.graphics.newShader("DarknessEffect.glsl")
    	self.lightShader = love.graphics.newShader("LightningEffect.glsl")
    	self.fogShader = love.graphics.newShader("FogEffect.glsl")
    	self.dreamShader = love.graphics.newShader("DreamyEffect.glsl")
	self.freezeShader = love.graphics.newShader("FreezeEffect.glsl")
	self.openingShader = love.graphics.newShader("OpeningEffect.glsl")

	self.mmeimg = love.graphics.newImage("assets/images/MiniMapEnemyIcon.png");
	self.mmplimg = love.graphics.newImage("assets/images/MiniMapPlayerIcon.png");
	self.mmpiimg = love.graphics.newImage("assets/images/MiniMapPieceIcon.png");
	self.mmnimg = love.graphics.newImage("assets/images/MiniMapNPCIcon.png");
	self.mmbimg = love.graphics.newImage("assets/images/MiniMapBossIcon.png");

	--Set Background
	self.backgroundImage={}
	for i = 1, self.level["level"]["backgroundframes"] do
		self.backgroundImage[i] =love.graphics.newImage(self.level["level"]["background"][""..i..""])
	end
	GLOBAL.WorldWidth = self.backgroundImage[1]:getWidth()
	GLOBAL.WorldHeight = self.backgroundImage[1]:getHeight()
	self.mask = Mask(self.shader,self.lightShader)
	self.fog = Fog(self.fogShader)
	self.dream = Mask(self.dreamShader,nil)
	self.freeze = FreezeMask(self.freezeShader)
	self.openingMask = OpeningMask(self.openingShader)
	self.freezeWorld = false

	self.backgroundshift = 1

	--Start Music
	self.song = love.audio.newSource(self.level["level"]["song"])
	self.song:setLooping(true)
	self.song:play()
	--Initialize mouse object
	mouse = {}
		mouse.x, mouse.y = love.mouse.getPosition()

	--Enemy Objects for testing
	self.enemies = {}
	self.numEnemies = self.level["level"]["enemies"]["numEnemies"]
	self.wave = 3

	--NPC characters
	self.NPCs = {}
	for i = 1,self.level["level"]["NPC"]["numNPCs"] do
		table.insert(self.NPCs, NPC(self.level["level"]["NPC"][""..i..""]["imageTable"],
				   self.level["level"]["NPC"][""..i..""]["dialogue"],
				   {self.level["level"]["NPC"][""..i..""]["location"]["x"],
				   self.level["level"]["NPC"][""..i..""]["location"]["y"]},
				   self.level["level"]["NPC"][""..i..""]["numPiecesGiven"],
				   self.level["level"]["NPC"][""..i..""]["rotate"],
				   self.level["level"]["NPC"][""..i..""]["towards"],
				   self.level["level"]["NPC"][""..i..""]["speed"],
				   self.level["level"]["NPC"][""..i..""]["typeNum"],
				   self.level["level"]["enemies"]))
	end

	--Arcades
	self.hasArcade = self.level["level"]["hasArcade"]
    	self.arcadeCount = 0
	self.arcades = {}
	if self.hasArcade then
		self.arcadeTable = self.level["level"]["Arcade"]
		for i = 1, self.arcadeTable["numArcades"] do
			table.insert(self.arcades,Arcade(self.arcadeTable[""..i..""]["imageTable"],
						{self.arcadeTable[""..i..""]["location"]["x"],
						self.arcadeTable[""..i..""]["location"]["y"]},
						self.arcadeTable[""..i..""]["game"]))
		end
	end

	--Collidable Objects in Scene
	self.Objects = {}
	if self.level["level"]["Object"]["hasObjects"] then
	for i = 1,self.level["level"]["Object"]["numObjects"] do
		table.insert(self.Objects, Border(self.level["level"]["Object"][""..i..""]["image"],
				   self.level["level"]["Object"][""..i..""]["location"]["x"],
				   self.level["level"]["Object"][""..i..""]["location"]["y"],
				   self.level["level"]["Object"][""..i..""]["rotate"]))
	end
	end

	--Internal timer for level
	self.timer = 0

	self.pic = self.backgroundImage[1]
	self.clip={}
	    self.clip[1]= love.graphics.newImage(self.level["level"]["exit"]["image1"])
	    self.clip[2]= love.graphics.newImage(self.level["level"]["exit"]["image2"])
	    self.caveimg=self.clip[1]
	--[[	self.outro = {}
		self.outro[1]= love.graphics.newImage("assets/images/Start.png")
		self.outro[2]= love.graphics.newImage("assets/images/Middle.png")
		self.outro[3]= love.graphics.newImage("assets/images/Middle2.png")
		self.outro[4]= love.graphics.newImage("assets/images/End.png")
		self.intro= self.outro[1]
	]]
	self.enter=false
	self.count = 1
	self.start=false
	self.round= false
	self.imgur = 1
-- Plus Appiece icon for whenever a new piece is added.
	self.collect = false
	self.collectTimer = 0.1
	self.plusAppiece = ASSETS.appiecePlus
	self.alpha = 255
	
	if self.levelType == "overworld" then
	--	player.currMaxHealth = 0
	end
	player.positionx = self.level["level"]["player"]["x"]*GLOBAL.WorldWidth/2
	player.positiony = self.level["level"]["player"]["y"]*GLOBAL.WorldHeight/2
	--Comet Object
	if self.levelNum == 1 then
	    self.cometTimer = 0
	    self.comet = Comet(ASSETS.cometSprite, 0.3, 9)
	elseif self.levelNum == 3 then
	    self.cometTimer = 0
	    self.lightTimer = 0
	    self.comet = Comet(ASSETS.lightningSprite, 0.3, 9)
	end

	--Planet Health
        self.maxPlanetHealth = self.level["level"]["planetHealth"] --to be read from file
        self.planetHealth = self.maxPlanetHealth

	--If overworld, initialize appieces and planet health
	self.cave = Cave(self.clip[1],self.clip[2],{self.level["level"]["exit"]["x"],self.level["level"]["exit"]["y"]})
	self.canLeave = true
	if self.levelType == "betweenworld" then
		self.canLeave = false
	end
	--Appiece setup
        appieces = {}
	collectedAppieces = {}
	if self.levelType == "overworld" then
	    self.appieceTotal = 1

	    --From file
	    if self.level["level"]["numPieces"] ~= 0 then
	        for i = 1, self.level["level"]["numPieces"] do
	            local posx = self.level["level"]["locations"][""..i..""]["x"]*(GLOBAL.WorldWidth/2)
	            local posy = self.level["level"]["locations"][""..i..""]["y"]*(GLOBAL.WorldHeight/2)
                    table.insert(appieces,Appiece(3,0.3,78,22,{posx,posy},i))
	        end
	    end
	    self.appieceModCounter = self.level["level"]["startingPieces"]
	    self.appieceCollectedSound = ASSETS.appieceCollectedSound
	    self.newPieceCollectedSound = ASSETS.newPieceCollectedSound
	    self.appiecesCollected = self.level["level"]["startingPieces"]

	    self.gameOver = false
	    self.endOfWorldTimer = 3
	end
	self.id = 1
	local numEnemies = self.level["level"]["enemies"]["numEnemies"]
	if numEnemies ~= 0 then
	for i=1, self.level["level"]["enemies"]["numtypes"] do
		for j = 1, self.level["level"]["enemies"][""..i..""]["number"] do
			--Specify locations
			self.location = {self.level["level"]["enemies"]["locations"][""..((self.id%numEnemies)+1)..""]["x"],
					 self.level["level"]["enemies"]["locations"][""..((self.id%numEnemies)+1)..""]["y"]}
			if (self.levelType == "overworld") or (self.levelNum == 5) or (self.levelNum == 6) then
			    local e = Enemy(self.level["level"]["enemies"][""..i..""]["enemyimageTABLE"],
                                    self.level["level"]["enemies"][""..i..""]["type"],
                                    self.level["level"]["enemies"][""..i..""]["attackspeed"],
				    self.level["level"]["enemies"][""..i..""]["movementspeed"],
                                    self.level["level"]["enemies"][""..i..""]["hasDeath"],
                                    self.level["level"]["enemies"][""..i..""]["hasShield"],
                                    self.level["level"]["enemies"][""..i..""]["hasDamage"],
				    self.level["level"]["enemies"][""..i..""]["hasAttack"],self.location,
                                    self.level["level"]["enemies"][""..i..""]["health"],500,
				    self.level["level"]["enemies"][""..i..""]["rotate"],self.id,
				    self.level["level"]["enemies"][""..i..""]["floater"],
				    self.level["level"]["enemies"][""..i..""]["movement"],
				    self.level["level"]["enemies"][""..i..""]["spawnsEnemies"],
				    self.level["level"]["enemies"][""..i..""]["spawnEnemyTABLE"],
				    self.level["level"]["enemies"][""..i..""]["hasMultipleShots"],
				    self.level["level"]["enemies"][""..i..""]["attackFrequency"])
		    		self.enemies[e.id]= e
				self.id = self.id + 1
			end

		end
	end
	end
	
	if not (self.levelNum == 1) then
		GLOBAL.gameScreen.tutorial.howToMove = false
		GLOBAL.gameScreen.tutorial.firstTailSegment = false
		GLOBAL.gameScreen.tutorial.firstTimeDamaged = false
		GLOBAL.gameScreen.tutorial.firstAppiece = false
	end
	if self.levelType == "underworld" then
		GLOBAL.gameScreen.tutorial.showTutorial = false
	end
	self.confirmationScreen = ConfirmationScreen()
	player.canMove = true

end

function Level:update(dt)

self.song:setVolume(GLOBAL.volume)

if self.freezeWorld == false then
    self.timer = self.timer + dt
	
    if self.levelNum == 1 then
        self.cometTimer = self.cometTimer + dt
    elseif self.levelNum == 3 then
        self.cometTimer = self.cometTimer + dt
	if self.lightTimer > 0 then
            self.lightTimer = self.lightTimer - dt
	end
    end
    if self.start == false then
		--        if self.timer > 0.3 then
            if self.enter then --outro
                if self.level["level"]["hasNext"] then
				GLOBAL.gameScreen.currLevel.song:stop()
				self.bossDefeatedJingle:stop()
				self:clearAppieces()
				self:clearEnemies()
				if self.levelType == "betweenworld" then
					GLOBAL.currentState = LevelTransition()
				else
					GLOBAL.gameScreen.currLevel= Level(self.level["level"]["nextLevel"])
				end
		else
		    GLOBAL.gameScreen.win = true
		end
	    else --intro
	--[[		self.intro = self.outro[self.count]
			self.count = self.count + 1
		        if self.count == 5 then
			    self.start= true
			    self.count = 1
		        end
	]]
		self.openingMask:update(dt)
	    end
	    self.timer =0.1
		--        end
    elseif self.start then
        --check boss defeated
        if self.bossDefeated then
	    player.vulnerable = false
	    if self.levelNum == 5 then
		--skip win jingle because final boss is too epic for that
		self.postBossTimer = 0
	    end
	    if self.postBossTimer > 0 then
		self.postBossTimer = self.postBossTimer - dt
	    else
	        if self.level["level"]["hasNext"] then
	            GLOBAL.gameScreen.currLevel.song:stop()
				self.bossDefeatedJingle:stop()
				self:clearAppieces()
				self:clearEnemies()
                    GLOBAL.gameScreen.currLevel= Level(self.level["level"]["nextLevel"])

		else
				GLOBAL.gameScreen.win = true
		end
	    end
	end
	--Animated background
        self.pic =  self.backgroundImage[self.backgroundshift]

	--Update characters in scene
	for index, enemy in ipairs(self.enemies) do
	    enemy:update(dt)
	end
	for index, npc in ipairs(self.NPCs) do
	    npc:update(dt)
	end
	for index, object in ipairs(self.Objects) do
	    object:update(dt)
	end

	--Check if allowed to leave (talked to NPC)
	if self.levelType == "betweenworld" then
	    if self.NPCs[1].talkedTo then
		self.canLeave = true
	    end
	end

	if self.levelType == "overworld" then
	    if self.levelNum == 1 or self.levelNum == 3 then
                --Comet update
    	        self.cometTimer = self.cometTimer + dt
                if self.cometTimer > 10 then
                    local cx,cy = player:getPosition()
                    self.comet:setNewComet(cx,cy)
                    self.cometTimer = 0
                end
	    end

	    --Planet health update
	    if self.planetHealth > 0 and self.levelNum ~= 4 then
    	        self.planetHealth = self.planetHealth - dt
	    end
	    if self.planetHealth <= 0 then
	        self:onPlanetDeath()
	        if self.gameOver then
		    if self.endOfWorldTimer > 0 then
	                self.endOfWorldTimer = self.endOfWorldTimer - dt
		    end
	            if self.endOfWorldTimer <= 0 then
		        player:onDeath()
	            end
	        end
	    end
        end
	--enemy waves
	--if self.numEnemies == 5 * self.wave  then
	if self.numEnemies==0 then
	    self:levelComplete()
	    self.numEnemies= -1
	end
	--end
if self.collect then
	self.collectTimer = self.collectTimer + dt
	if(self.collectTimer > 0.3) then
		self.alpha = self.alpha - 100
		if self.alpha < 0 then
			self.collect = false
		end
		self.collectTimer = 0.1
	end
end

	--Update background
	if self.timer > 0.5 then
	    self.backgroundshift = self.backgroundshift + 1
	    if self.backgroundshift > self.level["level"]["backgroundframes"] then
	        self.backgroundshift = 1
	    end
	    if self.enter then
	        self.count = self.count + 1
		if self.count < 3 then
		    self.caveimg=self.clip[self.count]
		else
		    self.count = 1
		    self.caveimg = self.clip[self.count]
		end
	    end
	    self.timer=0.1
	end

	if self.levelNum == 1 or self.levelNum == 3 then
	    self.comet:update(dt)
	end
	self.cave:update(dt)
    end --end "if start"

    --Update mask
    if (self.levelNum == 3 and self.levelType == "overworld") then
        self.mask:update(dt)
    end
    if (self.levelNum == 2 and self.levelType == "overworld") then
        self.fog:update(dt)
    end
    if (self.levelNum == 4 and self.levelType == "overworld") then
        self.dream:update(dt)
    end

    player:update(dt)
    for k,v in ipairs(appieces) do
        v:update(dt)
    end

    for k,v in ipairs(self.arcades) do
	v:update(dt)
    end

    --Update level 1 tutorial checker
    if self.levelNum == 1 then
        GLOBAL.gameScreen.tutorial:update(dt)
    end

    --End of game check
    if self.arcadeCount == 3 then
	table.insert(self.arcades,Arcade(self.arcadeTable["1"]["imageTable"],
						{1,1},
						"End Game"))
	self.arcadeCount = 4 --check that it doesn't create it again
    end
else
    if self.levelNum == 1 then
        GLOBAL.gameScreen.tutorial:update(dt)
    end
    self.confirmationScreen:update(dt)
end
end

function Level:draw()
	self.camera:set()
	love.graphics.draw(self.pic, 0, 0, 0, 1, 1)
			-- collidable objects
		for index, object in ipairs(self.Objects) do
			object:draw()
		end
		player:draw()

		self.cave:draw()
	if self.start == false  then
		--love.graphics.setShader()
		self.openingMask:draw()
	elseif self.start then
		--draw background
	--	love.graphics.draw(self.pic, 0, 0, 0, 1, 1)

		local x,y = player:getPosition()
		if self.collect then
			local oldColor = love.graphics.getColor()
			love.graphics.setColor(255,255,255,self.alpha)
			love.graphics.draw(self.plusAppiece,x,y, 0, 1,1, player:getWidth(),2* player:getHeight())
			love.graphics.setColor(255,255,255,255)
		end
		-- player
		--self.cave:draw()
		--player:draw()

		love.graphics.setShader(GLOBAL.brightnessShader)
		-- enemies
		for index, enemy in ipairs(self.enemies) do
			enemy:draw()
		end
		-- npcs
		for index, npc in ipairs(self.NPCs) do
			npc:draw()
		end


		if self.levelNum == 1 or self.levelNum == 3 then
		    self.comet:draw()
		end

		for k,v in ipairs(appieces) do
	    		v:draw()
		end

		for k,v in ipairs(self.arcades) do
	    		v:draw()
		end
	end

    love.graphics.setShader()
    if (self.levelNum == 3 and self.levelType == "overworld") then
	self.mask:draw()
    end
    if (self.levelNum == 2 and self.levelType == "overworld") then
	self.fog:draw()
    end
    if (self.levelNum == 4 and self.levelType == "overworld") then
	self.dream:draw()
    end


    --love.graphics.setShader()
    love.graphics.setShader(GLOBAL.brightnessShader)
    for index, enemy in ipairs(self.enemies) do
	if enemy.floater then
		enemy:draw()
	end
    end
    love.graphics.setShader()
    GLOBAL.gameScreen.score:draw()
    --Draw all colliders
    for k,v in ipairs(WORLD.collidables) do
        v:draw()
    end

    if self.freezeWorld then
	self.freeze:draw()
	if self.levelNum == 1 then
	    GLOBAL.gameScreen.tutorial:draw()
	end
        self.confirmationScreen:draw()
    end

	--Regeneration Timer
	local cx, cy = self.camera:getPosition()
	love.graphics.setFont(ASSETS.scoreFont)
	love.graphics.setColor(GLOBAL.brightness,GLOBAL.brightness,GLOBAL.brightness)
	if player.health == player.currMaxHealth then
		love.graphics.print("",cx+GLOBAL.CameraWidth/4,cy+10)
	elseif player.tail[player.health+1] then
	    --if player.tail[player.health+1].regenTimer < 3 then
		love.graphics.print(string.format("Next Segment in %.0f",(3-player.tail[player.health+1].regenTimer)),
				    cx+GLOBAL.CameraWidth/4,cy+10)
	    --end
	elseif player.health == 4 then
		love.graphics.print(string.format("Next Segment in %.0f",(3-player.tail[player.health].regenTimer)),
				    cx+GLOBAL.CameraWidth/4,cy+10)
	else
		love.graphics.print("",cx+GLOBAL.CameraWidth/4,cy+10)
	end

    self.camera:unset()

	love.graphics.setShader()
	-- draw the minimap
	local mmwidth = 250
	local mmheight = mmwidth * GLOBAL.WorldHeight/GLOBAL.WorldWidth
	local mmvieww = mmwidth * GLOBAL.CameraWidth/GLOBAL.WorldWidth
	local mmviewh = mmheight * GLOBAL.CameraHeight/GLOBAL.WorldHeight
	local mmplayerw, mmplayerh = self.camera:getPosition()
	mmplayerw = mmwidth - (mmwidth * mmplayerw/GLOBAL.WorldWidth)
	mmplayerh = mmheight - (mmheight * mmplayerh/GLOBAL.WorldHeight)
	love.graphics.setColor(0.0,0.0,0.0,64*GLOBAL.brightness)
	love.graphics.rectangle("fill", GLOBAL.CameraWidth - mmwidth, GLOBAL.CameraHeight - mmheight, mmwidth, mmheight)
	love.graphics.setColor(0.0,0.0,0.0,128*GLOBAL.brightness)
	love.graphics.rectangle("line", GLOBAL.CameraWidth - mmwidth, GLOBAL.CameraHeight - mmheight, mmwidth, mmheight)
	love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness,128*GLOBAL.brightness)
	love.graphics.rectangle("fill", GLOBAL.CameraWidth - mmplayerw, GLOBAL.CameraHeight - mmplayerh, mmvieww, mmviewh)

	-- the boundaries of the mini view in the minimap
	local xhi = mmplayerw
	local xlo = mmplayerw - mmvieww
	local yhi = mmplayerh
	local ylo = mmplayerh - mmviewh

	-- draw appieces, but only those visible on screen
	for index, piece in ipairs(appieces) do
		if piece.collider.active then
		    local x = piece.positionx
	 	    local y = piece.positiony
		    x = mmwidth - (mmwidth * x/GLOBAL.WorldWidth)
		    y = mmheight - (mmheight * y/GLOBAL.WorldHeight)
		    if piece.drawOnMiniMap then
	                love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,
					           255*GLOBAL.brightness,128*GLOBAL.brightness)
		        love.graphics.circle("fill", GLOBAL.CameraWidth - x, GLOBAL.CameraHeight - y, 2)
		    end
		    if x <= xhi and x >= xlo and y <= yhi and y >= ylo then
		        love.graphics.setColor(122*GLOBAL.brightness,245*GLOBAL.brightness,
					           245*GLOBAL.brightness,192*GLOBAL.brightness)
			love.graphics.draw(self.mmpiimg,GLOBAL.CameraWidth-x,GLOBAL.CameraHeight-y,0,1,1,self.mmpiimg:getWidth()/2,self.mmpiimg:getHeight()/2)
			piece.drawOnMiniMap = true
			--love.graphics.circle("fill", GLOBAL.CameraWidth - x, GLOBAL.CameraHeight - y, 2)
		    end
		end
	end

	-- draw the boss where ever, but only other enemies on screen
	for index, enemy in ipairs(self.enemies) do
		if enemy.collider.active then
			local x = enemy.positionx
			local y = enemy.positiony
			x = mmwidth - (mmwidth * x/GLOBAL.WorldWidth)
			y = mmheight - (mmheight * y/GLOBAL.WorldHeight)
			if enemy.enemytype == "boss" then
				love.graphics.setColor(255*GLOBAL.brightness,0,0,192*GLOBAL.brightness)
				love.graphics.draw(self.mmbimg,GLOBAL.CameraWidth-x,GLOBAL.CameraHeight-y)
				--love.graphics.circle("fill", GLOBAL.CameraWidth - x, GLOBAL.CameraHeight - y, 4)
			elseif x <= xhi and x >= xlo and y <= yhi and y >= ylo then
				love.graphics.setColor(0,0,0,192)
				--love.graphics.circle("fill", GLOBAL.CameraWidth - x, GLOBAL.CameraHeight - y, 3)
				love.graphics.draw(self.mmeimg,GLOBAL.CameraWidth-x,GLOBAL.CameraHeight-y)
			end
		end
	end

	-- draw all npcs
	for index, npc in ipairs(self.NPCs) do
		if npc.collider.active then
			local x = npc.positionx
			local y = npc.positiony
			x = mmwidth - (mmwidth * x/GLOBAL.WorldWidth)
			y = mmheight - (mmheight * y/GLOBAL.WorldHeight)
			love.graphics.setColor(122*GLOBAL.brightness,245*GLOBAL.brightness,245*GLOBAL.brightness,192*GLOBAL.brightness)
			--love.graphics.circle("fill", GLOBAL.CameraWidth - x, GLOBAL.CameraHeight - y, 3)
			love.graphics.draw(self.mmnimg,GLOBAL.CameraWidth-x,GLOBAL.CameraHeight-y)
		end
	end

	-- draw cave exit
	local x = self.cave.positionx
	local y = self.cave.positiony
	x = mmwidth - (mmwidth * x/GLOBAL.WorldWidth)
	y = mmheight - (mmheight * y/GLOBAL.WorldHeight)
	love.graphics.setColor(255*GLOBAL.brightness,0,0,192*GLOBAL.brightness)
	love.graphics.draw(self.mmbimg,GLOBAL.CameraWidth-x,GLOBAL.CameraHeight-y,0,0.7,0.7)
	--love.graphics.circle("fill", GLOBAL.CameraWidth - x, GLOBAL.CameraHeight - y, 3)

	-- the player in the minimap
	love.graphics.setColor(255*GLOBAL.brightness,255*GLOBAL.brightness,255*GLOBAL.brightness,192*GLOBAL.brightness)
	love.graphics.draw(self.mmplimg, GLOBAL.CameraWidth-mmplayerw+mmvieww/2, GLOBAL.CameraHeight-mmplayerh+mmviewh/2, 
				player.angle,0.7,0.7,self.mmplimg:getWidth()/2,self.mmplimg:getHeight()/2)
	--love.graphics.circle("fill", GLOBAL.CameraWidth - mmplayerw + mmvieww/2, GLOBAL.CameraHeight - mmplayerh + mmviewh/2, 2)
end

function Level:keypressed(key)
	if key == "f" then
		self.bossDefeatedJingle:stop()
		self.song:stop()
		self:clearAppieces()
		self:clearEnemies()
		self.cave.collider.active = false
        	GLOBAL.gameScreen.currLevel= Level(self.level["level"]["nextLevel"])
	elseif key == "g" then
		self.planetHealth = self.maxPlanetHealth*0.15
	elseif key == "=" then
		player.currMaxHealth = 4
		player.health = 4
	elseif key == "1" then
		self.bossDefeatedJingle:stop()
		self.song:stop()
		self:clearAppieces()
		self:clearEnemies()
		self.cave.collider.active = false
        	GLOBAL.gameScreen.currLevel= Level("level1.txt")
	elseif key == "2" then
		self.bossDefeatedJingle:stop()
		self.song:stop()
		self:clearAppieces()
		self:clearEnemies()
		self.cave.collider.active = false
        	GLOBAL.gameScreen.currLevel= Level("level2.txt")
	elseif key == "3" then
		self.bossDefeatedJingle:stop()
		self.song:stop()
		self:clearAppieces()
		self:clearEnemies()
		self.cave.collider.active = false
        	GLOBAL.gameScreen.currLevel= Level("level3.txt")
	elseif key == "4" then
		self.bossDefeatedJingle:stop()
		self.song:stop()
		self:clearAppieces()
		self:clearEnemies()
		self.cave.collider.active = false
        	GLOBAL.gameScreen.currLevel= Level("level4.txt")
	elseif key == "h" then
		if self.hasArcade then
			self.arcadeCount = 3
		end
	end
	player:keypressed(key)
	for index, npc in ipairs(self.NPCs) do
		npc:keypressed(key)
	end
	for index, arcade in ipairs(self.arcades) do
		arcade:keypressed(key)
	end
	if self.levelNum == 1 then
		GLOBAL.gameScreen.tutorial:keypressed(key)
	end
	self.confirmationScreen:keypressed(key)
end

function Level:mousepressed(x,y,button)
	player:mousepressed(x,y,button)
	for index, npc in ipairs(self.NPCs) do
		npc:mousepressed(x,y,button)
	end
	for index, arcade in ipairs(self.arcades) do
		arcade:mousepressed(x,y,button)
	end
	if self.levelNum == 1 then
		GLOBAL.gameScreen.tutorial:mousepressed(x,y,button)
	end
	self.confirmationScreen:mousepressed(x,y,button)
end

function Level:levelComplete()
	self.enter = true
end

function Level:onBossDefeated()
	self.bossDefeated = true
	player.currState = player.bossDefeatedState
	player.canMove = false
	self.postBossTimer = 3
	self.song:stop()
	self.bossDefeatedJingle:play()
end

--Increment number of pieces in Score (to display and save)
--If number of pieces is a mult of 5, gain a new health
function Level:pieceCollected()
	if self.levelType == "overworld" then
		self.appieceModCounter = self.appieceModCounter + 1
		GLOBAL.gameScreen.score:pointer(10000)
       	    	table.insert(collectedAppieces,Appiece(3,0.3,78,22,{0,0},i))
		self.appiecesCollected = self.appiecesCollected + 1
    		GLOBAL.gameScreen.score.appiecesCollected = GLOBAL.gameScreen.score.appiecesCollected + 1
		print("Number of pieces found: "..self.appiecesCollected)
		if self.appieceModCounter == 5 then
			print("New Piece!")
			self.newPieceCollectedSound:play()
			self.appieceModCounter = 0
			if player.currMaxHealth < player.maxHealth then
				print("More health!")
				player.currMaxHealth = player.currMaxHealth + 1
				player.health = player.currMaxHealth
			end
		else
			self.appieceCollectedSound:play()
		end
		self.collect = true
		self.collectCount = 0.1
		self.alpha = 255
	end
end

function Level:dropPiece(x,y)
	table.insert(appieces, Appiece(3,0.3,78,22,{x,y},self.appieceTotal))
	--self.appieceCounter = self.appieceCounter + 1
end

function Level:getPercentHealth()
	return self.planetHealth/self.maxPlanetHealth
end

function Level:getPlanetHealth()
	return self.planetHealth
end

function Level:onPlanetDeath()
    self.camera:setShake(40,5)
    self.gameOver = true
end

function Level:getAppiecesCollected()
    if self.appiecesCollected then
        return self.appiecesCollected
    else
	return 0
    end
end

function Level:clearAppieces()
    for k,v in ipairs(appieces) do
	table.remove(appieces, k)
    end
    for k,v in ipairs(collectedAppieces) do
	table.remove(collectedAppieces, k)
    end
    self.appieceTotal = 0
end

function Level:clearEnemies()
  for k,v in ipairs(self.enemies) do
	v.collider.active = false
		for i,j in ipairs(v.shooter) do
			j.collider.active = false
			table.remove(WORLD.collidables, j.collider.index)
			table.remove(v.shooter, i)
		end
	table.remove(WORLD.collidables, v.collider.index)
	table.remove(self.enemies, k)
    end
	self.enemies = {}
end

function Level:lightWorld()
    self.mask:lightWorld()
end
