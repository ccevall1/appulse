--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

TitleScreen = class()

function TitleScreen:__init()
    --initialize text and opening animation
    self.time = 0
    self.image1 = ASSETS.titleImage
    self.start = ASSETS.startButton

    self.song = ASSETS.titleScreenSong
    self.song:setLooping(true)
    self.song:play()
    self.shader = love.graphics.newShader("TitleEffect.glsl")
end

function TitleScreen:draw()
    love.graphics.setBackgroundColor(GLOBAL.color.black)
    love.graphics.setColor(GLOBAL.color.white)
    love.graphics.setShader(self.shader)
    love.graphics.draw(self.image1, GLOBAL.CameraWidth/2, GLOBAL.CameraHeight/2, 0,
		       1, 1,
                       self.image1:getWidth()/2, self.image1:getHeight()/2)
    love.graphics.setShader()
    if self.time > 3 then
        if (math.floor(self.time) % 2 == 0) then
	    love.graphics.draw(self.start,GLOBAL.CameraWidth/2,GLOBAL.CameraHeight/1.2,
				0,0.7,0.7,self.start:getWidth()/2,self.start:getHeight()/2)
	end
    end
end

function TitleScreen:update(dt)
    self.shader:send("brightness",GLOBAL.brightness)
    self.time = self.time + dt
    self.shader:send("time",self.time)
end

function TitleScreen:keypressed(key)
    self.song:stop()
    GLOBAL.mainScreen.song:play()
    GLOBAL.currentState = GLOBAL.mainScreen
end

function TitleScreen:mousepressed(x,y,button)
    self.song:stop()
    GLOBAL.mainScreen.song:play()
    GLOBAL.currentState = GLOBAL.mainScreen
end

function TitleScreen:mousemoved(x,y,dx,dy)
end
