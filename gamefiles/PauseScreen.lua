--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"

PauseScreen = class()


function PauseScreen:__init()
    --initialize menu options, background, click listeners?
    self.backgroundImage = ASSETS.pauseScreenBG
    self.title_string = "Pause"

    self.menu_options = {"Resume", "Options", "Help", "Quit"}
    self.currently_selected = 1 --which menu option is the player pointed at?
    self.current_y = 0
    self.enable_selection = true --boolean to prevent jump to next screen on space input
   --load graphic
    self.selector = ASSETS.menuSelectSprite
end

function PauseScreen:draw()
    love.graphics.setBackgroundColor(GLOBAL.color.black)
    love.graphics.draw(self.backgroundImage,GLOBAL.CameraWidth/2, 0, 0, 1, 1,
                       self.backgroundImage:getWidth()/2, 0)
    --print menu options
    local currY = GLOBAL.CameraHeight/2 + 70
    local currX = GLOBAL.CameraWidth/3 - 35
--    local option_height_index
    for k,v in ipairs(self.menu_options) do
        love.graphics.setColor(GLOBAL.color.white)
        currY = currY + 50
	currX = currX + 30
        if k == self.currently_selected then
            --draw head
	    self.current_y = currY
            love.graphics.draw(self.selector, currX, currY-50)
        end
    end
end

function PauseScreen:update(dt)
if GLOBAL.inputMode == "mouse" then
	local x,y = love.mouse.getPosition()
	if y < GLOBAL.CameraHeight/2 + 70 then
		self.currently_selected = 1
	elseif y < self.current_y - 50 then
		self.currently_selected = self.currently_selected - 1
	elseif y > self.current_y + 50 then
		self.currently_selected = self.currently_selected + 1
	elseif y > GLOBAL.CameraHeight - 100 then
		self.currently_selected = 4
	end
end
end

function PauseScreen:keypressed(key)
    if key == "down" or key == "s" then
        self.enable_selection = true
        if self.currently_selected < #self.menu_options then
            self.currently_selected = self.currently_selected + 1
        else self.currently_selected = 1
        end
    elseif key == "up" or key == "w" then
        self.enable_selection = true
        if self.currently_selected > 1 then
            self.currently_selected = self.currently_selected - 1
        else self.currently_selected = #self.menu_options
        end
    elseif key == "escape" then
        GLOBAL.currentState = GLOBAL.gameScreen
    end

    if self.enable_selection then
        if key == " " or key == "return" then
            --parse which menu option you are at
            local opt = self.currently_selected
            if self.menu_options[opt] == "Resume" then
		GLOBAL.gameScreen.currLevel.song:play()
                GLOBAL.currentState = GLOBAL.gameScreen
	    elseif self.menu_options[opt] == "Options" then
		GLOBAL.currentState = GLOBAL.pauseOptionScreen
            elseif self.menu_options[opt] == "Quit" then
                GLOBAL.gameScreen.currLevel.song:stop()
                GLOBAL.mainScreen.song:play()
                GLOBAL.currentState = GLOBAL.mainScreen
	    elseif self.menu_options[opt] == "Help" then
		GLOBAL.currentState = GLOBAL.pauseHelpScreen
            end
        end
    end
end

function PauseScreen:mousepressed(x,y,button)
	--parse which menu option you are at
	local opt = self.currently_selected
        if self.menu_options[opt] == "Resume" then
		GLOBAL.gameScreen.currLevel.song:play()
		GLOBAL.currentState = GLOBAL.gameScreen
	elseif self.menu_options[opt] == "Options" then
		GLOBAL.currentState = GLOBAL.pauseOptionScreen
	elseif self.menu_options[opt] == "Quit" then
                GLOBAL.gameScreen.currLevel.song:stop()
                GLOBAL.mainScreen.song:play()
                GLOBAL.currentState = GLOBAL.mainScreen
	elseif self.menu_options[opt] == "Help" then
		GLOBAL.currentState = GLOBAL.pauseHelpScreen
        end
end

