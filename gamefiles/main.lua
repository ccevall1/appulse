--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "math"
require "TitleScreen"
require "MainScreen"
require "HighScoreScreen"
require "GameScreen"
require "PauseScreen"
require "LoseScreen"
require "WinScreen"
require "OptionScreen"
require "HelpScreen"
require "PauseHelpScreen"
require "PauseOptionScreen"
require "Assets"
require "CutScene"
require "ContinueScreen"
require "TLfres"

math.randomseed(os.time())

function love.load()
love.mouse.setVisible(true)
--TLfres.setScreen({864,646,false,false,0}, 646, false,false)
TLfres.setScreen()
lastMousePosition = {}
lastMousePosition[1],lastMousePosition[2] = love.mouse.getPosition()
LoadAssets()
DEBUG = {}
    DEBUG.collisions = false

WORLD = {}
    WORLD.collidables = {}

GLOBAL = {}
    GLOBAL.CameraWidth, GLOBAL.CameraHeight = love.window.getDimensions()
    GLOBAL.WorldWidth = GLOBAL.CameraWidth * 4
    GLOBAL.WorldHeight = GLOBAL.CameraHeight * 4
    GLOBAL.color = {}
        GLOBAL.color.white = {255, 255, 255}
        GLOBAL.color.medGray = {102, 102, 102}
        GLOBAL.color.red = {244, 67, 54}
        GLOBAL.color.orange = {255, 152, 0}
        GLOBAL.color.yellow = {255, 235, 59}
        GLOBAL.color.black = {0, 0, 0}
        GLOBAL.color.green = {0, 255, 0}
        GLOBAL.color.blue = {0, 0, 255}
    --Unique index to keep track of collidable objects in world
    GLOBAL.collIndex = 0
    --Initialize each game state
    GLOBAL.titleScreen = TitleScreen()
    GLOBAL.mainScreen = MainScreen()
    GLOBAL.highScoreScreen = HighScoreScreen()
    GLOBAL.gameScreen = nil --GameScreen()
    GLOBAL.pauseScreen = PauseScreen()
    GLOBAL.loseScreen = LoseScreen()
    GLOBAL.winScreen = WinScreen()
    GLOBAL.optionsScreen = OptionScreen()
    GLOBAL.helpScreen = HelpScreen()
    GLOBAL.pauseHelpScreen = PauseHelpScreen()
    GLOBAL.pauseOptionScreen = PauseOptionScreen()
    GLOBAL.continueScreen = ContinueScreen()
    GLOBAL.cutsceneScreen = CutScene("Opening.txt")
    GLOBAL.currentState = GLOBAL.cutsceneScreen
    GLOBAL.currentLevel = 1
    GLOBAL.inputMode = "keyboard"

    GLOBAL.brightness = 1.0
    GLOBAL.brightnessShader = love.graphics.newShader("Brightness.glsl")
    GLOBAL.volume = 1.0
end

function love.update(dt)
    mouseMoved()
    GLOBAL.brightnessShader:send("brightness",GLOBAL.brightness)
    GLOBAL.currentState:update(dt)
end

function love.draw()
    TLfres.transform()
    love.graphics.setShader(GLOBAL.brightnessShader)
    GLOBAL.currentState:draw()
    love.graphics.setShader()
    TLfres.letterbox(4,3)
end

function love.keypressed(key)
    GLOBAL.inputMode = "keyboard"
    love.mouse.setVisible(false)
    GLOBAL.currentState:keypressed(key)
end

function love.mousepressed(x,y,button)
    GLOBAL.inputMode = "mouse"
    love.mouse.setVisible(true)
    GLOBAL.currentState:mousepressed(x,y,button)
end

function mouseMoved()
    local x,y = love.mouse.getPosition()
    if not (x == lastMousePosition[1]) or not (y == lastMousePosition[2]) then
	GLOBAL.inputMode = "mouse"
	love.mouse.setVisible(true)
	lastMousePosition = {x,y}
    end

end
