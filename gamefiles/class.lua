--Peter's sample code for Lua "classes"

function class(base)
	-- This table is what we'll return from the function as "the class"
	-- as it were. Most of the code that follows fills the right things
	-- into "c" to make everything work out.
	local c = {}

	-- If there's a base class, we'll remember it for later. This is
	-- used for initializers and for the is_a() operation defined
	-- between classes (see below).
	if base then
		c.__base = base
	end

	-- Objects of this class will use "c" as their metatable, so just
	-- like we did with prototypes, we want __index to delegate from
	-- instances to "c".
	--
	-- However, don't want to stop looking after just THIS class, we
	-- might have to check the BASE class for an attribute as well.
	-- That's what makes the following __index metamethod a bit more
	-- confusing than it probably should be.
	--
	-- Note that if we didn't want inheritance (see Circle below which
	-- doesn't have its own draw method) we could say "c.__index = c"
	-- and be done here.
	c.__index = function (self, name)
		m = getmetatable(self)
		while m do
			f = rawget(m, name) -- avoid recursion!
			if f then
				return f
			else
				m = m.__base
			end
		end
	end

	-- We want the class to function as a constructor using a Python-like
	-- notation. We can only make this happen by changing what a function
	-- call operator does to "c". That in turn requires a metatable for
	-- "c" itself. Yes, this is a little creepy. :-)
	local mt = {}
	mt.__call = function(table, ...)
		-- This is the function we run as a constructor. Make sure
		-- you understand where this function ends (textually) and
		-- where it ends up (dynamically)! :-)

		-- The new object we're creating with this constructor.
		local o = {}
		-- Use "c" as the metatable for "o" to delegate properly.
		setmetatable(o, c)
		-- Run initializers as appropriate.
		if table.__init then
			-- If there's a custom initializer, run it with the
			-- arguments provided to the constructor call.
			table.__init(o, ...)
		elseif c.__base and c.__base.__init then
			-- If there's an initializer in the base class, but
			-- not one in the current class, run that one.
			c.__base.__init(o, ...)
		end

		return o
	end
	setmetatable(c, mt)

	-- Finally add an is_a() operation that can be used to check if
	-- an object has a certain class.
	c.__is_a = function (self, klass)
		local m = getmetatable(self)
		while m do
			if m == klass then
				return true
			else
				m = m.__base
			end
		end
		return false
	end

	return c
end
