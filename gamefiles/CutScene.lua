-- Opening Animation
--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- tchapin3@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "GameState"
require "Assets"

CutScene = class()

function CutScene:__init(file)
	JSON = require("JSON")
	--JSON = assert(loadfile("JSON.lua"))()
	self.jsonrawtext= ""
	for line in love.filesystem.lines(file)do		
		self.jsonrawtext= self.jsonrawtext..line
	end
	self.cutScenes = JSON:decode(self.jsonrawtext)
self.frame = 1
self.clip={}
self.pictures = self.cutScenes["CutScene"]["pictures"]
if self.cutScenes["CutScene"]["nextStateGameScreen"] then
	self.nextState= GLOBAL.gameScreen
elseif self.cutScenes["CutScene"]["nextStateTitleScreen"] then
	self.nextState= GLOBAL.titleScreen
elseif self.cutScenes["CutScene"]["nextStateWinScreen"] then
	self.nextState=GLOBAL.winScreen
elseif self.cutScenes["CutScene"]["nextStateLoseScreen"] then
	self.nextState=GLOBAL.loseScreen
end

for i=1, self.pictures  do
self.clip[i] = love.graphics.newImage(self.cutScenes["CutScene"]["picTABLE"][""..i..""])
end
self.pic=self.clip[1]
GLOBAL.WorldWidth = self.pic:getWidth()
GLOBAL.WorldHeight = self.pic:getHeight()
self.timer = 0
end

function CutScene:update(dt)
	self.timer = self.timer + dt
	if self.timer > 0.2 then
		self.frame=self.frame + 1
		if self.frame > self.pictures then
			GLOBAL.currentState  = self.nextState
		else
			self.pic = self.clip[self.frame]
			self.timer = 0
		end
	end
end

function CutScene:draw()
    love.graphics.setColor(GLOBAL.color.white);
    love.graphics.draw(self.pic, GLOBAL.CameraWidth/2, GLOBAL.CameraHeight/2, 0, 
		       1, 1,
                       self.pic:getWidth()/2, self.pic:getHeight()/2)
end

function CutScene:keypressed(key)
   GLOBAL.currentState = self.nextState
end

function CutScene:mousepressed(x,y,button)
   GLOBAL.currentState = self.nextState
end
