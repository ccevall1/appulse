--[[
 - Caroline Cevallos- ccevall1@jhu.edu
 - Tucker Chapin- chapin@jhu.edu
 - Brandon Fiksel- bfiksel1@jhu.edu
 - Richard Gholston- rgholst1@jhu.edu
 - Margo Goodall- mgoodall3@jhu.edu
]]
require "class"
Sprite = class()

-- imagePath is a mandatory string, but shape and location are not
function Sprite:__init(frame_width,frame_height, location, color, shape)
    self.width = frame_width
    self.height = frame_height
    if location == "center" or location == "" then
      location = {}
        location.x = GLOBAL.CameraWidth/2
        location.y = GLOBAL.CameraHeight/2
    elseif location == "random" then
      location = {}
        location.x = math.random(GLOBAL.CameraWidth)
        location.y = math.random(GLOBAL.WorldHeight)
    end
    self.body = love.physics.newBody(world, location.x, location.y, "dynamic")
    self.start = location
    self.start.x = location[1]
    self.start.y = location[2]

    if shape then
      self.shape = love.physics.newRectangleShape(shape)
      x1, y1, x2, y2, x3, y3, x4, y4 = self.shape:getBoundingBox( )
      self.width = x4 - x1
      self.height = y2 - y1
    else
      self.shape = love.physics.newRectangleShape(self.width, self.height)
    end

    self.fixture = love.physics.newFixture(self.body, self.shape, 10)
    self.fixture:setUserData("player")
    self.direction = {0,1} -- used to calculate direction of movement
    self.maxSpeed = 5 -- max speed of sprite
    self.speed = 0 -- current speed of sprite
    self.maxAccel = .7
    self.velocity = {x=0, y=0}
    self.angle = 0

    if color == "random" then
      self.color = {math.random(255), math.random(255), math.random(255)}
    elseif color then
      self.color = color
    else
      self.color = GLOBAL.color.white
    end
end

function Sprite:draw()
    positionx, positiony = self.body:getCameraPoints(self.shape:getPoints())
    -- for debugging and seeing collision box
    -- love.graphics.setColor(GLOBAL.color.white)
    -- love.graphis.rectangle("fill", positionx, positiony, self.width, self.height)
    love.graphics.setColor(self.color)
    love.graphics.draw(self.image, positionx, positiony, self.body:getAngle())
end
