require "class"

bullets = class()

function bullets:__init(speed)
    self.bullets = {}
    self.bulletSpeed = speed
end

function bullets:update(dt)
    for i,v in ipairs(self.bullets) do
        if v.drawBullet then
            v:update(dt)
        end
    end
end

function bullets:draw()
    for i,v in ipairs(self.bullets) do
        v:draw()
    end
end

--insert new bullet into bullets table
function bullets:shoot(x,y)
local startX =x 
local startY = y
local angle,bulletDx,bulletDy = 0,0,0
if GLOBAL.inputMode == "mouse" then
    --calculate player position
	mouse.x, mouse.y = love.mouse.getPosition()
	local mouseX = mouse.x
	local mouseY = mouse.y
        mouseX = mouseX + GLOBAL.gameScreen.currLevel.camera:getX()
        mouseY = mouseY + GLOBAL.gameScreen.currLevel.camera:getY()
	angle = math.atan2((mouseY - startY), (mouseX - startX))
	bulletDx = self.bulletSpeed * math.cos(angle)
	bulletDy = self.bulletSpeed * math.sin(angle)
elseif GLOBAL.inputMode == "keyboard" then
	angle = player.angle - math.pi/2
	bulletDx = self.bulletSpeed * math.cos(angle)
	bulletDy = self.bulletSpeed * math.sin(angle)
end
        local song = ASSETS.shootSound
	song:setVolume(GLOBAL.volume)
        song:play()
	table.insert(self.bullets, bullet(startX,startY,bulletDx,bulletDy,
                     player.segments[player.health],"playerBullet",6,false,nil,0))
end

function bullets:removeBullet(b)
    table.remove(self.bullets, b.index)
end

