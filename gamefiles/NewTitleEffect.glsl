extern number time;
extern number brightness;

vec4 effect(vec4 color, Image img, vec2 tex, vec2 scr)
{
	vec4 c = Texel(img,tex);
	float wave = fract(sin(dot(scr,vec2(12.9898,78.233)))*43758.5453);
	return c*vec4(1.0*wave,1.0*wave,1.0*wave,cos(time*wave)+1.0)*brightness;
}
